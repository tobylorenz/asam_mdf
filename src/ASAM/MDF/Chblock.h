/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <vector>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Channel hierarchy block (CHBLOCK)
 *
 * Definition of a logical classure/hierarchy
 * for channels
 */
class ASAM_MDF_EXPORT Chblock final : public Block
{
public:
    Chblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief ch_ch_next
     *
     * Link to next sibling CHBLOCK (can be NIL)
     */
    LINK ch_ch_next;

    /**
     * @brief ch_ch_first
     *
     * Link to first child CHBLOCK
     * (can be NIL, must be NIL for ch_type = 3
     * ("map list")).
     */
    LINK ch_ch_first;

    /**
     * @brief ch_tx_name
     *
     * Link to TXBLOCK with the name of the
     * hierarchy level.
     *
     * Must be NIL for ch_type ≥ 4, must not be
     * NIL for all other types.
     *
     * If specified, the name must be according to
     * naming rules stated in 5.4.2 Naming Rules,
     * and it must be unique within all sibling
     * CHBLOCKs.
     */
    LINK ch_tx_name;

    /**
     * @brief ch_md_comment
     *
     * Link to TXBLOCK or MDBLOCK with
     * comment and other information for the
     * hierarchy level (can be NIL)
     */
    LINK ch_md_comment;

    /**
     * @brief ch_element
     *
     * References to the channels for this
     * hierarchy level.
     *
     * Each reference is a link triple with pointer to
     * parent DGBLOCK, parent CGBLOCK and
     * CNBLOCK for the channel. Thus the links
     * have the following order
     *
     * - DGBLOCK for channel 1
     * - CGBLOCK for channel 1
     * - CNBLOCK for channel 1
     * - ...
     * - DGBLOCK for channel N
     * - CGBLOCK for channel N
     * - CNBLOCK for channel N
     *
     * None of the links can be NIL.
     */
    std::vector<LinkTriple> ch_element;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief ch_element_count
     *
     * Number of channels N referenced by this
     * CHBLOCK.
     */
    UINT32 ch_element_count;

    /**
     * @brief ch_type
     *
     * Type of hierarchy level:
     *
     * (see also Table 20 for allowed child types)
     *
     * 0 = group.
     * All elements and children of this hierarchy
     * level form a logical group (see [MCD-2 MC]
     * keyword GROUP).
     *
     * 1 = function.
     * All children of this hierarchy level form a
     * functional group (see [MCD-2 MC] keyword
     * FUNCTION)
     * For this type, the hierarchy must not contain
     * CNBLOCK references (ch_element_count
     * must be 0).
     *
     * 2 = structure.
     * All elements and children of this hierarchy
     * level form a "fragmented" structure, see
     * 5.18.1 Structures.
     * Note: Do not use "fragmented" and
     * "compact" structure in parallel. If possible
     * prefer a "compact" structure.
     *
     * 3 = map list.
     * All elements of this hierarchy level form a
     * map list
     * (see [MCD-2 MC] keyword MAP_LIST):
     * - the first element represents the z axis
     *   (must be a curve, i.e. CNBLOCK with
     *   CABLOCK of type "scaling axis")
     * - all other elements represent the maps
     *   (must be 2-dimensional map, i.e.
     *   CNBLOCK with CABLOCK of type "lookup")
     *
     * 4 = input variables of function.
     * (see [MCD-2 MC] keyword
     * IN_MEASUREMENT)
     * All referenced channels must be
     * measurement objects ("calibration" flag (bit
     * 7) not set in cn_flags)
     *
     * 5 = output variables of function.
     * (see [MCD-2 MC] keyword
     * OUT_MEASUREMENT)
     * All referenced channels must be
     * measurement objects ("calibration" flag (bit
     * 7) not set in cn_flags)
     *
     * 6 = local variables of function.
     * (see [MCD-2 MC] keyword
     * LOC_MEASUREMENT)
     * All referenced channels must be
     * measurement objects ("calibration" flag (bit
     * 7) not set in cn_flags)
     *
     * 7 = calibration objects defined in function.
     * (see [MCD-2 MC] keyword
     * DEF_CHARACTERISTIC)
     * All referenced channels must be calibration
     * objects
     * ("calibration" flag (bit 7) set in cn_flags)
     *
     * 8 = calibration objects referenced in
     * function.
     * (see [MCD-2 MC] keyword
     * REF_CHARACTERISTIC)
     * All referenced channel must be calibration
     * objects
     * ("calibration" flag (bit 7) set in cn_flags)
     */
    UINT8 ch_type;

    /**
     * @brief ch_reserved
     *
     * Reserved
     */
    std::array<BYTE, 3> ch_reserved;

    /** @} */
};

}
}
