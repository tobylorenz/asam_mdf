/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"

namespace ASAM {
namespace MDF {

/* forward declarations */
class File;

/** Possible block entries */
enum BlockType {
    Unknown = 0,
    // ID is not based on the usual (base) block layout
    HD, MD, TX, FH, CH,
    AT, EV, DG, CG, SI,
    CN, CC, CA, DT, SR,
    RD, SD, DL, DZ, HL
};

/** Block base class */
class ASAM_MDF_EXPORT Block
{
public:
    Block();
    virtual ~Block();

    /**
     * Read block at link.
     *
     * @param[in] file file
     * @param[in] link file link
     */
    virtual void read(File & file, LINK link);

    /**
     * Write block.
     *
     * If link=0 then it will be appended to file.
     *
     * If link!=0 it will be written at the given link.
     * This is useful only during reordering.
     *
     * link will always return the actual write link.
     *
     * @param[in] file file
     * @param[in,out] link file link
     */
    virtual void write(File & file, LINK & link);

    /**
     * Return the block type at given link position.
     *
     * @param[in] file file
     * @param[in] link file link
     * @return block type
     */
    static BlockType blockType(File & file, LINK link);

    /**
     * Expected identifier.
     *
     * @return Expected identifier
     */
    virtual std::array<CHAR, 4> expectedId() const;

    /**
     * Minimum block size expected by version x.
     *
     * @param[in] id_ver Version
     * @return Minimum block size
     */
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const;

    /**
     * Block size expected by version x.
     *
     * @param[in] id_ver Version
     * @return Block size
     */
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const;

    /**
     * @name Header section
     * @{
     */

    /**
     * @brief id
     *
     * Block type identifier
     */
    std::array<CHAR, 4> id;

    /**
     * @brief reserved
     *
     * Reserved used for 8-Byte alignment
     */
    std::array<BYTE, 4> reserved;

    /**
     * @brief length
     *
     * Length of block
     */
    UINT64 length;

    /**
     * @brief link_count
     *
     * Number of links
     */
    UINT64 link_count;

    /** @} */
};

}
}
