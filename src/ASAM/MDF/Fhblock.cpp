/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Fhblock.h"

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Fhblock::Fhblock() :
    Block(),
    fh_fh_next(0),
    fh_md_comment(0),
    fh_time_ns(0),
    fh_tz_offset_min(0),
    fh_dst_offset_min(0),
    fh_time_flags(0),
    fh_reserved()
{
    id = expectedId();
}

void Fhblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, fh_fh_next);
    readLittleEndian(file.fs, fh_md_comment);

    /* read Data section */
    readLittleEndian(file.fs, fh_time_ns);
    readLittleEndian(file.fs, fh_tz_offset_min);
    readLittleEndian(file.fs, fh_dst_offset_min);
    readLittleEndian(file.fs, fh_time_flags);
    file.fs.read(reinterpret_cast<char *>(fh_reserved.data()), fh_reserved.size());
}

void Fhblock::write(File & file, LINK & link)
{
    /* pre processing */
    link_count = 2;
    length = expectedMinimumBlockSize(file.id.id_ver);

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, fh_fh_next);
    writeLittleEndian(file.fs, fh_md_comment);

    /* write Data section */
    writeLittleEndian(file.fs, fh_time_ns);
    writeLittleEndian(file.fs, fh_tz_offset_min);
    writeLittleEndian(file.fs, fh_dst_offset_min);
    writeLittleEndian(file.fs, fh_time_flags);
    file.fs.write(reinterpret_cast<char *>(fh_reserved.data()), fh_reserved.size());
}

std::array<CHAR, 4> Fhblock::expectedId() const
{
    return {{ '#', '#', 'F', 'H' }};
}

UINT64 Fhblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(fh_fh_next) +
            sizeof(fh_md_comment) +
            sizeof(fh_time_ns) +
            sizeof(fh_tz_offset_min) +
            sizeof(fh_dst_offset_min) +
            sizeof(fh_time_flags) +
            sizeof(fh_reserved);
        break;
    }
    return size;
}

UINT64 Fhblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size += 0;
        break;
    }
    return size;
}

}
}
