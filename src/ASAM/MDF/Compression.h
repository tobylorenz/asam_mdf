/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <vector>

#include <zlib.h>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"

namespace ASAM {
namespace MDF {

/**
 * Compress data
 *
 * dataOut will be resized to the required size.
 *
 * @param[out] dataOut output data
 * @param[in] dataIn input data
 * @param[in] compressionLevel compression level parsed to zlib compress2 function
 */
void ASAM_MDF_EXPORT deflateData(std::vector<BYTE> & dataOut, std::vector<BYTE> & dataIn, int compressionLevel = Z_DEFAULT_COMPRESSION);

/**
 * Uncompress data
 *
 * dataOut must already be resized to the known uncompressed data size,
 * e.g. dz_org_data_length.
 *
 * @param[out] dataOut output data
 * @param[in] dataIn input data
 */
void ASAM_MDF_EXPORT inflateData(std::vector<BYTE> & dataOut, std::vector<BYTE> & dataIn);

/**
 * Transpose data
 *
 * dataOut will be set to the size of dataIn.
 *
 * @param[out] dataOut output data
 * @param[in] dataIn input data
 * @param[in] nc column count
 * @param[in] mc row count
 */
void ASAM_MDF_EXPORT transposeData(std::vector<BYTE> & dataOut, std::vector<BYTE> & dataIn, UINT32 nc, UINT32 mc);

}
}
