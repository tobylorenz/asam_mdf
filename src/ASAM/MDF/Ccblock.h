/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <vector>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Channel conversion block (CCBLOCK)
 *
 * Description of a conversion formula for
 * a channel
 */
class ASAM_MDF_EXPORT Ccblock final : public Block
{
public:
    Ccblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief cc_tx_name
     *
     * Link to TXBLOCK with name (identifier) of
     * conversion (can be NIL).
     *
     * Name must be according to naming rules
     * stated in 5.4.2 Naming Rules.
     */
    LINK cc_tx_name;

    /**
     * @brief cc_md_unit
     *
     * Link to TXBLOCK/MDBLOCK with physical
     * unit of signal data (after conversion). (can
     * be NIL)
     *
     * Unit only applies if no unit defined in
     * CNBLOCK. Otherwise the unit of the
     * channel overwrites the conversion unit.
     *
     * An MDBLOCK can be used to additionally
     * reference the A-HDO unit definition (see
     * Table 55).
     *
     * Note: for channels with cn_sync_type > 0,
     * the unit is already defined, thus a reference
     * to an A-HDO definition should be omitted to
     * avoid redundancy.
     */
    LINK cc_md_unit;

    /**
     * @brief cc_md_comment
     *
     * Link to TXBLOCK/MDBLOCK with comment
     * of conversion and additional information,
     * see Table 54.(can be NIL)
     */
    LINK cc_md_comment;

    /**
     * @brief cc_cc_inverse
     *
     * Link to CCBLOCK for inverse formula (can
     * be NIL, must be NIL for CCBLOCK of the
     * inverse formula (no cyclic reference
     * allowed).
     */
    LINK cc_cc_inverse;

    /**
     * @brief cc_ref
     *
     * List of additional links to TXBLOCKs with
     * strings or to CCBLOCKs with partial
     * conversion rules. Length of list is given by
     * cc_ref_count. The list can be empty. Details
     * are explained in formula-specific block
     * supplement.
     */
    std::vector<LINK> cc_ref;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief cc_type
     *
     * Conversion type (formula identifier)
     *
     * - 0 = 1:1 conversion
     *   (in this case, the CCBLOCK can be omitted)
     * - 1 = linear conversion
     * - 2 = rational conversion
     * - 3 = algebraic conversion (MCD-2 MC text
     *   formula)
     * - 4 = value to value tabular look-up with
     *   interpolation
     * - 5 = value to value tabular look-up without
     *   interpolation
     * - 6 = value range to value tabular look-up
     * - 7 = value to text/scale conversion tabular
     *   look-up
     * - 8 = value range to text/scale conversion
     *   tabular look-up
     * - 9 = text to value tabular look-up
     * - 10 = text to text tabular look-up (translation)
     */
    UINT8 cc_type;

    /**
     * @brief cc_precision
     *
     * Precision for display of floating point values.
     * 0xFF means unrestricted precision (infinite)
     * Any other value specifies the number of
     * decimal places to use for display of floating
     * point values.
     *
     * Note: only valid if "precision valid" flag (bit 0)
     * is set and if cn_precision of the parent
     * CNBLOCK is invalid, otherwise cn_precision
     * must be used.
     */
    UINT8 cc_precision;

    /**
     * @brief cc_flags
     *
     * Flags
     *
     * The value contains the following bit flags
     * (Bit 0 = LSB):
     *
     * Bit 0: Precision valid flag.
     * If set, the precision value for display of
     * floating point values specified in
     * cc_precision is valid
     *
     * Bit 1: Physical value range valid flag.
     * If set, both the minimum and the maximum
     * physical value that occurred after
     * conversion for this signal within the samples
     * recorded in this file are known and stored in
     * cc_phy_range_min and cc_phy_range_max.
     * Otherwise the two fields are not valid.
     * Note: the physical value range can only be
     * expressed for conversions which return a
     * numeric value (REAL). For conversions
     * returning a string value or for the inverse
     * conversion rule the flag must not be set.
     *
     * Bit 2: Status string flag.
     * This flag indicates for conversion types 7
     * and 8 (value/value range to text/scale
     * conversion tabular look-up) that the normal
     * table entries are status strings (only
     * reference to TXBLOCK), and the actual
     * conversion rule is given in CCBLOCK
     * referenced by default value.
     * This also implies special handling of limits,
     * see [MCD-2 MC] keyword
     * STATUS_STRING_REF.
     * Can only be set for 7 <= cc_type <= 8.
     */
    UINT16 cc_flags;

    /**
     * @brief cc_ref_count
     *
     * Length M of cc_ref list with additional links.
     * See formula-specific block supplement for
     * meaning of the links.
     */
    UINT16 cc_ref_count;

    /**
     * @brief cc_val_count
     *
     * Length N of cc_val list with additional
     * parameters. See formula-specific block
     * supplement for meaning of the parameters.
     */
    UINT16 cc_val_count;

    /**
     * @brief cc_phy_range_min
     *
     * Minimum physical signal value that occurred
     * for this signal. Only valid if "physical value
     * range valid" flag (bit 1) is set.
     */
    REAL cc_phy_range_min;

    /**
     * @brief cc_phy_range_max
     *
     * Maximum physical signal value that
     * occurred for this signal. Only valid if
     * "physical value range valid" flag (bit 1) is
     * set.
     */
    REAL cc_phy_range_max;

    /**
     * @brief cc_val
     *
     * List of additional conversion parameters.
     * Length of list is given by cc_val_count. The
     * list can be empty.
     *
     * Details are explained in formula-specific
     * block supplement.
     */
    std::vector<REAL> cc_val;

    /** @} */
};

}
}
