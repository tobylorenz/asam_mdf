/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Data group block (DGBLOCK)
 *
 * Description of data block that may refer
 * to one or more channel groups
 */
class ASAM_MDF_EXPORT Dgblock final : public Block
{
public:
    Dgblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief dg_dg_next
     *
     * Pointer to next data group block (DGBLOCK)
     * (can be NIL)
     */
    LINK dg_dg_next;

    /**
     * @brief dg_cg_first
     *
     * Pointer to first channel group block
     * (CGBLOCK) (can be NIL)
     */
    LINK dg_cg_first;

    /**
     * @brief dg_data
     *
     * Pointer to data block (DTBLOCK or DZBLOCK
     * for this block type) or data list block
     * (DLBLOCK of data blocks or its HLBLOCK)
     * (can be NIL)
     */
    LINK dg_data;

    /**
     * @brief dg_md_comment
     *
     * Pointer to comment and additional information
     * (TXBLOCK or MDBLOCK) (can be NIL)
     */
    LINK dg_md_comment;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief dg_rec_id_size
     *
     * Number of Bytes used for record IDs in the
     * data block.
     *
     * 0 = data records without record ID
     * (only possible for sorted data group)
     *
     * 1 = record ID (UINT8)
     * before each data record
     *
     * 2 = record ID (UINT16, LE Byte order)
     * before each data record
     *
     * 4 = record ID (UINT32, LE Byte order)
     * before each data record
     *
     * 8 = record ID (UINT64, LE Byte order)
     * before each data record
     */
    UINT8 dg_rec_id_size;

    /**
     * @brief reserved
     *
     * Reserved
     */
    std::array<BYTE, 7> dg_reserved;

    /** @} */
};

}
}
