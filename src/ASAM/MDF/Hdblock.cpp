/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Hdblock.h"

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Hdblock::Hdblock() :
    Block(),
    hd_dg_first(0),
    hd_fh_first(0),
    hd_ch_first(0),
    hd_at_first(0),
    hd_ev_first(0),
    hd_md_comment(0),
    hd_start_time_ns(0),
    hd_tz_offset_min(0),
    hd_dst_offset_min(0),
    hd_time_flags(0),
    hd_time_class(0),
    hd_flags(0),
    hd_reserved(0),
    hd_start_angle_rad(0.0),
    hd_start_distance_m(0.0)
{
    id = expectedId();
}

void Hdblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, hd_dg_first);
    readLittleEndian(file.fs, hd_fh_first);
    readLittleEndian(file.fs, hd_ch_first);
    readLittleEndian(file.fs, hd_at_first);
    readLittleEndian(file.fs, hd_ev_first);
    readLittleEndian(file.fs, hd_md_comment);

    /* read Data section */
    readLittleEndian(file.fs, hd_start_time_ns);
    readLittleEndian(file.fs, hd_tz_offset_min);
    readLittleEndian(file.fs, hd_dst_offset_min);
    readLittleEndian(file.fs, hd_time_flags);
    readLittleEndian(file.fs, hd_time_class);
    readLittleEndian(file.fs, hd_flags);
    readLittleEndian(file.fs, hd_reserved);
    readLittleEndian(file.fs, hd_start_angle_rad);
    readLittleEndian(file.fs, hd_start_distance_m);
}

void Hdblock::write(File & file, LINK & link)
{
    /* pre processing */
    link_count = 6;
    length = expectedMinimumBlockSize(file.id.id_ver);

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, hd_dg_first);
    writeLittleEndian(file.fs, hd_fh_first);
    writeLittleEndian(file.fs, hd_ch_first);
    writeLittleEndian(file.fs, hd_at_first);
    writeLittleEndian(file.fs, hd_ev_first);
    writeLittleEndian(file.fs, hd_md_comment);

    /* write Data section */
    writeLittleEndian(file.fs, hd_start_time_ns);
    writeLittleEndian(file.fs, hd_tz_offset_min);
    writeLittleEndian(file.fs, hd_dst_offset_min);
    writeLittleEndian(file.fs, hd_time_flags);
    writeLittleEndian(file.fs, hd_time_class);
    writeLittleEndian(file.fs, hd_flags);
    writeLittleEndian(file.fs, hd_reserved);
    writeLittleEndian(file.fs, hd_start_angle_rad);
    writeLittleEndian(file.fs, hd_start_distance_m);
}

std::array<CHAR, 4> Hdblock::expectedId() const
{
    return {{ '#', '#', 'H', 'D' }};
}

UINT64 Hdblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(hd_dg_first) +
            sizeof(hd_fh_first) +
            sizeof(hd_ch_first) +
            sizeof(hd_at_first) +
            sizeof(hd_ev_first) +
            sizeof(hd_md_comment) +
            sizeof(hd_start_time_ns) +
            sizeof(hd_tz_offset_min) +
            sizeof(hd_dst_offset_min) +
            sizeof(hd_time_flags) +
            sizeof(hd_time_class) +
            sizeof(hd_flags) +
            sizeof(hd_reserved) +
            sizeof(hd_start_angle_rad) +
            sizeof(hd_start_distance_m);
        break;
    }
    return size;
}

UINT64 Hdblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size += 0;
        break;
    }
    return size;
}

}
}
