/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Dgblock.h"

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Dgblock::Dgblock() :
    Block(),
    dg_dg_next(0),
    dg_cg_first(0),
    dg_data(0),
    dg_md_comment(0),
    dg_rec_id_size(0),
    dg_reserved()
{
    id = expectedId();
}

void Dgblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, dg_dg_next);
    readLittleEndian(file.fs, dg_cg_first);
    readLittleEndian(file.fs, dg_data);
    readLittleEndian(file.fs, dg_md_comment);

    /* read Data section */
    readLittleEndian(file.fs, dg_rec_id_size);
    file.fs.read(reinterpret_cast<char *>(dg_reserved.data()), dg_reserved.size());
}

void Dgblock::write(File & file, LINK & link)
{
    /* pre processing */
    link_count = 4;
    length = expectedMinimumBlockSize(file.id.id_ver);

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, dg_dg_next);
    writeLittleEndian(file.fs, dg_cg_first);
    writeLittleEndian(file.fs, dg_data);
    writeLittleEndian(file.fs, dg_md_comment);

    /* write Data section */
    writeLittleEndian(file.fs, dg_rec_id_size);
    file.fs.write(reinterpret_cast<char *>(dg_reserved.data()), dg_reserved.size());
}

std::array<CHAR, 4> Dgblock::expectedId() const
{
    return {{ '#', '#', 'D', 'G' }};
}

UINT64 Dgblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(dg_dg_next) +
            sizeof(dg_cg_first) +
            sizeof(dg_data) +
            sizeof(dg_md_comment) +
            sizeof(dg_rec_id_size) +
            sizeof(dg_reserved);
        break;
    }
    return size;
}

UINT64 Dgblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size += 0;
        break;
    }
    return size;
}

}
}
