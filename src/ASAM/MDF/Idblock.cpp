/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Idblock.h"

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

/* expected identifier */
static const std::array<CHAR, 8> expectedIdFileFinished =
{{'M', 'D', 'F', ' ', ' ', ' ', ' ', ' '}};
static const std::array<CHAR, 8> expectedIdFileUnfinished =
{{ 'U', 'n', 'F', 'i', 'n', 'M', 'F', ' ' }};

Idblock::Idblock() :
    id_file(expectedIdFileFinished),
    id_vers({{ '4', '.', '1', '1', ' ', ' ', ' ', ' ' }}),
    id_prog({{ 'T', 'o', 'M', 'i', 'L', 'o', ' ', ' ' }}),
    id_reserved1(),
    id_ver(411),
    id_reserved2(),
    id_unfin_flags(0),
    id_custom_unfin_flags(0)
{
}

Idblock::~Idblock()
{
}

void Idblock::read(File & file)
{
    /* always at 0 */
    file.fs.seekg(0);

    /* read */
    file.fs.read(reinterpret_cast<char *>(id_file.data()), id_file.size());
    if ((id_file != expectedIdFileFinished) && (id_file != expectedIdFileUnfinished)) {
        throw UnexpectedIdentifier(0);
    }
    file.fs.read(reinterpret_cast<char *>(id_vers.data()), id_vers.size());
    file.fs.read(reinterpret_cast<char *>(id_prog.data()), id_prog.size());
    file.fs.read(reinterpret_cast<char *>(id_reserved1.data()), id_reserved1.size());
    readLittleEndian(file.fs, id_ver);
    file.fs.read(reinterpret_cast<char *>(id_reserved2.data()), id_reserved2.size());
    readLittleEndian(file.fs, id_unfin_flags);
    readLittleEndian(file.fs, id_custom_unfin_flags);
}

void Idblock::write(File & file)
{
    /* always at 0 */
    file.fs.seekp(0);

    /* write */
    file.fs.write(reinterpret_cast<char *>(id_file.data()), id_file.size());
    file.fs.write(reinterpret_cast<char *>(id_vers.data()), id_vers.size());
    file.fs.write(reinterpret_cast<char *>(id_prog.data()), id_prog.size());
    file.fs.write(reinterpret_cast<char *>(id_reserved1.data()), id_reserved1.size());
    writeLittleEndian(file.fs, id_ver);
    file.fs.write(reinterpret_cast<char *>(id_reserved2.data()), id_reserved2.size());
    writeLittleEndian(file.fs, id_unfin_flags);
    writeLittleEndian(file.fs, id_custom_unfin_flags);
}

bool Idblock::finalized()
{
    return
        (id_file == expectedIdFileFinished) &&
        (id_unfin_flags == 0) &&
        (id_custom_unfin_flags == 0);
}

void Idblock::updateFinalized()
{
    if ((id_unfin_flags == 0) && (id_custom_unfin_flags == 0)) {
        id_file = expectedIdFileFinished;
    } else {
        id_file = expectedIdFileUnfinished;
    }
}

void Idblock::setVersion(UINT16 ver)
{
    std::string verStr = std::to_string(ver);
    id_vers[0] = verStr[0];
    id_vers[1] = '.';
    id_vers[2] = verStr[1];
    id_vers[3] = verStr[2];
    id_vers[4] = ' ';
    id_vers[5] = ' ';
    id_vers[6] = ' ';
    id_vers[7] = ' ';
    id_ver = ver;
}

}
}
