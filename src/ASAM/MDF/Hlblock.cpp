/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Hlblock.h"

#include <cassert>

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Hlblock::Hlblock() :
    Block(),
    hl_dl_first(0),
    hl_flags(0),
    hl_zip_type(0),
    hl_reserved()
{
    id = expectedId();
}

void Hlblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, hl_dl_first);

    /* read Data section */
    readLittleEndian(file.fs, hl_flags);
    readLittleEndian(file.fs, hl_zip_type);
    file.fs.read(reinterpret_cast<char *>(hl_reserved.data()), hl_reserved.size());
}

void Hlblock::write(File & file, LINK & link)
{
    /* pre processing */
    link_count = 1;
    length = expectedMinimumBlockSize(file.id.id_ver);

    /* check */
    assert(file.id.id_ver >= 410);

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, hl_dl_first);

    /* write Data section */
    writeLittleEndian(file.fs, hl_flags);
    writeLittleEndian(file.fs, hl_zip_type);
    file.fs.write(reinterpret_cast<char *>(hl_reserved.data()), hl_reserved.size());
}

std::array<CHAR, 4> Hlblock::expectedId() const
{
    return {{ '#', '#', 'H', 'L' }};
}

UINT64 Hlblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 410:
    case 411:
        size +=
            sizeof(hl_dl_first) +
            sizeof(hl_flags) +
            sizeof(hl_zip_type) +
            sizeof(hl_reserved);
        break;
    }
    return size;
}

UINT64 Hlblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 410:
    case 411:
        size += 0;
        break;
    }
    return size;
}

}
}
