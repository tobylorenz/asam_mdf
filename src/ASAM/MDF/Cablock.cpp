/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Cablock.h"

#include <cassert>

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Cablock::Cablock() :
    Block(),
    ca_composition(0),
    ca_data(),
    ca_dynamic_size(),
    ca_input_quantity(),
    ca_output_quantity(),
    ca_comparison_quantity(),
    ca_cc_axis_conversion(),
    ca_axis(),
    ca_type(0),
    ca_storage(0),
    ca_ndim(0),
    ca_flags(0),
    ca_byte_offset_base(0),
    ca_inval_bit_pos_base(0),
    ca_dim_size(),
    ca_axis_value(),
    ca_cycle_count()
{
    id = expectedId();
}

void Cablock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, ca_composition);
    std::vector<LINK> remainingLinks;
    remainingLinks.resize(link_count - 1);
    for (LINK & i : remainingLinks) {
        readLittleEndian(file.fs, i);
    }

    /* read Data section */
    readLittleEndian(file.fs, ca_type);
    readLittleEndian(file.fs, ca_storage);
    readLittleEndian(file.fs, ca_ndim);
    readLittleEndian(file.fs, ca_flags);
    readLittleEndian(file.fs, ca_byte_offset_base);
    readLittleEndian(file.fs, ca_inval_bit_pos_base);
    ca_dim_size.resize(ca_ndim);
    for (UINT64 & i : ca_dim_size) {
        readLittleEndian(file.fs, i);
    }

    /* calculate sum and product */
    UINT64 sum = 0;
    UINT64 product = 1;
    for (unsigned int i = 0; i < ca_ndim; ++i) {
        sum += ca_dim_size[i];
        product *= ca_dim_size[i];
    }

    /* read remaining Data section */
    if (ca_flags & (1 << 5)) { // if "fixed axes" flag is set
        ca_axis_value.resize(sum);
        for (REAL & i : ca_axis_value) {
            readLittleEndian(file.fs, i);
        }
    }
    if ((ca_storage == 1) || (ca_storage == 2)) { // for storage types "CG/DG template"
        ca_cycle_count.resize(product);
        for (UINT64 & i : ca_cycle_count) {
            readLittleEndian(file.fs, i);
        }
    }

    /* assign remaining links */
    UINT16 n = 0;
    if (ca_storage == 2) { // for storage type "DG template"
        ca_data.resize(product);
        ca_data.assign(remainingLinks.begin() + n, remainingLinks.begin() + n + ca_data.size());
        n += product;
    }
    if (ca_flags & (1 << 0)) { // if "dynamic size" flag is set
        ca_dynamic_size.resize(ca_ndim);
        for (UINT64 i = 0; i < ca_ndim; ++i) {
            ca_dynamic_size[i].dg = remainingLinks[n++];
            ca_dynamic_size[i].cg = remainingLinks[n++];
            ca_dynamic_size[i].cn = remainingLinks[n++];
        }
    }
    if (ca_flags & (1 << 1)) { // if "input quantity" flag is set
        ca_input_quantity.resize(ca_ndim);
        for (UINT64 i = 0; i < ca_ndim; ++i) {
            ca_input_quantity[i].dg = remainingLinks[n++];
            ca_input_quantity[i].cg = remainingLinks[n++];
            ca_input_quantity[i].cn = remainingLinks[n++];
        }
    }
    if (ca_flags & (1 << 2)) { // if "output quantity" flag is set
        ca_output_quantity.resize(1);
        ca_output_quantity[0].dg = remainingLinks[n++];
        ca_output_quantity[0].cg = remainingLinks[n++];
        ca_output_quantity[0].cn = remainingLinks[n++];
    }
    if (ca_flags & (1 << 3)) { // if "comparison quantity" flag is set
        ca_comparison_quantity.resize(1);
        ca_comparison_quantity[0].dg = remainingLinks[n++];
        ca_comparison_quantity[0].cg = remainingLinks[n++];
        ca_comparison_quantity[0].cn = remainingLinks[n++];
    }
    if (ca_flags & (1 << 4)) { // if "axis" flag is set
        ca_cc_axis_conversion.resize(ca_ndim);
        ca_cc_axis_conversion.assign(remainingLinks.begin() + n, remainingLinks.begin() + n + ca_cc_axis_conversion.size());
        n += ca_ndim;
        if (!(ca_flags & (1 << 5))) { // if "fixed axes flag" is not set
            ca_axis.resize(ca_ndim);
            for (UINT64 i = 0; i < ca_ndim; ++i) {
                ca_axis[i].dg = remainingLinks[n++];
                ca_axis[i].cg = remainingLinks[n++];
                ca_axis[i].cn = remainingLinks[n++];
            }
        }
    }
}

void Cablock::write(File & file, LINK & link)
{
    /* pre processing */
    link_count =
        1 + ca_data.size() +
        3 * ca_dynamic_size.size() +
        3 * ca_input_quantity.size() +
        3 * ca_output_quantity.size() +
        3 * ca_comparison_quantity.size() +
        ca_cc_axis_conversion.size() +
        3 * ca_axis.size();
    length =
        expectedMinimumBlockSize(file.id.id_ver) +
        (link_count - 1) * sizeof(LINK) +
        ca_dim_size.size() * sizeof(UINT64) +
        ca_axis_value.size() * sizeof(REAL) +
        ca_cycle_count.size() * sizeof(UINT64);

    /* check */
    if ((ca_type == 1) || (ca_type == 2)) { // for array type "scaling axis" or "interval axis"
        assert(ca_ndim == 1);
    } else {
        assert(ca_ndim > 0);
    }
    if (ca_type >= 3) {
        assert(file.id.id_ver >= 410); // valid since MDF 4.1.0
    }
    if (ca_storage == 2) { // for storage type "DG template"
        assert(ca_data.size() > 0);
    } else {
        assert(ca_data.size() == 0);
    }
    if (ca_flags & (1 << 0)) { // if "dynamic size" flag is set
        assert(ca_dynamic_size.size() == ca_data.size());
        assert((ca_type == 1) || (ca_type == 2)); // for array types "scaling axis" or "look-up"
    } else {
        assert(ca_dynamic_size.size() == 0);
    }
    if (ca_flags & (1 << 1)) { // if "input quantity" flag is set
        assert(ca_input_quantity.size() == ca_data.size());
        assert((ca_type == 1) || (ca_type == 2)); // for array types "scaling axis" or "look-up"
    } else {
        assert(ca_input_quantity.size() == 0);
    }
    if (ca_flags & (1 << 2)) { // if "output quantity" flag is set
        assert(ca_output_quantity.size() == 1);
        assert(ca_type == 2); // for array types "look-up"
    } else {
        assert(ca_output_quantity.size() == 0);
    }
    if (ca_flags & (1 << 3)) { // if "comparison quantity" flag is set
        assert(ca_comparison_quantity.size() == 1);
        assert(ca_type == 2); // for array types "look-up"
    } else {
        assert(ca_comparison_quantity.size() == 0);
    }
    if (ca_flags & (1 << 4)) { // if "axis" flag is set
        assert(ca_cc_axis_conversion.size() == ca_data.size());
        assert((ca_type == 1) || (ca_type == 2) || (ca_type == 4)); // for array types "scaling axis" or "look-up" or "classification result"
    } else {
        assert(ca_cc_axis_conversion.size() == 0);
    }
    if (ca_flags & (1 << 5)) { // if "fixed axis" flag is set
        assert((ca_type == 1) || (ca_type == 2)); // for array types "scaling axis" or "look-up"
    }
    if ((ca_flags & (1 << 4)) && !(ca_flags & (1 << 5))) { // ... and if "fixed axes flag" is not set
        assert(ca_axis.size() == ca_data.size());
    } else {
        assert(ca_axis.size() == 0);
    }
    if (ca_flags & (1 << 5)) { // if "fixed axes flag" is set
        assert((ca_type == 1) || (ca_type == 2)); // for array types "scaling axis" or "look-up"
        assert(ca_axis_value.size() > 0);
    } else {
        assert(ca_axis_value.size() == 0);
    }
    if (ca_flags & (1 << 7)) { // if "left-open interval flag" is set
        assert(file.id.id_ver >= 410); // valid since MDF 4.1.0
    }
    if ((ca_storage == 1) || (ca_storage == 2)) { // for storage type "CG/DG template"
        assert(ca_cycle_count.size() > 0);
    } else {
        assert(ca_cycle_count.size() == 0);
    }

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, ca_composition);
    for(LINK & i : ca_data) {
        writeLittleEndian(file.fs, i);
    }
    for(LinkTriple & i : ca_dynamic_size) {
        writeLittleEndian(file.fs, i.dg);
        writeLittleEndian(file.fs, i.cg);
        writeLittleEndian(file.fs, i.cn);
    }
    for(LinkTriple & i : ca_input_quantity) {
        writeLittleEndian(file.fs, i.dg);
        writeLittleEndian(file.fs, i.cg);
        writeLittleEndian(file.fs, i.cn);
    }
    for(LinkTriple & i : ca_output_quantity) {
        writeLittleEndian(file.fs, i.dg);
        writeLittleEndian(file.fs, i.cg);
        writeLittleEndian(file.fs, i.cn);
    }
    for(LinkTriple & i : ca_comparison_quantity) {
        writeLittleEndian(file.fs, i.dg);
        writeLittleEndian(file.fs, i.cg);
        writeLittleEndian(file.fs, i.cn);
    }
    for(LINK & i : ca_cc_axis_conversion) {
        writeLittleEndian(file.fs, i);
    }
    for(LinkTriple & i : ca_axis) {
        writeLittleEndian(file.fs, i.dg);
        writeLittleEndian(file.fs, i.cg);
        writeLittleEndian(file.fs, i.cn);
    }

    /* write Data section */
    writeLittleEndian(file.fs, ca_type);
    writeLittleEndian(file.fs, ca_storage);
    writeLittleEndian(file.fs, ca_ndim);
    writeLittleEndian(file.fs, ca_flags);
    writeLittleEndian(file.fs, ca_byte_offset_base);
    writeLittleEndian(file.fs, ca_inval_bit_pos_base);
    for(UINT64 & i : ca_dim_size) {
        writeLittleEndian(file.fs, i);
    }
    for(REAL & i : ca_axis_value) {
        writeLittleEndian(file.fs, i);
    }
    for(UINT64 & i : ca_cycle_count) {
        writeLittleEndian(file.fs, i);
    }
}

std::array<CHAR, 4> Cablock::expectedId() const
{
    return {{ '#', '#', 'C', 'A' }};
}

UINT64 Cablock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(ca_composition) +
            sizeof(ca_type) +
            sizeof(ca_storage) +
            sizeof(ca_ndim) +
            sizeof(ca_flags) +
            sizeof(ca_byte_offset_base) +
            sizeof(ca_inval_bit_pos_base);
        break;
    }
    return size;
}

UINT64 Cablock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size = length;
        break;
    }
    return size;
}

}
}
