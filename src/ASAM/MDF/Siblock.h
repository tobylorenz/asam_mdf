/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Source information block (SIBLOCK)
 *
 * Specifies source information for a
 * channel or for the acquisition of a
 * channel group.
 */
class ASAM_MDF_EXPORT Siblock final : public Block
{
public:
    Siblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief si_tx_name
     *
     * Pointer to TXBLOCK with name (identification)
     * of source (can be NIL).
     *
     * The source name must be according to naming
     * rules stated in 5.4.2 Naming Rules.
     */
    LINK si_tx_name;

    /**
     * @brief si_tx_path
     *
     * Pointer to TXBLOCK with (tool-specific) path of
     * source (can be NIL).
     *
     * The path string must be according to naming
     * rules stated in 5.4.2 Naming Rules.
     * Each tool may generate a different path string.
     * The only purpose is to ensure uniqueness as
     * explained in section 5.4.3 Identification of
     * Channels.
     *
     * As a recommendation, the path should be a
     * human readable string containing additional
     * information about the source. However, the path
     * string should not be used to store this
     * information in order to retrieve it later by parsing
     * the string. Instead, additional source information
     * should be stored in generic or custom XML fields
     * in the comment MDBLOCK si_md_comment.
     */
    LINK si_tx_path;

    /**
     * @brief si_md_comment
     *
     * Pointer to source comment and additional
     * information (TXBLOCK or MDBLOCK) (can be
     * NIL)
     */
    LINK si_md_comment;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief si_type
     *
     * Source type
     *
     * additional classification of source:
     *
     * 0 = OTHER source type does not fit into given
     * categories or is unknown
     *
     * 1 = ECU source is an ECU
     *
     * 2 = BUS source is a bus
     * (e.g. for bus monitoring)
     *
     * 3 = I/O source is an I/O device
     * (e.g. analog I/O)
     *
     * 4 = TOOL source is a software tool
     * (e.g. for tool generated
     * signals/events)
     *
     * 5 = USER source is a user interaction/input
     * (e.g. for user generated events)
     */
    UINT8 si_type;

    /**
     * @brief si_bus_type
     *
     * Bus type
     *
     * additional classification of used bus (should be 0
     * for si_type ≥ 3):
     *
     * 0 = NONE no bus
     *
     * 1 = OTHER bus type does not fit into given
     * categories or is unknown
     *
     * 2 = CAN
     *
     * 3 = LIN
     *
     * 4 = MOST
     *
     * 5 = FLEXRAY
     *
     * 6 = K_LINE
     *
     * 7 = ETHERNET
     *
     * 8 = USB
     *
     * Vender defined bus types can be added starting
     * with value 128.
     */
    UINT8 si_bus_type;

    /**
     * @brief si_flags
     *
     * Flags
     *
     * The value contains the following bit flags (Bit 0 =
     * LSB):
     *
     * Bit 0: simulated source
     * Source is only a simulation (can be hardware or
     * software simulated)
     * Cannot be set for si_type = 4 (TOOL).
     */
    UINT8 si_flags;

    /**
     * @brief si_reserved
     *
     * reserved
     */
    std::array<BYTE, 5> si_reserved;

    /** @} */
};

}
}
