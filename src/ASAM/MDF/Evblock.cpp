/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Evblock.h"

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Evblock::Evblock() :
    Block(),
    ev_ev_next(0),
    ev_ev_parent(0),
    ev_ev_range(0),
    ev_tx_name(0),
    ev_md_comment(0),
    ev_scope(),
    ev_at_reference(),
    ev_type(0),
    ev_sync_type(0),
    ev_range_type(0),
    ev_cause(0),
    ev_flags(0),
    ev_reserved(),
    ev_scope_count(0),
    ev_attachment_count(0),
    ev_creator_index(0),
    ev_sync_base_value(0),
    ev_sync_factor(0.0)
{
    id = expectedId();
}

void Evblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, ev_ev_next);
    readLittleEndian(file.fs, ev_ev_parent);
    readLittleEndian(file.fs, ev_ev_range);
    readLittleEndian(file.fs, ev_tx_name);
    readLittleEndian(file.fs, ev_md_comment);
    std::vector<LINK> remainingLinks;
    remainingLinks.resize(link_count - 5);
    for(LINK & i : remainingLinks) {
        readLittleEndian(file.fs, i);
    }

    /* read Data section */
    readLittleEndian(file.fs, ev_type);
    readLittleEndian(file.fs, ev_sync_type);
    readLittleEndian(file.fs, ev_range_type);
    readLittleEndian(file.fs, ev_cause);
    readLittleEndian(file.fs, ev_flags);
    file.fs.read(reinterpret_cast<char *>(ev_reserved.data()), ev_reserved.size());
    readLittleEndian(file.fs, ev_scope_count);
    readLittleEndian(file.fs, ev_attachment_count);
    readLittleEndian(file.fs, ev_creator_index);
    readLittleEndian(file.fs, ev_sync_base_value);
    readLittleEndian(file.fs, ev_sync_factor);

    /* assign remaining links */
    UINT16 n = 0;
    if (ev_scope_count > 0) {
        ev_scope.resize(ev_scope_count);
        ev_scope.assign(remainingLinks.begin() + n, remainingLinks.begin() + n + ev_scope.size());
        n += ev_scope_count;
    }
    if (ev_attachment_count > 0) {
        ev_at_reference.resize(ev_attachment_count);
        ev_at_reference.assign(remainingLinks.begin() + n, remainingLinks.begin() + n + ev_at_reference.size());
        // n += ev_attachment_count;
    }
}

void Evblock::write(File & file, LINK & link)
{
    /* pre processing */
    ev_scope_count = static_cast<UINT32>(ev_scope.size());
    ev_attachment_count = static_cast<UINT16>(ev_at_reference.size());
    link_count = 5 + ev_attachment_count + ev_scope_count;
    length =
        expectedMinimumBlockSize(file.id.id_ver) +
        (link_count - 5) * sizeof(LINK);

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, ev_ev_next);
    writeLittleEndian(file.fs, ev_ev_parent);
    writeLittleEndian(file.fs, ev_ev_range);
    writeLittleEndian(file.fs, ev_tx_name);
    writeLittleEndian(file.fs, ev_md_comment);
    for(LINK & i : ev_scope) {
        writeLittleEndian(file.fs, i);
    }
    for(LINK & i : ev_at_reference) {
        writeLittleEndian(file.fs, i);
    }

    /* write Data section */
    writeLittleEndian(file.fs, ev_type);
    writeLittleEndian(file.fs, ev_sync_type);
    writeLittleEndian(file.fs, ev_range_type);
    writeLittleEndian(file.fs, ev_cause);
    writeLittleEndian(file.fs, ev_flags);
    file.fs.write(reinterpret_cast<char *>(ev_reserved.data()), ev_reserved.size());
    writeLittleEndian(file.fs, ev_scope_count);
    writeLittleEndian(file.fs, ev_attachment_count);
    writeLittleEndian(file.fs, ev_creator_index);
    writeLittleEndian(file.fs, ev_sync_base_value);
    writeLittleEndian(file.fs, ev_sync_factor);
}

std::array<CHAR, 4> Evblock::expectedId() const
{
    return {{ '#', '#', 'E', 'V' }};
}

UINT64 Evblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(ev_ev_next) +
            sizeof(ev_ev_parent) +
            sizeof(ev_ev_range) +
            sizeof(ev_tx_name) +
            sizeof(ev_md_comment) +
            sizeof(ev_type) +
            sizeof(ev_sync_type) +
            sizeof(ev_range_type) +
            sizeof(ev_cause) +
            sizeof(ev_flags) +
            sizeof(ev_reserved) +
            sizeof(ev_scope_count) +
            sizeof(ev_attachment_count) +
            sizeof(ev_creator_index) +
            sizeof(ev_sync_base_value) +
            sizeof(ev_sync_factor);
        break;
    }
    return size;
}

UINT64 Evblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size += (link_count - 5) * sizeof(LINK);
        break;
    }
    return size;
}

}
}
