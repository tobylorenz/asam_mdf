/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Cgblock.h"

#include <cassert>

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Cgblock::Cgblock() :
    Block(),
    cg_cg_next(0),
    cg_cn_first(0),
    cg_tx_acq_name(0),
    cg_si_acq_source(0),
    cg_sr_first(0),
    cg_md_comment(0),
    cg_record_id(0),
    cg_cycle_count(0),
    cg_flags(0),
    cg_path_separator(0),
    cg_reserved(),
    cg_data_bytes(0),
    cg_inval_bytes(0)
{
    id = expectedId();
}

void Cgblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, cg_cg_next);
    readLittleEndian(file.fs, cg_cn_first);
    readLittleEndian(file.fs, cg_tx_acq_name);
    readLittleEndian(file.fs, cg_si_acq_source);
    readLittleEndian(file.fs, cg_sr_first);
    readLittleEndian(file.fs, cg_md_comment);

    /* read Data section */
    readLittleEndian(file.fs, cg_record_id);
    readLittleEndian(file.fs, cg_cycle_count);
    readLittleEndian(file.fs, cg_flags);
    readLittleEndian(file.fs, cg_path_separator);
    file.fs.read(reinterpret_cast<char *>(cg_reserved.data()), cg_reserved.size());
    readLittleEndian(file.fs, cg_data_bytes);
    readLittleEndian(file.fs, cg_inval_bytes);
}

void Cgblock::write(File & file, LINK & link)
{
    /* pre processing */
    link_count = 6;
    length = expectedMinimumBlockSize(file.id.id_ver);

    /* check */
    if (cg_flags & (1 << 1)) {
        assert(file.id.id_ver >= 410); // valid since MDF 4.1.0
    }
    if (cg_flags & (1 << 2)) {
        assert(file.id.id_ver >= 410); // valid since MDF 4.1.0
    }
    if (cg_path_separator != 0) {
        assert(file.id.id_ver >= 410); // valid since MDF 4.1.0
    }

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, cg_cg_next);
    writeLittleEndian(file.fs, cg_cn_first);
    writeLittleEndian(file.fs, cg_tx_acq_name);
    writeLittleEndian(file.fs, cg_si_acq_source);
    writeLittleEndian(file.fs, cg_sr_first);
    writeLittleEndian(file.fs, cg_md_comment);

    /* write Data section */
    writeLittleEndian(file.fs, cg_record_id);
    writeLittleEndian(file.fs, cg_cycle_count);
    writeLittleEndian(file.fs, cg_flags);
    writeLittleEndian(file.fs, cg_path_separator);
    file.fs.write(reinterpret_cast<char *>(cg_reserved.data()), cg_reserved.size());
    writeLittleEndian(file.fs, cg_data_bytes);
    writeLittleEndian(file.fs, cg_inval_bytes);
}

std::array<CHAR, 4> Cgblock::expectedId() const
{
    return {{ '#', '#', 'C', 'G' }};
}

UINT64 Cgblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(cg_cg_next) +
            sizeof(cg_cn_first) +
            sizeof(cg_tx_acq_name) +
            sizeof(cg_si_acq_source) +
            sizeof(cg_sr_first) +
            sizeof(cg_md_comment) +
            sizeof(cg_record_id) +
            sizeof(cg_cycle_count) +
            sizeof(cg_flags) +
            sizeof(cg_path_separator) +
            sizeof(cg_reserved) +
            sizeof(cg_data_bytes) +
            sizeof(cg_inval_bytes);
        break;
    }
    return size;
}

UINT64 Cgblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size += 0;
        break;
    }
    return size;
}

}
}
