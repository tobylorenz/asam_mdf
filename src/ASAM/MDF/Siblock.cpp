/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Siblock.h"

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Siblock::Siblock() :
    Block(),
    si_tx_name(0),
    si_tx_path(0),
    si_md_comment(0),
    si_type(0),
    si_bus_type(0),
    si_flags(0),
    si_reserved()
{
    id = expectedId();
}

void Siblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, si_tx_name);
    readLittleEndian(file.fs, si_tx_path);
    readLittleEndian(file.fs, si_md_comment);

    /* read Data section */
    readLittleEndian(file.fs, si_type);
    readLittleEndian(file.fs, si_bus_type);
    readLittleEndian(file.fs, si_flags);
    file.fs.read(reinterpret_cast<char *>(si_reserved.data()), si_reserved.size());
}

void Siblock::write(File & file, LINK & link)
{
    /* pre processing */
    link_count = 3;
    length = expectedMinimumBlockSize(file.id.id_ver);

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, si_tx_name);
    writeLittleEndian(file.fs, si_tx_path);
    writeLittleEndian(file.fs, si_md_comment);

    /* write Data section */
    writeLittleEndian(file.fs, si_type);
    writeLittleEndian(file.fs, si_bus_type);
    writeLittleEndian(file.fs, si_flags);
    file.fs.write(reinterpret_cast<char *>(si_reserved.data()), si_reserved.size());
}

std::array<CHAR, 4> Siblock::expectedId() const
{
    return {{ '#', '#', 'S', 'I' }};
}

UINT64 Siblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(si_tx_name) +
            sizeof(si_tx_path) +
            sizeof(si_md_comment) +
            sizeof(si_type) +
            sizeof(si_bus_type) +
            sizeof(si_flags) +
            sizeof(si_reserved);
        break;
    }
    return size;
}

UINT64 Siblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size += 0;
        break;
    }
    return size;
}

}
}
