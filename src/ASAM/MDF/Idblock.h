/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"

namespace ASAM {
namespace MDF {

/* forward declarations */
class File;

/**
 * @brief Identification block (IDBLOCK)
 *
 * Identification of the file as MDF file
 */
class ASAM_MDF_EXPORT Idblock final
{
public:
    Idblock();
    virtual ~Idblock();

    /**
     * Read block at position.
     *
     * @param[in] file file
     */
    virtual void read(File & file);

    /**
     * Write block.
     *
     * @param[in] file file
     */
    virtual void write(File & file);

    /**
     * @name File Header
     * @{
     */

    /**
     * @brief id_file
     *
     * File identifier always contains "MDF     "
     * ("MDF" followed by five spaces, no zero
     * termination), except for "unfinalized" MDF files
     * (see 5.5.2 Unfinalized MDF). The file identifier for
     * unfinalized MDF files contains "UnFinMF "
     * ("UnFinMF" followed by one space, no zero
     * termination).
     */
    std::array<CHAR, 8> id_file;

    /**
     * Get (un)finalized identifier from id_file.
     *
     * @return true for finalized, false for unfinalized identifier
     */
    virtual bool finalized();

    /**
     * Update id_file to (un)finalized identifier.
     *
     * This sets id_file to finalized, if id_unfin_flags and
     * id_custom_unfin_flags are both zero. Otherwise it
     * sets it to unfinalized.
     */
    virtual void updateFinalized();

    /**
     * @brief id_vers
     *
     * Format identifier, a textual representation of the
     * format version for display, e.g. "4.10" (including
     * zero termination) or "4.10    " (followed by spaces,
     * no zero termination required if 4 spaces).
     */
    std::array<CHAR, 8> id_vers;

    /**
     * @brief id_prog
     *
     * Program identifier, to identify the program which
     * generated the MDF file (no zero termination
     * required).
     *
     * This program identifier serves only for compatibility
     * with previous MDF format versions. Detailed
     * information about the generating application must
     * be written to the first FHBLOCK referenced by the
     * HDBLOCK.
     *
     * As a recommendation, the program identifier
     * inserted into the 8 characters should be the base
     * name (first 8 characters) of the EXE/DLL of the
     * writing application. Alternatively, also version
     * information of the application can be appended
     * (e.g. "MyApp45" for version 4.5 of MyApp.exe).
     */
    std::array<CHAR, 8> id_prog;

    /**
     * @brief id_reserved1
     *
     * Reserved (must be 0 for compatibility reasons!)
     */
    std::array<BYTE, 4> id_reserved1;

    /**
     * @brief id_ver
     *
     * Version number of the MDF format, i.e. 411 for this
     * version
     */
    UINT16 id_ver;

    /**
     * Set id_vers and id_ver in one step.
     *
     * @param ver Version
     */
    void setVersion(UINT16 ver);

    /**
     * @brief id_reserved2
     *
     * Reserved
     */
    std::array<BYTE, 30> id_reserved2;

    /**
     *
     * @brief id_unfin_flags
     *
     * Standard flags for unfinalized MDF
     *
     * Bit combination of flags that indicate the steps
     * required to finalize the MDF file. For a finalized
     * MDF file, the value must be 0 (no flag set). See
     * also the description in section 5.5.2 Unfinalized
     * MDF and for id_custom_unfin_flags.
     *
     * The bit flags for id_unfin_flags are defined below in
     * Table 13.
     *
     * Note that for the currently defined standard flags,
     * the respective finalization steps must be executed
     * in the following order (given that the respective bit
     * is set in id_unfin_flags):
     *   1. Update DL block (bit 4)
     *   2. Update DT/RD block (bit 2 and bit 3)
     *   3. All other standard steps
     *
     * Custom finalization flags (see id_custom_unfin_
     * flags) also may require a specific order which may
     * be intertwined with the above ordering.
     */
    UINT16 id_unfin_flags;

    /**
     * @brief id_custom_unfin_flags
     *
     * Custom flags for unfinalized MDF
     *
     * Bit combination of flags that indicate custom steps
     * required to finalize the MDF file. For a finalized
     * MDF file, the value must be 0 (no flag set). See
     * also 5.5.2 Unfinalized MDF.
     *
     * Custom flags should only be used to handle cases
     * that are not covered by the (currently known)
     * standard flags in id_unfin_flags. The meaning of
     * the flags depends on the creator tool, i.e. the
     * application that has written the MDF file or
     * executed the most recent modification (see id_prog
     * and last entry in file history). No tool should modify
     * the MDF file (and thus write a new entry to the file
     * history) before it was finalized correctly at least for
     * all custom finalization steps.
     *
     * Finalization should only be done by the creator tool
     * or a tool that is familiar with all custom finalization
     * steps required for a file from this creator tool.
     */
    UINT16 id_custom_unfin_flags;

    /** @} */
};

}
}
