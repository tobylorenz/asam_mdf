/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <vector>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Attachment block (ATBLOCK)
 *
 * Container for binary data or a reference
 * to an external file
 */
class ASAM_MDF_EXPORT Atblock final : public Block
{
public:
    Atblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief at_at_next
     *
     * Link to next ATBLOCK (linked list) (can be
     * NIL)
     */
    LINK at_at_next;

    /**
     * @brief at_tx_filename
     *
     * Link to TXBLOCK with the path and file
     * name of the embedded or referenced file
     * (can only be NIL if data is embedded).
     *
     * The path of the file can be relative or
     * absolute. If relative, it is relative to the
     * directory of the MDF file. If no path is given,
     * the file must be in the same directory as the
     * MDF file.
     */
    LINK at_tx_filename;

    /**
     * @brief at_tx_mimetype
     *
     * LINK to TXBLOCK with MIME content-type
     * text that gives information about the attached
     * data. Can be NIL if the content-type is
     * unknown, but should be specified whenever
     * possible.
     *
     * The MIME content-type string must be written in lowercase.
     */
    LINK at_tx_mimetype;

    /**
     * @brief at_md_comment
     *
     * Link to MDBLOCK with comment and
     * additional information about the attachment
     * (can be NIL).
     *
     * @note The specification only mentions links to MDBLOCK here.
     * However the Examples/ChannelInfo/AttachmentRef/Vector_AttachmentRef.mf4
     * shows that also links to TX are used.
     */
    LINK at_md_comment;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief at_flags
     *
     * Flags.
     *
     * The value contains the following bit flags (Bit
     * 0 = LSB):
     *
     * Bit 0: Embedded data flag.
     * If set, the attachment data is embedded,
     * otherwise it is contained in an external file
     * referenced by file path and name in
     * at_tx_filename.
     *
     * Bit 1: Compressed embedded data flag.
     * If set, the stream for the embedded data is
     * compressed using the Defalte zip algorithm
     * (see [DEF] and [ZLIB]).
     * Can only be set if "embedded data" flag (bit
     * 0) is set.
     *
     * Bit 2: MD5 check sum valid flag.
     * If set, the at_md5_checksum field contains
     * the MD5 check sum of the data.
     */
    UINT16 at_flags;

    /**
     * @brief at_creator_index
     *
     * Creator index, i.e. zero-based index of
     * FHBLOCK in global list of FHBLOCKs that
     * specifies which application has created this
     * attachment, or changed it most recently.
     */
    UINT16 at_creator_index;

    /**
     * @brief at_reserved
     *
     * Reserved
     */
    std::array<BYTE, 4> at_reserved;

    /**
     * @brief at_md5_checksum
     *
     * 128-bit value for MD5 check sum (of the
     * uncompressed data if data is embedded and
     * compressed). Only valid if "MD5 check sum
     * valid" flag (bit 2) is set.
     */
    std::array<BYTE, 16> at_md5_checksum;

    /**
     * @brief at_original_size
     *
     * Original data size in Bytes, i.e. either for
     * external file or for uncompressed data.
     */
    UINT64 at_original_size;

    /**
     * @brief at_embedded_size
     *
     * Embedded data size N, i.e. number of Bytes
     * for binary embedded data following this
     * element.
     *
     * Must be 0 if external file is referenced.
     */
    UINT64 at_embedded_size;

    /**
     * @brief at_embedded_data
     *
     * Contains binary embedded data (possibly
     * compressed).
     */
    std::vector<BYTE> at_embedded_data;

    /** @} */
};

}
}
