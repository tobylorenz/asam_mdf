/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Sample reduction block (SRBLOCK)
 *
 * Description of reduced/condensed
 * signal values which can be used for
 * preview
 */
class ASAM_MDF_EXPORT Srblock final : public Block
{
public:
    Srblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief sr_sr_next
     *
     * Pointer to next sample reduction block (SRBLOCK)
     * (can be NIL)
     */
    LINK sr_sr_next;

    /**
     * @brief sr_data
     *
     * Pointer to reduction data block with sample
     * reduction records (RDBLOCK or DZBLOCK of this
     * block type) or a data list block (DLBLOCK of
     * reduction data blocks or its HLBLOCK).
     */
    LINK sr_data;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief sr_cycle_count
     *
     * Number of cycles, i.e. number of sample reduction
     * records in the reduction data block.
     */
    UINT64 sr_cycle_count;

    /**
     * @brief sr_interval
     *
     * Length of sample interval > 0 used to calculate the
     * sample reduction records (see explanation below).
     *
     * Unit depends on sr_sync_type.
     */
    REAL sr_interval;

    /**
     * @brief sr_sync_type
     *
     * Sync type
     *
     * 1 = sr_interval contains time interval in seconds
     *
     * 2 = sr_interval contains angle interval in radians
     *
     * 3 = sr_interval contains distance interval in meter
     *
     * 4 = sr_interval contains index interval for record
     * index
     *
     * See also section 5.4.5 Synchronization Domains.
     */
    UINT8 sr_sync_type;

    /**
     * @brief sr_flags
     *
     * Flags
     *
     * The value contains the following bit flags (Bit 0 =
     * LSB):
     *
     * Bit 0: invalidation Bytes flag.
     * If set, the sample reduction record contains
     * invalidation Bytes, i.e. after the three data Byte
     * sections for mean, minimum and maximum values,
     * there is one invalidation Byte section.
     * If not set, the invalidation Bytes are omitted.
     * Must only be set if cg_inval_bytes > 0.
     * If invalidation Bytes are used, and if the invalidation
     * bit of the respective channel is valid (i.e.
     * "invalidation bit valid" flag (bit 1) is set in cn_flags),
     * then a set invalidation bit in the invalidation Bytes of
     * the sample reduction record means that within the
     * sample interval all signal values of the respective
     * channel have been invalid.
     */
    UINT8 sr_flags;

    /**
     * @brief sr_reserved
     *
     * Reserved
     */
    std::array<BYTE, 6> sr_reserved;

    /** @} */
};

}
}
