/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Block.h"

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Block::Block() :
    id(expectedId()),
    reserved(),
    length(0),
    link_count(0)
{
}


Block::~Block()
{
}

void Block::read(File & file, LINK link)
{
    /* seek to link */
    file.fs.seekg(link);

    /* read header section */
    file.fs.read(reinterpret_cast<char *>(id.data()), id.size());
    if (id != expectedId()) {
        throw UnexpectedIdentifier(link);
    }
    file.fs.read(reinterpret_cast<char *>(reserved.data()), reserved.size());
    readLittleEndian(file.fs, length);
    readLittleEndian(file.fs, link_count);
    if (length < expectedMinimumBlockSize(file.id.id_ver)) {
        throw UnexpectedBlockSize(link);
    }
    if (length > expectedBlockSize(file.id.id_ver)) {
        throw UnexpectedBlockSize(link);
    }

    /* read link section */
    // nothing to do here

    /* read data section */
    // nothing to do here
}

void Block::write(File & file, LINK & link)
{
    if (link == 0) {
        /* seek to end of file */
        file.fs.seekp(0, std::ios_base::end);
        link = file.fs.tellp();

        /* write zeros for 8-byte alignment */
        if (link % 8 > 0) {
            file.fs.write("\0\0\0\0\0\0\0\0", 8 - (link % 8));
            link = file.fs.tellp();
        }
    } else {
        /* seek to LINK link */
        file.fs.seekp(link);
    }

    /* write header section */
    file.fs.write(reinterpret_cast<char *>(id.data()), id.size());
    file.fs.write(reinterpret_cast<char *>(reserved.data()), reserved.size());
    writeLittleEndian(file.fs, length);
    writeLittleEndian(file.fs, link_count);

    /* write link section */
    // nothing to do here

    /* write data section */
    // nothing to do here
}

BlockType Block::blockType(File & file, LINK link)
{
    /* seek to link */
    file.fs.seekp(link);

    /* read block identifier */
    uint32_t id;
    readLittleEndian(file.fs, id);

    /* we just combine the four characters in an uint32 and run a switch/case over it */
    switch (id) {
    case ('#' << 0) | ('#' << 8) | ('H' << 16) | ('D' << 24):
        return HD;
    case ('#' << 0) | ('#' << 8) | ('M' << 16) | ('D' << 24):
        return MD;
    case ('#' << 0) | ('#' << 8) | ('T' << 16) | ('X' << 24):
        return TX;
    case ('#' << 0) | ('#' << 8) | ('F' << 16) | ('H' << 24):
        return FH;
    case ('#' << 0) | ('#' << 8) | ('C' << 16) | ('H' << 24):
        return CH;
    case ('#' << 0) | ('#' << 8) | ('A' << 16) | ('T' << 24):
        return AT;
    case ('#' << 0) | ('#' << 8) | ('E' << 16) | ('V' << 24):
        return EV;
    case ('#' << 0) | ('#' << 8) | ('D' << 16) | ('G' << 24):
        return DG;
    case ('#' << 0) | ('#' << 8) | ('C' << 16) | ('G' << 24):
        return CG;
    case ('#' << 0) | ('#' << 8) | ('S' << 16) | ('I' << 24):
        return SI;
    case ('#' << 0) | ('#' << 8) | ('C' << 16) | ('N' << 24):
        return CN;
    case ('#' << 0) | ('#' << 8) | ('C' << 16) | ('C' << 24):
        return CC;
    case ('#' << 0) | ('#' << 8) | ('C' << 16) | ('A' << 24):
        return CA;
    case ('#' << 0) | ('#' << 8) | ('D' << 16) | ('T' << 24):
        return DT;
    case ('#' << 0) | ('#' << 8) | ('S' << 16) | ('R' << 24):
        return SR;
    case ('#' << 0) | ('#' << 8) | ('R' << 16) | ('D' << 24):
        return RD;
    case ('#' << 0) | ('#' << 8) | ('S' << 16) | ('D' << 24):
        return SD;
    case ('#' << 0) | ('#' << 8) | ('D' << 16) | ('L' << 24):
        return DL;
    case ('#' << 0) | ('#' << 8) | ('D' << 16) | ('Z' << 24):
        return DZ;
    case ('#' << 0) | ('#' << 8) | ('H' << 16) | ('L' << 24):
        return HL;
    default:
        break;
    }

    return Unknown;
}

std::array<CHAR, 4> Block::expectedId() const
{
    return {{ '#', '#', '#', '#' }};
}

UINT64 Block::expectedMinimumBlockSize(UINT16) const
{
    return
        id.size() +
        reserved.size() +
        sizeof(length) +
        sizeof(link_count);
}

UINT64 Block::expectedBlockSize(UINT16 id_ver) const
{
    return expectedMinimumBlockSize(id_ver);
}

}
}
