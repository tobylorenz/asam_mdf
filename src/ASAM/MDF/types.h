/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cstdint>

#include "platform.h"

namespace ASAM {
namespace MDF {

/**
 * 8-bit unsigned integer
 */
using UINT8 = uint8_t;

/**
 * same as UINT8, but only serves for plain data or reserved
 * Bytes
 */
using BYTE = uint8_t;

/**
 * same as UINT8 but each byte representing a character
 * encoded in standard ASCII (ISO-8859-1 Latin character
 * set)
 */
using CHAR = char;

/**
 * 16-bit unsigned integer
 */
using UINT16 = uint16_t;

/**
 * 16-bit signed integer
 */
using INT16 = int16_t;

/**
 * 32-bit unsigned integer
 */
using UINT32 = uint32_t;

/**
 * 32-bit signed integer
 */
using INT32 = int32_t;

/**
 * 64-bit unsigned integer
 */
using UINT64 = uint64_t;

/**
 * 64-bit signed integer
 */
using INT64 = int64_t;

/**
 * Floating-point compliant with IEEE 754, double precision
 * (64 bits) (see [IEEE-FP])
 *
 * An infinite value (e.g. for tabular ranges in conversion rule)
 * can be expressed using the NaNs INFINITY resp.
 * –INFINITY
 */
using REAL = double;

/**
 * 64-bit signed integer, used as byte position within the file.
 * If a LINK is NIL (corresponds to 0), this means the LINK
 * cannot be de-referenced.
 *
 * A link must be a multiple of 8.
 */
using LINK = int64_t;

/**
 * Link Triple
 *
 * In CABLOCK, CHBLOCK and CNBLOCK there are link triples used,
 * which consist of DGBLOCK, CGBLOCK and CNBLOCK.
 */
struct LinkTriple {
    /** DGBLOCK */
    LINK dg;

    /** CGBLOCK */
    LINK cg;

    /** CNBLOCK */
    LINK cn;
};

}
}
