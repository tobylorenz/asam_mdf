/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Compression.h"

#include "Exceptions.h"

namespace ASAM {
namespace MDF {

void deflateData(std::vector<BYTE> & dataOut, std::vector<BYTE> & dataIn, int compressionLevel)
{
    uLong sizeIn = dataIn.size();
    uLong sizeOut = compressBound(sizeIn);
    dataOut.resize(sizeOut);
    int retVal = compress2(
                     reinterpret_cast<Byte *>(dataOut.data()),
                     &sizeOut,
                     reinterpret_cast<Byte *>(dataIn.data()),
                     sizeIn,
                     compressionLevel);
    if (retVal != Z_OK) {
        throw CompressionError(retVal);
    }
    dataOut.resize(sizeOut);
}

void inflateData(std::vector<BYTE> & dataOut, std::vector<BYTE> & dataIn)
{
    uLong sizeIn = dataIn.size();
    uLongf sizeOut = dataOut.size();
    int retVal = uncompress(
                     reinterpret_cast<Bytef *>(dataOut.data()),
                     &sizeOut,
                     reinterpret_cast<Bytef *>(dataIn.data()),
                     sizeIn);
    if (retVal != Z_OK) {
        throw CompressionError(retVal);
    }
}

void transposeData(std::vector<BYTE> & dataOut, std::vector<BYTE> & dataIn, UINT32 nc, UINT32 mc)
{
    /* transpose bytes */
    dataOut.resize(dataIn.size());
    for (UINT32 n = 0; n < nc; ++n) {
        for (UINT32 m = 0; m < mc; ++m) {
            dataOut[n * mc + m] = dataIn[m * nc + n];
        }
    }

    /* copy remaining bytes */
    for (UINT32 i = nc * mc; i < dataIn.size(); ++i) {
        dataOut[i] = dataIn[i];
    }
}

}
}
