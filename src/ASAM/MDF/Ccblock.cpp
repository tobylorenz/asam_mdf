/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Ccblock.h"

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Ccblock::Ccblock() :
    Block(),
    cc_tx_name(0),
    cc_md_unit(0),
    cc_md_comment(0),
    cc_cc_inverse(0),
    cc_ref(),
    cc_type(0),
    cc_precision(0),
    cc_flags(0),
    cc_ref_count(0),
    cc_val_count(0),
    cc_phy_range_min(0.0),
    cc_phy_range_max(0.0),
    cc_val()
{
    id = expectedId();
}

void Ccblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, cc_tx_name);
    readLittleEndian(file.fs, cc_md_unit);
    readLittleEndian(file.fs, cc_md_comment);
    readLittleEndian(file.fs, cc_cc_inverse);
    cc_ref.resize(static_cast<UINT16>(link_count - 4)); // cc_ref_count is UINT16
    for(LINK & i : cc_ref) {
        readLittleEndian(file.fs, i);
    }

    /* read Data section */
    readLittleEndian(file.fs, cc_type);
    readLittleEndian(file.fs, cc_precision);
    readLittleEndian(file.fs, cc_flags);
    readLittleEndian(file.fs, cc_ref_count);
    readLittleEndian(file.fs, cc_val_count);
    readLittleEndian(file.fs, cc_phy_range_min);
    readLittleEndian(file.fs, cc_phy_range_max);
    cc_val.resize(cc_val_count);
    for(REAL & i : cc_val) {
        readLittleEndian(file.fs, i);
    }
}

void Ccblock::write(File & file, LINK & link)
{
    /* pre processing */
    cc_ref_count = static_cast<UINT16>(cc_ref.size());
    cc_val_count = static_cast<UINT16>(cc_val.size());
    link_count = 4 + cc_ref_count;
    length =
        expectedMinimumBlockSize(file.id.id_ver) +
        (link_count - 4) * sizeof(LINK) +
        cc_val.size() * sizeof(REAL);

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, cc_tx_name);
    writeLittleEndian(file.fs, cc_md_unit);
    writeLittleEndian(file.fs, cc_md_comment);
    writeLittleEndian(file.fs, cc_cc_inverse);
    for(LINK & i : cc_ref) {
        writeLittleEndian(file.fs, i);
    }

    /* write Data section */
    writeLittleEndian(file.fs, cc_type);
    writeLittleEndian(file.fs, cc_precision);
    writeLittleEndian(file.fs, cc_flags);
    writeLittleEndian(file.fs, cc_ref_count);
    writeLittleEndian(file.fs, cc_val_count);
    writeLittleEndian(file.fs, cc_phy_range_min);
    writeLittleEndian(file.fs, cc_phy_range_max);
    for(REAL & i : cc_val) {
        writeLittleEndian(file.fs, i);
    }
}

std::array<CHAR, 4> Ccblock::expectedId() const
{
    return {{ '#', '#', 'C', 'C' }};
}

UINT64 Ccblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(cc_tx_name) +
            sizeof(cc_md_unit) +
            sizeof(cc_md_comment) +
            sizeof(cc_cc_inverse) +
            sizeof(cc_type) +
            sizeof(cc_precision) +
            sizeof(cc_flags) +
            sizeof(cc_ref_count) +
            sizeof(cc_val_count) +
            sizeof(cc_phy_range_min) +
            sizeof(cc_phy_range_max);
        break;
    }
    return size;
}

UINT64 Ccblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size = length;
        break;
    }
    return size;
}

}
}
