/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <fstream>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Idblock.h"
#include "Hdblock.h"

namespace ASAM {
namespace MDF {

/* forward declarations */
class Block;

/**
 * Main class
 */
class ASAM_MDF_EXPORT File final
{
public:
    File();
    virtual ~File();

    /** enumeration for openMode */
    enum class OpenMode
    {
        /** Read */
        Read = 0,

        /** Write */
        Write = 1,

        /** Append */
        Append = 2
    };

    /** open mode */
    OpenMode openMode;

    /**
     * open file
     *
     * @param[in] filename file name
     * @param[in] openMode open in read or write mode
     */
    virtual void open(const char * filename, OpenMode openMode = OpenMode::Read);

    /**
     * open file
     *
     * @param[in] filename file name
     * @param[in] openMode open in read or write mode
     */
    virtual void open(const std::string & filename, OpenMode openMode = OpenMode::Read);

    /**
     * is file open?
     *
     * @return true if file is open
     */
    virtual bool is_open() const;

    /**
     * is end-of-file reached?
     *
     * @return true if end-of-file reached
     */
    virtual bool eof() const;

    /** close file */
    virtual void close();

    /** file stream */
    std::fstream fs;

    /** Identification */
    Idblock id;

    /** Header */
    Hdblock hd;
};

}
}
