/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Channel group block (CGBLOCK)
 *
 * Description of a channel group, i.e.
 * channels which are always measured
 * jointly
 */
class ASAM_MDF_EXPORT Cgblock final : public Block
{
public:
    Cgblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief cg_cg_next
     *
     * Pointer to next channel group block
     * (CGBLOCK) (can be NIL)
     */
    LINK cg_cg_next;

    /**
     * @brief cg_cn_first
     *
     * Pointer to first channel block (CNBLOCK)
     * (can be NIL, must be NIL for VLSD
     * CGBLOCK, i.e. if "VLSD channel group" flag
     * (bit 0) is set)
     */
    LINK cg_cn_first;

    /**
     * @brief cg_tx_acq_name
     *
     * Pointer to acquisition name (TXBLOCK)
     * (can be NIL, must be NIL for VLSD
     * CGBLOCK)
     */
    LINK cg_tx_acq_name;

    /**
     * @brief cg_si_acq_source
     *
     * Pointer to acquisition source (SIBLOCK)
     * (can be NIL, must be NIL for VLSD
     * CGBLOCK)
     *
     * See also rules for uniqueness explained in
     * 5.4.3 Identification of Channels.
     */
    LINK cg_si_acq_source;

    /**
     * @brief cg_sr_first
     *
     * Pointer to first sample reduction block
     * (SRBLOCK) (can be NIL, must be NIL for
     * VLSD CGBLOCK)
     */
    LINK cg_sr_first;

    /**
     * @brief cg_md_comment
     *
     * Pointer to comment and additional
     * information (TXBLOCK or MDBLOCK)
     * (can be NIL, must be NIL for VLSD
     * CGBLOCK)
     */
    LINK cg_md_comment;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief cg_record_id
     *
     * Record ID, value must be less than
     * maximum unsigned integer value allowed by
     * dg_rec_id_size in parent DGBLOCK.
     *
     * Record ID must be unique within linked list
     * of CGBLOCKs.
     */
    UINT64 cg_record_id;

    /**
     * @brief cg_cycle_count
     *
     * Number of cycles, i.e. number of samples
     * for this channel group. This specifies the
     * number of records of this type in the data
     * block.
     */
    UINT64 cg_cycle_count;

    /**
     * @brief cg_flags
     *
     * Flags
     *
     * The value contains the following bit flags
     * (Bit 0 = LSB):
     *
     * Bit 0: VLSD channel group flag.
     * If set, this is a "variable length signal data"
     * (VLSD) channel group. See explanation in
     * 5.14.3 Variable Length Signal Data (VLSD)
     * CGBLOCK.
     *
     * Bit 1: Bus event channel group flag
     * If set, this channel group contains
     * information about a bus event, i.e. it
     * contains a structure channel with bit 10 (bus
     * event falg) set in cn_flags.
     * For details please refer to [MDF-BL].
     * valid since MDF 4.1.0, should not be set for
     * earlier versions
     *
     * Bit 2: Plain bus event channel group flag.
     * Only relevant if "bus event channel group"
     * flag (bit 1) is set.
     * If set, this indicates that only the plain bus
     * event is stored in this channel group, but no
     * channels describing the signals transported
     * in the payload of the bus event. If not set, at
     * least one channel for a signal transported in
     * the payload of the bus event (data
     * frame/PDU) must be present.
     * For details please refer to [MDF-BL].
     * valid since MDF 4.1.0, should not be set for
     * earlier versions
     */
    UINT16 cg_flags;

    /**
     * @brief cg_path_separator
     *
     * Value of character to be used as path
     * separator, 0 if no path separator specified.
     * The specified value is the UTF-16 Little
     * Endian encoding of the character (no zero
     * termination).
     *
     * Note: UTF-16 is used instead of UTF-8 to
     * restrict the size to two Bytes.
     *
     * Commonly used characters are:
     * - Dot (.): 0x002E (dec: 46)
     * - Slash (/): 0x002F (dec: 47)
     * - Backslash(\): 0x005c (dec: 92)
     *
     * The path separator character can be
     * specified if one of the following strings (see
     * Table 8) is composed of several parts which
     * are separated by the given character:
     * - group name (gn), group source (gs), and
     *   group path (gp) for this channel group
     * - channel name (cn), channel source (cs),
     *   and channel path (cp) for a channel in
     *   this channel group
     * valid since MDF 4.1.0, should be 0 for
     * earlier versions
     */
    UINT16 cg_path_separator;

    /**
     * @brief cg_reserved
     *
     * Reserved.
     */
    std::array<BYTE, 4> cg_reserved;

    /**
     * @brief cg_data_bytes
     *
     * Normal CGBLOCK:
     * Number of data Bytes (after record ID) used
     * for signal values in record, i.e. size of plain
     * data for each recorded sample of this
     * channel group.
     *
     * VLSD CGBLOCK:
     * Low part of a UINT64 value that specifies
     * the total size in Bytes of all variable length
     * signal values for the recorded samples of
     * this channel group. See explanation for
     * cg_inval_bytes.
     */
    UINT32 cg_data_bytes;

    /**
     * @brief cg_inval_bytes
     *
     * Normal CGBLOCK:
     * Number of additional Bytes for record used
     * for invalidation bits. Can be zero if no
     * invalidation bits are used at all.
     * Invalidation bits may only occur in the
     * specified number of Bytes after the data
     * Bytes, not within the data Bytes that contain
     * the signal values.
     *
     * VLSD CGBLOCK:
     * High part of UINT64 value that specifies the
     * total size in Bytes of all variable length
     * signal values for the recorded samples of
     * this channel group, i.e. the total size in
     * Bytes can be calculated by
     * cg_data_bytes + (cg_inval_bytes << 32)
     * Note: this value does not include the Bytes
     * used to specify the length of each VLSD
     * value!
     */
    UINT32 cg_inval_bytes;

    /** @} */
};

}
}
