/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <vector>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Reduction data block (RDBLOCK)
 *
 * Container for data record triples with
 * result of a sample reduction
 */
class ASAM_MDF_EXPORT Rdblock final : public Block
{
public:
    Rdblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    // empty

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief rd_data
     */
    std::vector<BYTE> rd_data;

    /** @} */
};

}
}
