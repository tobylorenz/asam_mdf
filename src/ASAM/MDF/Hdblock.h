/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Header block (HDBLOCK)
 *
 * General description of the measurement
 * file
 */
class ASAM_MDF_EXPORT Hdblock final : public Block
{
public:
    Hdblock();

    virtual void read(File & file, LINK link = 64) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief hd_dg_first
     *
     * Pointer to the first data group block
     * (DGBLOCK) (can be NIL)
     */
    LINK hd_dg_first;

    /**
     * @brief hd_fh_first
     *
     * Pointer to first file history block (FHBLOCK)
     *
     * There must be at least one FHBLOCK with
     * information about the application which
     * created the MDF file.
     */
    LINK hd_fh_first;

    /**
     * @brief hd_ch_first
     *
     * Pointer to first channel hierarchy block
     * (CHBLOCK) (can be NIL).
     */
    LINK hd_ch_first;

    /**
     * @brief hd_at_first
     *
     * Pointer to first attachment block (ATBLOCK)
     * (can be NIL)
     */
    LINK hd_at_first;

    /**
     * @brief hd_ev_first
     *
     * Pointer to first event block (EVBLOCK)
     * (can be NIL)
     */
    LINK hd_ev_first;

    /**
     * @brief hd_md_comment
     *
     * Pointer to the measurement file comment
     * (TXBLOCK or MDBLOCK) (can be NIL)
     */
    LINK hd_md_comment;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief hd_start_time_ns
     *
     * Time stamp at start of measurement in
     * nanoseconds elapsed since 00:00:00
     * 01.01.1970 (UTC time or local time,
     * depending on "local time" flag, see [UTC]).
     * All time stamps for time synchronized master
     * channels or events are always relative to this
     * start time stamp.
     */
    UINT64 hd_start_time_ns;

    /**
     * @brief hd_tz_offset_min
     *
     * Time zone offset in minutes.
     * The value is not necessarily a multiple of 60
     * and can be negative! For the current time
     * zone definitions, it is expected to be in the
     * range [-840,840] min.
     *
     * For example a value of 60 (min) means
     * UTC+1 time zone = Central European Time
     * (CET).
     * Only valid if "time offsets valid" flag is set in
     * time flags.
     */
    INT16 hd_tz_offset_min;

    /**
     * @brief hd_dst_offset_min
     *
     * Daylight saving time (DST) offset in minutes
     * for start time stamp. During the summer
     * months, most regions observe a DST offset
     * of 60 min (1 hour).
     *
     * Only valid if "time offsets valid" flag is set in
     * time flags.
     */
    INT16 hd_dst_offset_min;

    /**
     * @brief hd_time_flags
     *
     * Time flags
     *
     * The value contains the following bit flags (Bit
     * 0 = LSB):
     *
     * Bit 0: Local time flag.
     * If set, the start time stamp in nanoseconds
     * represents the local time instead of the UTC
     * time, In this case, time zone and DST offset
     * must not be considered (time offsets flag
     * must not be set). Should only be used if UTC
     * time is unknown.
     * If the bit is not set (default), the start time
     * stamp represents the UTC time.
     *
     * Bit 1: Time offsets valid flag.
     * If set, the time zone and DST offsets are
     * valid. Must not be set together with "local
     * time" flag (mutually exclusive).
     * If the offsets are valid, the locally displayed
     * time at start of recording can be determined
     * (after conversion of offsets to ns) by
     * Local time = UTC time + time zone offset +
     * DST offset.
     */
    UINT8 hd_time_flags;

    /**
     * @brief hd_time_class
     *
     * Time quality class
     *
     * - 0 = local PC reference time (Default)
     * - 10 = external time source
     * - 16 = external absolute synchronized time
     */
    UINT8 hd_time_class;

    /**
     * @brief hd_flags
     *
     * Flags
     *
     * The value contains the following bit flags (Bit
     * 0 = LSB):
     *
     * Bit 0: Start angle valid flag.
     * If set, the start angle value below is valid.
     *
     * Bit 1: Start distance valid flag.
     * If set, the start distance value below is valid.
     */
    UINT8 hd_flags;

    /**
     * @brief hd_reserved
     *
     * Reserved
     */
    BYTE hd_reserved;

    /**
     * @brief hd_start_angle_rad
     *
     * Start angle in radians at start of
     * measurement
     * (only for angle synchronous measurements)
     * Only valid if "start angle valid" flag is set.
     * All angle values for angle synchronized
     * master channels or events are relative to this
     * start angle.
     */
    REAL hd_start_angle_rad;

    /**
     * @brief hd_start_distance_m
     *
     * Start distance in meters at start of
     * measurement
     * (only for distance synchronous
     * measurements)
     * Only valid if "start distance valid" flag is set.
     * All distance values for distance synchronized
     * master channels or events are relative to this
     * start distance.
     */
    REAL hd_start_distance_m;

    /** @} */
};

}
}
