/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "File.h"

#include "Block.h"
#include "Cablock.h"
#include "Cgblock.h"
#include "Cnblock.h"
#include "Dgblock.h"
#include "Dlblock.h"
#include "Dtblock.h"
#include "Srblock.h"
#include "Exceptions.h"

namespace ASAM {
namespace MDF {

File::File() :
    openMode(OpenMode::Read),
    fs(),
    id(),
    hd()
{
}

File::~File()
{
}

void File::open(const char * filename, OpenMode openMode)
{
    this->openMode = openMode;

    switch (this->openMode) {
    case OpenMode::Read: {
        /* open file */
        fs.open(filename, std::fstream::in | std::fstream::binary);
        if (!fs.is_open()) {
            return;
        }

        /* read identification block */
        id.read(*this);
        // tool should reject reading file due to newer major
        // version number
        if ((id.id_ver / 100) > 4) {
            throw UnsupportedVersion(id.id_ver);
        }
        // tool possibly can read files of an older MDF
        // major version only if implemented separately
        // due to compatibility break.
        if ((id.id_ver / 100) < 4) {
            throw UnsupportedVersion(id.id_ver);
        }

        /* read header block */
        hd.read(*this);
    }
    break;
    case OpenMode::Write: {
        /* open file */
        fs.open(filename, std::fstream::out | std::fstream::binary);
        if (!fs.is_open()) {
            return;
        }

        /* write identification block */
        id.write(*this);

        /* write header block */
        LINK link = 64;
        hd.write(*this, link); // always at position 64
    }
    break;
    case OpenMode::Append: {
        /* open file */
        fs.open(filename, std::fstream::in | std::fstream::app | std::fstream::binary);
        if (!fs.is_open()) {
            return;
        }

        /* read identification block */
        id.read(*this);
        // tool should reject reading file due to newer major
        // version number
        if ((id.id_ver / 100) > 4) {
            throw UnsupportedVersion(id.id_ver);
        }
        // tool possibly can read files of an older MDF
        // major version only if implemented separately
        // due to compatibility break.
        if ((id.id_ver / 100) < 4) {
            throw UnsupportedVersion(id.id_ver);
        }

        /* read header block */
        hd.read(*this);
    }
    break;
    }
}

void File::open(const std::string & filename, OpenMode openMode)
{
    open(filename.c_str(), openMode);
}

bool File::is_open() const
{
    return fs.is_open();
}

bool File::eof() const
{
    return fs.eof();
}

void File::close()
{
    fs.close();
}

}
}
