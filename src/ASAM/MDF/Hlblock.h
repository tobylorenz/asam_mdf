/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Header of list block (HLBLOCK)
 *
 * Header information for linked list of data
 * blocks
 */
class ASAM_MDF_EXPORT Hlblock final : public Block
{
public:
    Hlblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief hl_dl_first
     *
     * Pointer to the first data list block (DLBLOCK)
     */
    LINK hl_dl_first;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief hl_flags
     *
     * Flags
     *
     * The value contains the following bit flags (Bit
     * 0 = LSB):
     *
     * Bit 0: Equal length flag.
     * For the referenced DLBLOCK (and thus for
     * each DLBLOCK in the linked list), the value
     * of the "equal length" flag (bit 0 in dl_flags)
     * must be equal to this flag.
     */
    UINT16 hl_flags;

    /**
     * @brief hl_zip_type
     *
     * Zip algorithm used by DZBLOCKs
     *
     * referenced in the list, i.e. in an DLBLOCK of
     * the link list starting at hl_dl_first.
     *
     * Note: all DZBLOCKs in the list must use the
     * same zip algorithm.
     *
     * For possible values, please refer to
     * dz_zip_type member of DZBLOCK.
     */
    UINT8 hl_zip_type;

    /**
     * @brief hl_reserved
     *
     * Reserved
     */
    std::array<BYTE, 5> hl_reserved;

    /** @} */
};

}
}
