/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Dlblock.h"

#include <cassert>

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Dlblock::Dlblock() :
    Block(),
    dl_dl_next(0),
    dl_data(),
    dl_flags(0),
    dl_reserved(),
    dl_count(0),
    dl_equal_length(),
    dl_offset()
{
    id = expectedId();
}

void Dlblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, dl_dl_next);
    dl_data.resize(link_count - 1);
    for(LINK & i : dl_data) {
        readLittleEndian(file.fs, i);
    }

    /* read Data section */
    readLittleEndian(file.fs, dl_flags);
    file.fs.read(reinterpret_cast<char *>(dl_reserved.data()), dl_reserved.size());
    readLittleEndian(file.fs, dl_count);
    if (dl_flags & (1 << 0)) { // if "equal length" flag is set
        dl_equal_length.resize(1);
        for(UINT64 & i : dl_equal_length) {
            readLittleEndian(file.fs, i);
        }
        dl_offset.resize(0);
    } else { // if "equal length" flag is not set
        dl_equal_length.resize(0);
        dl_offset.resize(dl_count);
        for(UINT64 & i : dl_offset) {
            readLittleEndian(file.fs, i);
        }
    }
}

void Dlblock::write(File & file, LINK & link)
{
    /* pre processing */
    dl_count = static_cast<UINT32>(dl_data.size());
    link_count = 1 + dl_count;
    length =
        expectedMinimumBlockSize(file.id.id_ver) +
        (link_count - 1) * sizeof(LINK) +
        dl_equal_length.size() * sizeof(UINT64) +
        dl_offset.size() * sizeof(UINT64);

    /* check */
    if (dl_flags & (1 << 0)) { // if "equal length" flag is set
        assert(dl_equal_length.size() > 0);
        assert(dl_offset.size() == 0);
    } else {
        assert(dl_equal_length.size() == 0);
        assert(dl_offset.size() == dl_count);
    }

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, dl_dl_next);
    for(LINK & i : dl_data) {
        writeLittleEndian(file.fs, i);
    }

    /* write Data section */
    writeLittleEndian(file.fs, dl_flags);
    file.fs.write(reinterpret_cast<char *>(dl_reserved.data()), dl_reserved.size());
    writeLittleEndian(file.fs, dl_count);
    for(UINT64 & i : dl_equal_length) {
        writeLittleEndian(file.fs, i);
    }
    for(UINT64 & i : dl_offset) {
        writeLittleEndian(file.fs, i);
    }
}

std::array<CHAR, 4> Dlblock::expectedId() const
{
    return {{ '#', '#', 'D', 'L' }};
}

UINT64 Dlblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(dl_dl_next) +
            sizeof(dl_flags) +
            sizeof(dl_reserved) +
            sizeof(dl_count);
        break;
    }
    return size;
}

UINT64 Dlblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size = length;
        break;
    }
    return size;
}

}
}
