/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <cassert>
#include <iostream>

#include "platform.h"

namespace ASAM {
namespace MDF {

#if __cplusplus >= 201402L

template <typename T>
constexpr typename std::enable_if<sizeof(T) == 1, T>::type
byteSwap(T value) noexcept
{
    return value;
}

template <typename T>
constexpr typename std::enable_if<sizeof(T) == 2, T>::type
byteSwap(T value) noexcept
{
    return
        ((value & 0x00FF) << 8) |
        ((value & 0xFF00) >> 8);
}

template <typename T>
constexpr typename std::enable_if<sizeof(T) == 4, T>::type
byteSwap(T value) noexcept
{
    return
        ((value & 0x000000FF) << 24) |
        ((value & 0x0000FF00) <<  8) |
        ((value & 0x00FF0000) >>  8) |
        ((value & 0xFF000000) >> 24);
}

template <typename T>
constexpr typename std::enable_if<sizeof(T) == 8, T>::type
byteSwap(T value) noexcept
{
    return
        ((value & 0xFF00000000000000ull) >> 56) |
        ((value & 0x00FF000000000000ull) >> 40) |
        ((value & 0x0000FF0000000000ull) >> 24) |
        ((value & 0x000000FF00000000ull) >>  8) |
        ((value & 0x00000000FF000000ull) <<  8) |
        ((value & 0x0000000000FF0000ull) << 24) |
        ((value & 0x000000000000FF00ull) << 40) |
        ((value & 0x00000000000000FFull) << 56);
}

#else

template <typename T>
T byteSwap(T value)
{
    switch(sizeof(T)) {
    case 1:
        return value;
    case 2:
        return
            ((value & 0x00FF) << 8) |
            ((value & 0xFF00) >> 8);
    case 4:
        return
            ((value & 0x000000FF) << 24) |
            ((value & 0x0000FF00) <<  8) |
            ((value & 0x00FF0000) >>  8) |
            ((value & 0xFF000000) >> 24);
    case 8:
        return
            ((value & 0xFF00000000000000ull) >> 56) |
            ((value & 0x00FF000000000000ull) >> 40) |
            ((value & 0x0000FF0000000000ull) >> 24) |
            ((value & 0x000000FF00000000ull) >>  8) |
            ((value & 0x00000000FF000000ull) <<  8) |
            ((value & 0x0000000000FF0000ull) << 24) |
            ((value & 0x000000000000FF00ull) << 40) |
            ((value & 0x00000000000000FFull) << 56);
    }
    assert(false);
}

#endif

#if BYTE_ORDER_BIG_ENDIAN == 0

template <typename T>
void readLittleEndian(std::istream & is, T & value)
{
    is.read(reinterpret_cast<char *>(&value), sizeof(T));
}

template <typename T>
void writeLittleEndian(std::ostream & os, T & value)
{
    os.write(reinterpret_cast<char *>(&value), sizeof(T));
}

template <typename T>
void readBigEndian(std::istream & is, T & value)
{
    is.read(reinterpret_cast<char *>(&value), sizeof(T));
    value = byteSwap(value);
}

template <typename T>
void writeBigEndian(std::ostream & os, T & value)
{
    T valueSwapped = byteSwap(value);
    os.write(reinterpret_cast<char *>(&valueSwapped), sizeof(T));
}

#else

template <typename T>
void readLittleEndian(std::istream & is, T & value)
{
    is.read(reinterpret_cast<char *>(&value), sizeof(T));
    value = byteSwap(value);
}

template <typename T>
void writeLittleEndian(std::ostream & os, T & value)
{
    T valueSwapped = byteSwap(value);
    os.write(reinterpret_cast<char *>(&valueSwapped), sizeof(T));
}

template <typename T>
void readBigEndian(std::istream & is, T & value)
{
    is.read(reinterpret_cast<char *>(&value), sizeof(T));
}

template <typename T>
void writeBigEndian(std::ostream & os, T & value)
{
    os.write(reinterpret_cast<char *>(&value), sizeof(T));
}

#endif

}
}
