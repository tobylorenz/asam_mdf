/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <vector>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"
#include "Dtblock.h"
#include "Sdblock.h"
#include "Rdblock.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Data zipped block (DZBLOCK)
 *
 * Container for zipped (compressed) data
 * section of a DT/SD/RD block as
 * replacement of such a block.
 */
class ASAM_MDF_EXPORT Dzblock final : public Block
{
public:
    Dzblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    // empty

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief dz_org_block_type
     *
     * Block type identifier of the original (replaced)
     * data block without the "##" prefix, i.e. either
     * "DT", "SD" or "RD"
     */
    std::array<CHAR, 2> dz_org_block_type;

    /**
     * @brief dz_zip_type
     *
     * Zip algorithm used to compress the data
     * stored in dz_data
     *
     * 0 = Deflate.
     * The Deflate zip algorithm as used in various
     * zip implementations
     * (see [DEF] and [ZLIB])
     *
     * 1 = Transposition + Deflate.
     * Before compression, the data block is
     * transposed as explained in 5.26.2
     * Transposition of Data.
     * Typically only used for sorted data groups
     * and DT or RD block types.
     */
    UINT8 dz_zip_type;

    /**
     * @brief dz_reserved
     *
     * Reserved
     */
    BYTE dz_reserved;

    /**
     * @brief dz_zip_parameter
     *
     * Parameter for zip algorithm. Content and
     * meaning depends on dz_zip_type:
     * For dz_zip_type = 1, the value must be > 1
     * and specifies the number of Bytes used as
     * columns, i.e. usually the length of the record
     * for a sorted data group.
     * Otherwise the value must be 0.
     */
    UINT32 dz_zip_parameter;

    /**
     * @brief dz_org_data_length
     *
     * Length of uncompressed data in Bytes, i.e.
     * length of data section for original data block.
     * For a sorted data group, this should not
     * exceed 222 Byte (4 MByte).
     */
    UINT64 dz_org_data_length;

    /**
     * @brief dz_data_length
     *
     * Length N of compressed data in Bytes, i.e.
     * the number of Bytes stored in dz_data.
     * If dz_org_data_length < dz_data_length + 24
     * the overhead of the compression is higher
     * than the memory saved. In this case, we
     * recommend writing the original data block
     * instead.
     */
    UINT64 dz_data_length;

    /**
     * @brief dz_data
     *
     * Contains compressed binary data for data
     * section of original data block.
     */
    std::vector<BYTE> dz_data;

    /** @} */

    /**
     * Compress the given block.
     *
     * @param[in] dt Reference to Dtblock
     */
    void deflate(Dtblock & dt);

    /**
     * Compress the given block.
     *
     * @param[in] sd Reference to Sdblock
     */
    void deflate(Sdblock & sd);

    /**
     * Compress the given block.
     *
     * @param[in] rd Reference to Rdblock
     */
    void deflate(Rdblock & rd);

    /**
     * Uncompress the given block.
     *
     * @param[out] dt Reference to Dtblock
     */
    void inflate(Dtblock & dt);

    /**
     * Uncompress the given block.
     *
     * @param[out] sd Reference to Sdblock
     */
    void inflate(Sdblock & sd);

    /**
     * Uncompress the given block.
     *
     * @param[out] rd Reference to Rdblock
     */
    void inflate(Rdblock & rd);
};

}
}
