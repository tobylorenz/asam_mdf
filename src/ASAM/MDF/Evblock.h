/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <vector>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Event block (EVBLOCK)
 *
 * Description of an event
 */
class ASAM_MDF_EXPORT Evblock final : public Block
{
public:
    Evblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief ev_ev_next
     *
     * Link to next EVBLOCK (linked list)
     * (can be NIL)
     */
    LINK ev_ev_next;

    /**
     * @brief ev_ev_parent
     *
     * Referencing link to EVBLOCK with parent
     * event (can be NIL).
     *
     * The parent relationship must not contain
     * circular references.
     *
     * The scope of the parent event must be
     * larger than or equal to the scope of the
     * child event. If the child does not define its
     * own scope, then the scope of the parent is
     * used (see ev_scope).
     *
     * Note: in contrast to ev_ev_range, there is
     * no restriction on the type of the parent
     * event. Common use cases are that trigger
     * or interrupt events have a recording
     * (begin) event as parent.
     */
    LINK ev_ev_parent;

    /**
     * @brief ev_ev_range
     *
     * Referencing link to EVBLOCK with event
     * that defines the beginning of a range (can
     * be NIL, must be NIL if ev_range_type ≠
     * 2).
     *
     * The event referenced by ev_ev_range
     * and the current event define the borders
     * of a range. ev_ev_range must define the
     * beginning of the range (i.e. ev_range_type
     * = 1) and the current event its end. This
     * implies the following restrictions:
     *
     * ev_ev_range must have occurred prior to
     * the current event, i.e. the (calculated)
     * synchronization value for ev_ev_range
     * must be smaller than for the current event.
     *
     * Furthermore, both events must have the
     * same event type and sync type and the
     * same parent, i.e. the values of ev_type,
     * ev_sync_type and ev_ev_parent must be
     * equal.
     *
     * In addition, both events must have the
     * same scope, which is achieved by the
     * rule, that the current event must re-use
     * the scope list of ev_ev_range (see
     * explanation for ev_scope). This avoids a
     * duplication of the scope list.
     */
    LINK ev_ev_range;

    /**
     * @brief ev_tx_name
     *
     * Pointer to TXBLOCK with event name
     * (can be NIL)
     *
     * Name must be according to naming rules
     * stated in 5.4.2 Naming Rules.
     *
     * If available, the name of a named trigger
     * condition should be used as event name.
     * Other event types may have individual
     * names or no names.
     */
    LINK ev_tx_name;

    /**
     * @brief ev_md_comment
     *
     * Pointer to TX/MDBLOCK with event
     * comment and additional information, e.g.
     * trigger condition or formatted user
     * comment text (can be NIL)
     */
    LINK ev_md_comment;

    /**
     * @brief ev_scope
     *
     * List of links to channels and channel
     * groups to which the event applies
     * (referencing links to CNBLOCKs or
     * CGBLOCKs). This defines the "scope" of
     * the event.
     *
     * The length of the list is given by
     * ev_scope_count. It can be empty
     * (ev_scope_count = 0).
     *
     * If the record index is used for
     * synchronization (ev_sync_type = 1), then
     * the scope must be less than or equal to
     * one channel group, i.e. all affected
     * channels must be in one channel group.
     *
     * If this event defines the end of a range
     * (ev_range_type = 2) and references the
     * event for the beginning of the range
     * (ev_ev_range ≠ NIL), then the scope list
     * must be empty and this event must use
     * the scope list of the event referenced by
     * ev_ev_range. Thus both events have the
     * same scope.
     *
     * If this event has a parent event
     * (ev_ev_parent ≠ NIL), and if its scope list
     * is empty (and also the one of
     * ev_ev_range ≠ NIL), then the scope list of
     * ev_ev_parent must be used.
     *
     * For all other cases, an empty scope list
     * means that the event has a global scope,
     * i.e. the event applies to the whole file.
     */
    std::vector<LINK> ev_scope;

    /**
     * @brief ev_at_reference
     *
     * List of attachments for this event
     * (references to ATBLOCKs in global linked
     * list of ATBLOCKs).
     *
     * The length of the list is given by
     * ev_attachment_count. It can be empty
     * (ev_attachment_count = 0), i.e. there are
     * no attachments for this event.
     */
    std::vector<LINK> ev_at_reference;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief ev_type
     *
     * Event type
     *
     * 0 = Recording.
     * This event type specifies a recording
     * period, i.e. the first and last time a signal
     * value could theoretically be recorded.
     * Recording events must always define a
     * range, i.e. 1 <= ev_range_type <= 2.
     * All recording events in the MDF file must
     * only occur on exactly one scope level:
     * either all have a global scope, e.g. if file
     * has been created by a single recorder; or
     * all of them have a scope on CG level
     * (ev_scope list must contain links to one or
     * more CGBLOCKs), e.g. after combining
     * two files. Recording events which apply to
     * single channels only (CN level scope)
     * generally is not possible.
     * Within one scope, there can be several
     * recording periods, e.g. if the recording
     * was paused between these periods
     * (although this should be modeled by a
     * recording interrupt event, see below).
     * However, these periods must not overlap.
     *
     * 1 = Recording Interrupt.
     * This event type indicates that the
     * recording has been interrupted. It can only
     * occur within the range of a matching
     * recording period. Like the recording event
     * type, it can only have a global scope or a
     * CG level scope. It should be either on the
     * same scope level as the recording event,
     * or on a lower level, i.e. if the recording
     * events have a global scope, the recording
     * interrupt event can either have a global or
     * a CG level scope.
     * A recording interrupt can be defined as
     * point single event) or as range (pair of
     * events as explained for ev_ev_range). If it
     * defines a point, the interruption should be
     * considered to automatically have finished
     * when the next sample for a signal value
     * for a channel in its scope level occurred.
     * Since a recording interrupt event is closely
     * related to a recording period, its
     * ev_ev_parent should point to the
     * respective "range begin" recording event.
     *
     * 2 = Acquisition Interrupt.
     * This event type indicates that not only the
     * recording, but already the acquisition of
     * the signal values has been interrupted.
     * Except of this, the same rules apply as for
     * the recording interrupt event type.
     *
     * 3 = Start Recording Trigger.
     * This event type specifies an event which
     * started the recording of signal values due
     * to some condition (including user
     * interaction). The trigger condition can be
     * specified in the ev_md_comment
     * MDBLOCK, (see Table 27). Here also a
     * pre and post trigger interval can be
     * specified.
     * A start recording trigger event can only
     * occur as point (ev_range_type = 0), not as
     * range. It usually is closely related to a
     * recording period, i.e. it should have the
     * same scope as the respective recording
     * events (note that due to the pre-trigger
     * interval, the matching "range begin"
     * recording event can be before this event).
     * Here ev_ev_parent may be used to point
     * to the "range begin" recording event. For
     * some other use case, ev_ev_parent
     * instead might point to another trigger
     * event, e.g. if this trigger activated the
     * condition for the start recording trigger.
     *
     * 4 = Stop Recording Trigger.
     * Symmetrically to the "start recording
     * trigger" event type, this event type
     * specifies an event which stopped the
     * recording of signal values due to some
     * condition. The same rules apply as for the
     * start recording trigger event type.
     * Note that the two event types may occur
     * in pairs, but they do not have to.
     *
     * 5 = Trigger.
     * This event type generally specifies an
     * event that occurred due to some condition
     * (except of the special start and stop
     * recording trigger event types). The trigger
     * condition can be specified in the
     * ev_md_comment MDBLOCK, (see Table
     * 27).
     * A trigger event can only occur as point
     * (ev_range_type = 0), not as range.
     *
     * 6 = Marker.
     * This event type specifies a marker for a
     * point or a range. As examples, a marker
     * could be a user-generated comment or an
     * automatically generated bookmark.
     */
    UINT8 ev_type;

    /**
     * @brief ev_sync_type
     *
     * Sync type
     *
     * 1 = calculated synchronization value
     * represents time in seconds
     *
     * 2 = calculated synchronization value
     * represents angle in radians
     *
     * 3 = calculated synchronization value
     * represents distance in meter
     *
     * 4 = calculated synchronization value
     * represents zero-based record index
     *
     * For ev_sync_type < 4, the scope of the
     * event must either be global or it must only
     * contain channel groups (or channels from
     * channel groups) which are in the same
     * synchronization domain, i.e. which contain
     * a (virtual) master channel with matching
     * cn_sync_type. In case of a global scope,
     * the scope automatically is restricted to
     * channel groups which are in the same
     * synchronization domain.
     *
     * For ev_sync_type = 4, the scope of the
     * event must be less than or equal to a
     * single channel group, i.e. the channel
     * group whose record index is used for
     * synchronization.
     *
     * See also 5.4.5 Synchronization Domains.
     */
    UINT8 ev_sync_type;

    /**
     * @brief ev_range_type
     *
     * Range type:
     *
     * 0 = event defines a point
     *
     * 1 = event defines the beginning of a range
     *
     * 2 = event defines the end of a range
     *
     * Point and range are defined in the
     * synchronization domain defined by
     * ev_sync_type.
     */
    UINT8 ev_range_type;

    /**
     * @brief ev_cause
     *
     * Cause of event
     *
     * 0 = OTHER.
     * cause of event is not known or does not fit
     * into given categories.
     *
     * 1 = ERROR.
     * event was caused by some error.
     *
     * 2 = TOOL.
     * event was caused by tool-internal
     * condition, e.g. trigger condition or reconfiguration.
     *
     * 3 = SCRIPT.
     * event was caused by a scripting
     * command.
     *
     * 4 = USER.
     * event was caused directly by user, e.g.
     * user input or some other interaction with
     * GUI.
     */
    UINT8 ev_cause;

    /**
     * @brief ev_flags
     *
     * Flags
     *
     * The value contains the following bit flags
     * (Bit 0 = LSB):
     *
     * Bit 0: Post processing flag.
     * If set, the event has been generated
     * during post processing of the file.
     */
    UINT8 ev_flags;

    /**
     * @brief ev_reserved
     *
     * Reserved
     */
    std::array<BYTE, 3> ev_reserved;

    /**
     * @brief ev_scope_count
     *
     * Length M of ev_scope list. Can be zero.
     */
    UINT32 ev_scope_count;

    /**
     * @brief ev_attachment_count
     *
     * Length N of ev_at_reference list, i.e.
     * number of attachments for this event. Can
     * be zero.
     */
    UINT16 ev_attachment_count;

    /**
     * @brief ev_creator_index
     *
     * Creator index, i.e. zero-based index of
     * FHBLOCK in global list of FHBLOCKs that
     * specifies which application has created or
     * changed this event (e.g. when generating
     * event offline).
     */
    UINT16 ev_creator_index;

    /**
     * @brief ev_sync_base_value
     *
     * Base value for synchronization value.
     *
     * The synchronization value of the event is
     * the product of base value and factor
     * (ev_sync_base_value x ev_sync_factor).
     * See also remark for ev_sync_factor.
     *
     * The calculated synchronization value
     * depends on ev_sync_type:
     *
     * For ev_sync_type < 4, the synchronization
     * value is a time/angle/distance value
     * relative to the respective start value in
     * HDBLOCK. Negative synchronization
     * values can be used for events that
     * occurred before the start value.
     *
     * For ev_sync_type = 4, the synchronization
     * value is the absolute record index in the
     * channel group specified by the scope. The
     * event thus occurred at the same time as
     * the record indicated by the index. In this
     * case, the value must be rounded to an
     * Integer value ≥ 0 and < cg_cycle_count,
     * i.e. it must be less than the number of
     * cycles specified for the channel group.
     *
     * See also 5.4.5 Synchronization Domains.
     */
    INT64 ev_sync_base_value;

    /**
     * @brief ev_sync_factor
     *
     * Factor for event synchronization value.
     *
     * The event synchronization value is the
     * product of base value and factor
     * (ev_sync_base_value x ev_sync_factor).
     * Generally, base value and factor can be
     * chosen arbitrarily, but as recommendation
     * they should be used to reflect the raw
     * value and conversion factor of the master
     * channel to be synchronized with. For
     * instance, assume a time master channel
     * using a 64-bit Integer channel data type to
     * store the raw time value in nanoseconds
     * using a linear conversion with offset 0 and
     * factor 1e-9. In this case, ev_sync_factor
     * could be set to 1e-9 and
     * ev_sync_base_value could store the
     * nanosecond time stamp value. Thus, a
     * higher precision is available than when
     * just specifying the time value in seconds
     * as REAL value.
     *
     * For ev_sync_type = 4, ev_sync_factor
     * generally could be set to 1.0, so that the
     * record index will be given by
     * ev_sync_base_value.
     */
    REAL ev_sync_factor;

    /** @} */
};

}
}
