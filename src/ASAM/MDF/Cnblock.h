/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <vector>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Channel block (CNBLOCK)
 *
 * Description of a channel, i.e. information
 * about the measured signal and how the
 * signal values are stored.
 */
class ASAM_MDF_EXPORT Cnblock final : public Block
{
public:
    Cnblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief cn_cn_next
     *
     * Pointer to next channel block (CNBLOCK)
     * (can be NIL)
     */
    LINK cn_cn_next;

    /**
     * @brief cn_composition
     *
     * Composition of channels: Pointer to channel
     * array block (CABLOCK) or channel block
     * (CNBLOCK) (can be NIL).
     *
     * Details see 5.18 Composition of Channels.
     */
    LINK cn_composition;

    /**
     * @brief cn_tx_name
     *
     * Pointer to TXBLOCK with name (identification)
     * of channel. Name must be according to
     * naming rules stated in 5.4.2 Naming Rules.
     *
     * The combination of name and source name
     * and path (both from SIBLOCK) must be
     * unique within all channels of this channel
     * group. See also 5.4.3 Identification of
     * Channels.
     *
     * Note: Alternative names (e.g. display name)
     * can be stored in MDBLOCK of
     * cn_md_comment, see Table 37.
     */
    LINK cn_tx_name;

    /**
     * @brief cn_si_source
     *
     * Pointer to channel source (SIBLOCK) (can be
     * NIL)
     *
     * Must be NIL for component channels
     * (members of a structure or array elements)
     * because they all must have the same source
     * and thus simply use the SIBLOCK of their
     * parent CNBLOCK (direct child of CGBLOCK).
     *
     * See also 5.4.3 Identification of Channels.
     */
    LINK cn_si_source;

    /**
     * @brief cn_cc_conversion
     *
     * Pointer to the conversion formula (CCBLOCK)
     * (can be NIL, must be NIL for complex channel
     * data types, i.e. for cn_data_type ≥ 10).
     *
     * If the pointer is NIL, this means that a 1:1
     * conversion is used (phys = int).
     */
    LINK cn_cc_conversion;

    /**
     * @brief cn_data
     *
     * Pointer to channel type specific signal data
     *
     * For variable length data channel
     * (cn_type = 1):
     * unique link to signal data block (SDBLOCK
     * or DZBLOCK for this block type) or data list
     * block (DLBLOCK of signal data blocks or its
     * HLBLOCK) or, only for unsorted data
     * groups, referencing link to a VLSD channel
     * group block (CGBLOCK).
     * Can only be NIL if SDBLOCK would be
     * empty.
     *
     * For synchronization channel
     * (cn_type = 4):
     * referencing link to attachment block
     * (ATBLOCK) in global linked list of
     * ATBLOCKs starting at hd_at_first.
     * Cannot be NIL.
     *
     * For maximum length data channel
     * (cn_type = 5):
     * referencing link to channel block
     * (CNBLOCK) in same channel group.
     * Cannot be NIL.
     *
     * Must be NIL for all other channel types.
     * See respective channel type for further details.
     */
    LINK cn_data;

    /**
     * @brief cn_md_unit
     *
     * Pointer to TXBLOCK/MDBLOCK with
     * designation for physical unit of signal data
     * (after conversion) or (only for channel data
     * types "MIME sample" and "MIME stream") to
     * MIME context-type text. (can be NIL).
     *
     * The unit can be used if no conversion rule
     * is specified or to overwrite the unit specified
     * for the conversion rule (e.g. if a conversion
     * rule is shared between channels). If the link
     * is NIL, then the unit from the conversion
     * rule must be used. If the content is an
     * empty string, no unit should be displayed.
     * If an MDBLOCK is used, in addition the AHDO
     * unit definition can be stored, see
     * Table 38.
     * Note: for (virtual) master and
     * synchronization channels the A-HDO
     * definition should be omitted to avoid
     * redundancy. Here the unit is already
     * specified by cn_sync_type of the channel.
     *
     * In case of channel data types "MIME
     * sample" and "MIME stream", the text of the
     * unit must be the content-type text of a
     * MIME type which specifies the content of
     * the values of the channel (either fixed
     * length in record or variable length in
     * SDBLOCK). The MIME content-type string
     * must be written in lowercase, and it must
     * apply to the same rules as defined for
     * at_tx_mimetype in 5.11 The Attachment
     * Block ATBLOCK.
     */
    LINK cn_md_unit;

    /**
     * @brief cn_md_comment
     *
     * Pointer to TXBLOCK/MDBLOCK with
     * comment and additional information about the
     * channel, see Table 37. (can be NIL)
     */
    LINK cn_md_comment;

    /**
     * @brief cn_at_reference
     *
     * List of attachments for this channel
     * (references to ATBLOCKs in global linked list
     * of ATBLOCKs).
     *
     * The length of the list is given by
     * cn_attachment_count. It can be empty
     * (cn_attachment_count = 0), i.e. there are no
     * attachments for this channel.
     *
     * valid since MDF 4.1.0
     */
    std::vector<LINK> cn_at_reference;

    /**
     * @brief cn_default_x
     *
     * Only present if "default X" flag (bit 12) is
     * set.
     *
     * Reference to channel to be preferably used as
     * X axis.
     *
     * The reference is a link triple with pointer to
     * parent DGBLOCK, parent CGBLOCK and
     * CNBLOCK for the channel (none of them must
     * be NIL).
     *
     * The referenced channel does not need to have
     * the same raster nor monotonously increasing
     * values. It can be master channel, e.g. in case
     * several master channels are present. In case
     * of different raster, visualization may depend on
     * the interpolation method used by the tool.
     * In case no default X channel is specified, the
     * tool is free to choose the X axis; usually a
     * master channels would be used.
     *
     * valid since MDF 4.1.0
     */
    std::vector<LinkTriple> cn_default_x;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief cn_type
     *
     * Channel type
     *
     * 0 = fixed length data channel.
     * channel value is contained in record.
     *
     * 1 = variable length data channel
     * also denoted as "variable length signal data"
     * (VLSD) channel.
     * channel value in the record is an unsigned
     * integer value (LE/Intel Byte order) of the
     * specified number of bits. This value is the
     * offset to a variable length signal value in the
     * data section of the SDBLOCK referenced by
     * cn_data. Channel data type and CC rule refer
     * to data in SDBLOCK, not to record data.
     * Signal data must point to an SDBLOCK or a
     * DLBLOCK with list of SDBLOCKs. For
     * unsorted data groups, alternatively it can point
     * to a VLSD CGBLOCK, see explanation in
     * 5.14.3 Variable Length Signal Data (VLSD)
     * CGBLOCK.
     *
     * 2 = master channel for all signals of this group.
     * Must be combined with one of the following
     * sync types: 1,2,3 (see table line for
     * cn_sync_type).
     * In each channel group, not more than one
     * channel can be defined as master or virtual
     * master channel for each possible sync type.
     * Value decoding is equal to a fixed length data
     * channel.
     * Physical values of this channel must return the
     * SI unit (see [SI]) of the respective sync type,
     * with or without CCBLOCK, i.e. seconds for a
     * time master channel.
     * Values of this channel listed over the record
     * index must be strictly monotonic increasing.
     * They are always relative to the respective start
     * value in the HDBLOCK.
     * See also section 5.4.5 Synchronization
     * Domains.
     *
     * 3 = virtual master channel.
     * Like a master channel, except that the channel
     * value is not contained in the record
     * (cn_bit_count must be zero). Instead the
     * physical value must be calculated by feeding
     * the zero-based record index to the conversion
     * rule. The data type of the virtual master
     * channel must be unsigned integer with Little
     * Endian byte order (cn_data_type = 0).
     * Except of this, the same rules apply as for a
     * master channel (cn_type = 2).
     *
     * 4 = synchronization channel.
     * Must be combined with one of the following
     * sync types: 1,2,3,4 (see table line for
     * cn_sync_type).
     * A synchronization channel is used to
     * synchronize the records of this channel group
     * with samples in some other stream, e.g. AVI
     * frames. Physical values of this channel must
     * return the unit of the respective
     * synchronization domain, i.e. seconds for
     * cn_sync_type = 1, or an index value for
     * cn_sync_type = 4. These values are used for
     * synchronization with the stream. Hence, the
     * stream must use the same synchronization
     * domain, i.e. a data row with synchronization
     * values or index-based samples (only for
     * cn_sync_type = 4).
     * The signal data link (cn_data) must refer to an
     * ATBLOCK in the global list of attachments.
     * The ATBLOCK contains the stream data or a
     * reference to a stream file.
     * See also section 5.4.5 Synchronization
     * Domains.
     *
     * 5 = maximum length data channel.
     * also denoted as "maximum length signal data"
     * (MLSD) channel
     * Record contains range with maximum number
     * of Bytes for channel value like for a fixed
     * length data channel (cn_type = 0), but the
     * number of currently valid Bytes is given by the
     * value of a channel referenced by cn_data
     * (denoted as "size signal").
     * Since the size signal defines the number of
     * Bytes to use, its physical values must be
     * Integer values ≥ 0 and <= (cn_bit_count>>3),
     * i.e. its values must not exceed the number of
     * Bytes reserved in the record for this channel.
     * Usually, the size signal should have some
     * Integer data type (cn_data_type <= 3) without
     * conversion rule and without unit.
     * The value of the size signal must be contained
     * in the same record as the current channel, i.e.
     * both channels must be in the same channel
     * group.
     * Note: this channel type must not be used with
     * numeric or CANopen data types, i.e. it can
     * occur only for (6 <= cn_data_type <= 12).
     * valid since MDF 4.1.0, should not occur for
     * earlier versions
     *
     * 6 = virtual data channel.
     * Similar to a virtual master channel the channel
     * value is not contained in the record
     * (cn_bit_count must be zero). Instead the
     * physical value must be calculated by feeding
     * the zero-based record index to the conversion
     * rule. The data type of the virtual master
     * channel must be unsigned integer with Little
     * Endian byte order (cn_data_type = 0).
     * Except of this, the same rules apply as for a
     * fixed length data channel (cn_type = 0).
     * A virtual data channel may be used to specify
     * a channel with constant values without
     * consuming any space in the record. The
     * constant value can be given by the offset of a
     * linear conversion rule with factor equal to zero.
     * valid since MDF 4.1.0, should not occur for
     * earlier versions
     */
    UINT8 cn_type;

    /**
     * @brief cn_sync_type
     *
     * Sync type:
     *
     * - 0 = None (to be used for normal data
     *   channels)
     * - 1 = Time (physical values must be seconds)
     * - 2 = Angle (physical values must be radians)
     * - 3 = Distance (physical values must be meters)
     * - 4 = Index (physical values must be zero-based
     *   index values)
     *
     * See also section 5.4.5 Synchronization
     * Domains.
     */
    UINT8 cn_sync_type;

    /**
     * @brief cn_data_type
     *
     * Channel data type of raw signal value
     *
     * Integer data types:
     * - 0 = unsigned integer (LE Byte order)
     * - 1 = unsigned integer (BE Byte order)
     * - 2 = signed integer (two’s complement) (LE
     *   Byte order)
     * - 3 = signed integer (two’s complement) (BE
     *   Byte order)
     *
     * Floating-point data types:
     * - 4 = IEEE 754 floating-point format (LE Byte
     *   order)
     * - 5 = IEEE 754 floating-point format (BE Byte
     *   order)
     *
     * String data types:
     * - 6 = String (SBC, standard ASCII encoded
     *   (ISO-8859-1 Latin), NULL terminated)
     * - 7 = String (UTF-8 encoded, NULL terminated)
     * - 8 = String (UTF-16 encoded LE Byte order,
     *   NULL terminated)
     * - 9 = String (UTF-16 encoded BE Byte order,
     *   NULL terminated)
     *
     * Complex data types:
     * - 10 = Byte Array with unknown content (e.g.
     *   structure)
     * - 11 = MIME sample (sample is Byte Array with
     *   MIME content-type specified in cn_md_unit)
     * - 12 = MIME stream (all samples of channel
     *   represent a stream with MIME content-type
     *   specified in cn_md_unit)
     * - 13 = CANopen date (Based on 7 Byte
     *   CANopen Date data structure, see Table 35)
     * - 14 = CANopen time (Based on 6 Byte
     *   CANopen Time data structure, see Table 36)
     */
    UINT8 cn_data_type;

    /**
     * @brief cn_bit_offset
     *
     * Bit offset (0-7): first bit (=LSB) of signal value
     * after Byte offset has been applied (see
     * 5.21.4.2 Reading the Signal Value).
     *
     * If zero, the signal value is 1-Byte aligned. A
     * value different to zero is only allowed for
     * Integer data types (cn_data_type <= 3) and if
     * the Integer signal value fits into 8 contiguous
     * Bytes (cn_bit_count + cn_bit_offset <= 64). For
     * all other cases, cn_bit_offset must be zero.
     */
    UINT8 cn_bit_offset;

    /**
     * @brief cn_byte_offset
     *
     * Offset to first Byte in the data record that
     * contains bits of the signal value. The offset is
     * applied to the plain record data, i.e. skipping
     * the record ID.
     */
    UINT32 cn_byte_offset;

    /**
     * @brief cn_bit_count
     *
     * Number of bits for signal value in record
     */
    UINT32 cn_bit_count;

    /**
     * @brief cn_flags
     *
     * Flags
     *
     * The value contains the following bit flags (Bit 0
     * = LSB):
     *
     * Bit 0: All values invalid flag.
     * If set, all values of this channel are invalid. If in
     * addition an invalidation bit is used (bit 1 set),
     * then the value of the invalidation bit must be
     * set (high) for every value of this channel.
     * Must not be set for a master channel (channel
     * types 2 and 3).
     *
     * Bit 1: Invalidation bit valid flag.
     * If set, this channel uses an invalidation bit
     * (position specified by cn_inval_bit_pos).
     * Must not be set if cg_inval_bytes is zero.
     * Must not be set for a master channel (channel
     * types 2 and 3).
     *
     * Bit 2: Precision valid flag.
     * If set, the precision value for display of floating
     * point values specified in cn_precision is valid
     * and overrules a possibly specified precision
     * value of the conversion rule (cc_precision).
     *
     * Bit 3: Value range valid flag.
     * If set, both the minimum and the maximum
     * raw value that occurred for this signal within
     * the samples recorded in this file are known
     * and stored in cn_val_range_min and
     * cn_val_range_max. Otherwise the two fields
     * are not valid.
     * Note: the raw value range can only be
     * expressed for numeric channel data types
     * (cn_data_type <= 5). For all other data types,
     * the flag must not be set.
     *
     * Bit 4: Limit range valid flag.
     * If set, the limits of the signal value are known
     * and stored in cn_limit_min and cn_limit_max.
     * Otherwise the two fields are not valid.
     * (see [MCD-2 MC] keywords
     * LowerLimit/UpperLimit for MEASUREMENT
     * and CHARACTERISTIC)
     * This limit range defines the range of plausible
     * values for this channel and may be used for
     * display a warning.
     * Note: the (extended) limit range can only be
     * expressed for a channel whose conversion
     * rule returns a numeric value (REAL) or which
     * has a numeric channel data type
     * (cn_data_type <= 5). In all other cases, the flag
     * must not be set.
     * Depending on the type of conversion, the limit
     * values are interpreted as physical or raw
     * values. If the conversion rule results in
     * numeric values, the limits must be interpreted
     * as physical values, otherwise (e.g. for verbal /
     * scale conversion) as raw values.
     *
     * Bit 5: Extended limit range valid flag.
     * If set, the extended limits of the signal value
     * are known and stored in cn_limit_ext_min and
     * cn_limit_ext_max. Otherwise the two fields are
     * not valid.
     * (see [MCD-2 MC] keyword
     * EXTENDED_LIMITS)
     * The extended limit range must be larger or
     * equal to the limit range (if valid). Values
     * outside the extended limit range indicate an
     * error during measurement acquisition.
     * The extended limit range can be used for any
     * limits, e.g. physical ranges from analog
     * sensors.
     * See also remarks for "limit range valid" flag (bit
     * 4).
     *
     * Bit 6: Discrete value flag.
     * If set, the signal values of this channel are
     * discrete and must not be interpolated.
     * (see [MCD-2 MC] keyword DISCRETE)
     *
     * Bit 7: Calibration flag.
     * If set, the signal values of this channel
     * correspond to a calibration object, otherwise to
     * a measurement object
     * (see [MCD-2 MC] keywords MEASUREMENT
     * and CHARACTERISTIC)
     *
     * Bit 8: Calculated flag.
     * If set, the values of this channel have been
     * calculated from other channel inputs. (see
     * [MCD-2 MC] keywords VIRTUAL and
     * DEPENDENT_CHARACTERISTIC)
     * In MDBLOCK for cn_md_comment the used
     * input signals and the calculation formula can
     * be documented, see Table 37.
     *
     * Bit 9: Virtual flag.
     * If set, this channel is virtual, i.e. it is simulated
     * by the recording tool. (see [MCD-2 MC]
     * keywords VIRTUAL and
     * VIRTUAL_CHARACTERISTIC)
     * Note: for a virtual measurement according to
     * MCD-2 MC both the "Virtual" flag (bit 9) and
     * the "Calculated" flag (bit 8) should be set.
     *
     * Bit 10: Bus event flag.
     * If set, this channel contains information about
     * a bus event. For details please refer to [MDFBL].
     * valid since MDF 4.1.0, should not be set for
     * earlier versions
     *
     * Bit 11: Monotonous flag.
     * If set, this channel contains only strictly
     * monotonous increasing/decreasing values.
     * The flag is optional.
     * valid since MDF 4.1.0, should not be set for
     * earlier versions
     *
     * Bit 12: Default X axis flag.
     * If set, a channel to be preferably used as X
     * axis is specified by cn_default_x. This is only a
     * recommendation, a tool may choose to use a
     * different X axis.
     * valid since MDF 4.1.0, should not be set for
     * earlier versions
     */
    UINT32 cn_flags;

    /**
     * @brief cn_inval_bit_pos
     *
     * Position of invalidation bit.
     *
     * The invalidation bit can be used to specify if
     * the signal value in the current record is valid or
     * not.
     *
     * Note: the invalidation bit is optional and can
     * only be used if the "invalidation bit valid" flag
     * (bit 1) is set.
     *
     * cn_inval_bit_pos contains both bit and Byte
     * offset for the (single) invalidation bit and
     * specifies its position within the invalidation
     * Bytes of the record. This means that the
     * record ID (if present) and the data Bytes must
     * be skipped before applying the Byte offset
     * (cn_inval_bit_pos >> 3). Within this Byte, the
     * number of the invalidation bit (starting from
     * LSB = 0) is specified by (cn_inval_bit_pos &
     * 0x07).
     *
     * If an invalidation bit is used for a channel, and
     * if the respective bit in the invalidation Bytes of
     * the record is set (value is high), then the signal
     * value of this channel in the record is invalid. In
     * this case, as best practice, the record should
     * contain the most recent valid signal value for
     * this channel, or zero if no valid signal value
     * has occurred yet.
     *
     * For an example please refer to 5.21.4.1
     * Reading the Invalidation Bit.
     */
    UINT32 cn_inval_bit_pos;

    /**
     * @brief cn_precision
     *
     * Precision for display of floating point values.
     * 0xFF means unrestricted precision (infinite).
     * Any other value specifies the number of
     * decimal places to use for display of floating
     * point values.
     *
     * Only valid if "precision valid" flag (bit 2) is set
     */
    UINT8 cn_precision;

    /**
     * @brief cn_reserved
     *
     * Reserved
     */
    BYTE cn_reserved;

    /**
     * @brief cn_attachment_count
     *
     * Length N of cn_at_reference list, i.e. number
     * of attachments for this channel. Can be zero.
     *
     * valid since MDF 4.1.0, should be zero for
     * earlier versions
     */
    UINT16 cn_attachment_count;

    /**
     * @brief cn_val_range_min
     *
     * Minimum signal value that occurred for this
     * signal (raw value)
     *
     * Only valid if "value range valid" flag (bit 3) is
     * set.
     */
    REAL cn_val_range_min;

    /**
     * @brief cn_val_range_max
     *
     * Maximum signal value that occurred for this
     * signal (raw value)
     *
     * Only valid if "value range valid" flag (bit 3) is
     * set.
     */
    REAL cn_val_range_max;

    /**
     * @brief cn_limit_min
     *
     * Lower limit for this signal (physical value for
     * numeric conversion rule, otherwise raw value)
     * Only valid if "limit range valid" flag (bit 4) is set.
     */
    REAL cn_limit_min;

    /**
     * @brief cn_limit_max
     *
     * Upper limit for this signal (physical value for
     * numeric conversion rule, otherwise raw value)
     * Only valid if "limit range valid" flag (bit 4) is set.
     */
    REAL cn_limit_max;

    /**
     * @brief cn_limit_ext_min
     *
     * Lower extended limit for this signal (physical
     * value for numeric conversion rule, otherwise
     * raw value)
     *
     * Only valid if "extended limit range valid" flag
     * (bit 5) is set.
     *
     * If cn_limit_min is valid, cn_limit_min must be
     * larger or equal to cn_limit_ext_min.
     */
    REAL cn_limit_ext_min;

    /**
     * @brief cn_limit_ext_max
     *
     * Upper extended limit for this signal (physical
     * value for numeric conversion rule, otherwise
     * raw value)
     *
     * Only valid if "extended limit range valid" flag
     * (bit 5) is set.
     *
     * If cn_limit_max is valid, cn_limit_max must be
     * less or equal to cn_limit_ext_max.
     */
    REAL cn_limit_ext_max;

    /** @} */
};

/**
 * Data structure for channel data type "CANopen date"
 */
struct cn_data_13 {
    /**
     * - Bit 0 .. Bit 15: Milliseconds (0 .. 59999)
     */
    UINT16 ms;

    /**
     * - Bit 0 .. Bit 5: Minutes (0 .. 59)
     * - Bit 6 .. Bit 7: Reserved
     */
    UINT8 min;

    /**
     * - Bit 0 .. Bit 4: Hours (0 .. 23)
     * - Bit 5 .. Bit 6: Reserved
     * - Bit 7: 0 = Standard time, 1 = Summer time
     */
    UINT8 hour;

    /**
     * - Bit 0 .. Bit 4: Day (1 .. 31)
     * - Bit 5 .. Bit 7: Day of week (1 = Monday ... 7 = Sunday)
     */
    UINT8 day;

    /**
     * - Bit 0 .. Bit 5: Month (1 = January .. 12 = December)
     * - Bit 6 .. Bit 7: Reserved
     */
    UINT8 month;

    /**
     * - Bit 0 .. Bit 6: Year (0 .. 99)
     * - Bit 7: Reserved
     */
    UINT8 year;
};

/**
 * Data structure for channel data type "CANopen time"
 */
struct cn_data_14 {
    /**
     * - Bit 0 .. Bit 27: Number of Milliseconds since midnight of 01.
     *   Jan.1984
     * - Bit 28 .. Bit 31: Reserved
     */
    UINT32 ms;

    /**
     * Bit 0 .. Bit 15: Number of days since 01. Jan.1984 (Can be 0)
     */
    UINT16 days;
};

}
}
