/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <vector>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Channel array block (CABLOCK)
 *
 * Description of an array dependency
 * (N-dimensional matrix of channels with
 * equal data type) including axes.
 */
class ASAM_MDF_EXPORT Cablock final : public Block
{
public:
    Cablock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief ca_composition
     *
     * Array of composed elements: Pointer to a
     * CNBLOCK for array of structures, or to a
     * CABLOCK for array of arrays (can be NIL).
     * If a CABLOCK is referenced, it must use the
     * "CN template" storage type (ca_storage = 0).
     * Details are explained in 5.20.2 Arrays of
     * Composed Signals.
     */
    LINK ca_composition;

    /**
     * @brief ca_data
     *
     * Only present for storage type "DG
     * template" (ca_storage = 2).
     *
     * List of links to either a data block (DTBLOCK
     * or DZBLOCK for this block type) or a data list
     * block (DLBLOCK of data blocks or its
     * HLBLOCK) for each element. A link in this
     * list may only be NIL if the cycle count of the
     * respective element is 0:
     *
     * ca_data[k] = NIL => ca_cycle_count[k] = 0
     *
     * The links are stored line-oriented, i.e.
     * element k uses ca_data[k] (see explanation
     * below).
     *
     * The size of the list must be equal to Π N(d),
     * i.e. to the product of the number of elements
     * per dimension N(d) over all dimensions D.
     *
     * Note: link ca_data[0] must be equal to
     * dg_data link of the parent DGBLOCK.
     */
    std::vector<LINK> ca_data;

    /**
     * @brief ca_dynamic_size
     *
     * Only present if "dynamic size" flag (bit 0)
     * is set.
     *
     * References to channels for size signal of
     * each dimension (can be NIL).
     *
     * Each reference is a link triple with pointer to
     * parent DGBLOCK, parent CGBLOCK and
     * CNBLOCK for the channel (either all three
     * links are assigned or NIL). Thus the links
     * have the following order:
     *
     * - DGBLOCK for size signal of dimension 1
     * - CGBLOCK for size signal of dimension 1
     * - CNBLOCK for size signal of dimension 1
     * - ...
     * - DGBLOCK for size signal of dimension D
     * - CGBLOCK for size signal of dimension D
     * - CNBLOCK for size signal of dimension D
     *
     * The size signal can be used to model arrays
     * whose number of elements per dimension
     * can vary over time. If a size signal is
     * specified for a dimension, the number of
     * elements for this dimension at some point in
     * time is equal to the value of the size signal at
     * this time (i.e. for time-synchronized signals,
     * the size signal value with highest time stamp
     * less or equal to current time stamp). If the
     * size signal has no recorded signal value for
     * this time (yet), assume 0 as size.
     *
     * Since each referenced channel defines the
     * current size of a dimension, its physical
     * values must be Integer values ≥ 0 and <=
     * N(d), i.e. the values must not exceed the
     * maximum number of elements for the
     * respective dimension.
     *
     * Usually, a size signal should have some
     * Integer data type (cn_data_type <= 3) without
     * conversion rule and without unit.
     *
     * Since the size signal and the array signal
     * must be synchronized, their channel groups
     * must contain at least one common master
     * channel type.
     *
     * Note: the "positions" of the array elements
     * are fixed as defined for the maximum
     * allowed array size. They will not change
     * when the array is reduced by a size signal
     * value less than N(d). However, the
     * "removed" array elements must not be
     * considered. For example, assume a max 2x3
     * matrix as in the example below. For roworiented
     * CN template storage, the elements
     * would be stored in the record in following
     * order:
     *
     * a11, a12, a13, a21, a22, a23
     *
     * Assume that the size of the second
     * dimension is reduced from 3 to 2. Then the
     * values of elements a13 and a23 will be
     * undefined and should not be used. However,
     * the other elements stay at the same position
     * (Byte offset).
     *
     * a11, a12, [a13], a21, a22, [a23]
     */
    std::vector<LinkTriple> ca_dynamic_size;

    /**
     * @brief ca_input_quantity
     *
     * Only present if "input quantity" flag (bit 1)
     * is set.
     *
     * Reference to channels for input quantity
     * signal for each dimension (can be NIL).
     *
     * Each reference is a link triple with pointer to
     * parent DGBLOCK, parent CGBLOCK and
     * CNBLOCK for the channel (either all three
     * links are assigned or NIL). Thus the links
     * have the following order:
     *
     * - DGBLOCK for input quantity of dimension 1
     * - CGBLOCK for input quantity of dimension 1
     * - CNBLOCK for input quantity of dimension 1
     * - ...
     * - DGBLOCK for input quantity of dimension D
     * - CGBLOCK for input quantity of dimension D
     * - CNBLOCK for input quantity of dimension D
     *
     * Since the input quantity signal and the array
     * signal must be synchronized, their channel
     * groups must contain at least one common
     * master channel type.
     *
     * The input quantity often is denoted as
     * "working point" because it is used for look-up
     * of a value in the array. See [MCD-2 MC]
     * keyword "InputQuantity".
     *
     * For array type "look-up", the value of the
     * input quantity will be used for looking up a
     * value in the scaling axis of the respective
     * dimension (either axis channel or fixed axis
     * values listed in ca_axis_value list).
     *
     * For array type "scaling axis", the value of the
     * input quantity will be used for looking up a
     * value in the axis itself.
     *
     * The input quantity should have the same
     * physical unit as the axis it is applied to.
     */
    std::vector<LinkTriple> ca_input_quantity;

    /**
     * @brief ca_output_quantity
     *
     * Only present if "output quantity" flag (bit
     * 2) is set.
     *
     * Reference to channel for output quantity (can
     * be NIL).
     *
     * The reference is a link triple with pointer to
     * parent DGBLOCK, parent CGBLOCK and
     * CNBLOCK for the channel (either all three
     * links are assigned or NIL).
     *
     * Since the output quantity signal and the array
     * signal must be synchronized, their channel
     * groups must contain at least one common
     * master channel type.
     *
     * For array type "look-up", the output quantity
     * is the result of the complete look-up (see
     * [MCD-2 MC] keyword RIP_ADDR_W). The
     * output quantity should have the same
     * physical unit as the array elements of the
     * array that references it.
     */
    std::vector<LinkTriple> ca_output_quantity;

    /**
     * @brief ca_comparison_quantity
     *
     * Only present if "comparison quantity"
     * flag (bit 3) is set.
     *
     * Reference to channel for comparison
     * quantity (can be NIL).
     *
     * The reference is a link triple with pointer to
     * parent DGBLOCK, parent CGBLOCK and
     * CNBLOCK for the channel (either all three
     * links are assigned or NIL).
     *
     * Since the comparison quantity signal and the
     * array signal must be synchronized, their
     * channel groups must contain at least one
     * common master channel type.
     *
     * The comparison quantity should have the
     * same physical unit as the array elements.
     * See [MCD-2 MC] keyword
     * COMPARISON_QUANTITY.
     */
    std::vector<LinkTriple> ca_comparison_quantity;

    /**
     * @brief ca_cc_axis_conversion
     *
     * Only present if "axis" flag (bit 4) is set.
     * Pointer to a conversion rule (CCBLOCK) for
     * the axis of each dimension. If a link NIL a 1:1
     * conversion must be used for this axis.
     *
     * If the "fixed axis" flag (Bit 5) is set, the
     * conversion must be applied to the fixed axis
     * values of the respective axis/dimension
     * (ca_axis_value list stores the raw values as
     * REAL). If the link to the CCBLOCK is NIL
     * already the physical values are stored in the
     * ca_axis_value list.
     *
     * If the "fixed axes" flag (Bit 5) is not set, the
     * conversion must be applied to the raw values
     * of the respective axis channel, i.e. it
     * overrules the conversion specified for the
     * axis channel, even if the ca_axis_conversion
     * link is NIL!
     *
     * Note: ca_axis_conversion may reference the
     * same CCBLOCK as referenced by the
     * respective axis channel ("sharing" of
     * CCBLOCK).
     */
    std::vector<LINK> ca_cc_axis_conversion;

    /**
     * @brief ca_axis
     *
     * Only present if "axis" flag (bit 4) is set
     * and "fixed axes flag" (bit 5) is not set.
     * References to channels for axis of respective
     * dimension (can be NIL).
     *
     * Each reference is a link triple with pointer to
     * parent DGBLOCK, parent CGBLOCK and
     * CNBLOCK for the channel (either all three
     * links are assigned or NIL). Thus the links
     * have the following order:
     *
     * - DGBLOCK for axis of dimension 1
     * - CGBLOCK for axis of dimension 1
     * - CNBLOCK for axis of dimension 1
     * - ...
     * - DGBLOCK for axis of dimension D
     * - CGBLOCK for axis of dimension D
     * - CNBLOCK for axis of dimension D
     *
     * Each referenced channel must be an array of
     * type "scaling axis" or "interval axis". For a
     * "scaling axis" the maximum number of
     * elements (ca_dim_size[0] in axis) must not
     * be less than the maximum number of
     * elements of the respective dimension d in the
     * array which references it (ca_dim_size[d-1]).
     * For an "interval axis" the number of elements
     * must be exactly one more than respective
     * dimension d in the "classification result" array
     * (ca_dim_size[d-1]+1).
     *
     * If an axis signal must be synchronized with
     * the array signal (e.g. for dynamic size axis),
     * the channel groups of the axis and the array
     * signal must contain at least one common
     * master channel type. If a fixed axis is stored
     * by an axis channel, its channel group should
     * only contain one cycle.
     *
     * If a reference to an axis is NIL, there is no
     * axis and a zero-based index should be used
     * for axis values.
     */
    std::vector<LinkTriple> ca_axis;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief ca_type
     *
     * Array type (defines semantic of the array)
     *
     * Attention: for some array types certain
     * features are not allowed (see ca_flags)
     *
     * 0 = Array.
     * The array is a simple D-dimensional value
     * array (value block) without axes and without
     * input/output/comparison quantities.
     * (see [MCD-2 MC] keywords VAL_BLK for
     * CHARACTERISTIC or
     * MATRIX_DIM/ARRAY_SIZE for
     * MEASUREMENT)
     *
     * 1 = Scaling axis.
     * The array is a scaling axis (1-dimensional
     * vector), possibly referenced by one or more
     * arrays.
     * If refereced by an array of type "look-up"
     * (see [MCD-2 MC] keywords
     * AXIS_PTS/AXIS_DESCR), the axis itself
     * may have a scaling axis (e.g. CURVE_AXIS)
     * and an own input quantity.
     *
     * 2 = Look-up.
     * The array is a D-dimensional array with axes.
     * It can have input/output/comparison
     * quantities.
     * (see [MCD-2 MC] keywords
     * CURVE/MAP/CUBOID/CUBE_n)
     *
     * 3 = Interval axis.
     * The array is an axis (1-dimensional vector)
     * defining interval ranges as "axis points". It
     * can be referenced by one or more arrays of
     * type "classification result" (ca_type = 4).
     * The interval axis must have at least two
     * elements (ca_dim_size[0] = N(0) > 1) and the
     * physical values of the elements must be
     * strictly monotonous increasing over the
     * element index, i.e.
     * ai < ai+1 for 1 <= i < N(0)
     * In contrast to a scaling axis (ca_type = 1), an
     * interval axis always has one element more
     * than the number of elements for the
     * respective dimension of the classification
     * result array which references it.
     * The elements of the class interval axis define
     * the borders of interval ranges that are seen
     * as axis points.
     * Depending on the "left-open interval" flag (bit
     * 7 in ca_flags), the intervals are defined as
     * ]ao,a1], ]a1,a2], ..., ]aN(0)-1,aN(0)] (flag set)
     * [ao,a1[, [a1,a2[, ..., [aN(0)-1,aN(0)[ (flag not set)
     * valid since MDF 4.1.0, should not occur for
     * earlier versions
     *
     * 4 = Classification result.
     * The array is a D-dimensional array
     * containing classification results. It can have
     * scaling axes (ca_type = 1) or interval axes
     * (ca_type = 3), even mixed.
     * Details about the classification method and
     * parameters can be given as XML tags in
     * cn_md_comment of the parent CN block and
     * the CN blocks of the axes.
     * valid since MDF 4.1.0, should not occur for
     * earlier versions
     */
    UINT8 ca_type;

    /**
     * @brief ca_storage
     *
     * Storage type (defines how the element
     * values are stored)
     *
     * 0 = CN template.
     * Values of all elements of the array are stored
     * in the same record (i.e. all elements are
     * measured together).
     * The parent CNBLOCK defines the first
     * element in the record (k = 0). All other
     * elements are defined by the same
     * CNBLOCK except that the values for
     * cn_byte_offset and cn_inval_bit_pos change
     * for each component (see explanation below).
     *
     * 1 = CG template.
     * Value for each element of the array is stored
     * in a separate record (i.e. elements are stored
     * independently of each other).
     * All records are stored in the same data block
     * referenced by the parent DGBLOCK. As a
     * consequence the data group (and thus the
     * MDF file) is unsorted.
     * All elements have exactly the same record
     * layout specified by the parent CGBLOCK.
     * However, each element uses a different
     * cycle count (given by ca_cycle_count[k]) and
     * a different record ID which must be
     * calculated by "auto-increment" of the record
     * ID of the parent CGBLOCK: cg_record_id +
     * k.
     * Since ca_cycle_count[0] must be equal to
     * cg_cycle_count of the parent CGBLOCK, the
     * parent CNBLOCK of the CABLOCK
     * automatically describes the first array
     * element (k = 0).
     * When sorting a data group, a CABLOCK with
     * "CG template" storage will be converted to a
     * CABLOCK with "DG template" storage.
     *
     * 2 = DG template.
     * Similar to CG template, the value of each
     * element of the array is stored in a separate
     * record (i.e. elements are stored
     * independently of each other). However, the
     * records for each element are stored in
     * separate data blocks referenced by the list of
     * links in ca_data.
     * Similar to "CG template" storage, all
     * elements have exactly the same record
     * layout (defined by the parent CGBLOCK) but
     * a different cycle count (specified by
     * ca_cycle_count[k], see below).
     * Since ca_cycle_count[0] must be equal to
     * cg_cycle_count of the parent CGBLOCK,
     * and ca_data[0] must be equal to dg_data of
     * the parent DGBLOCK, the parent CNBLOCK
     * of the CABLOCK automatically describes the
     * first array element (k = 0).
     */
    UINT8 ca_storage;

    /**
     * @brief ca_ndim
     *
     * Number of dimensions D > 0
     *
     * For array type "scaling axis" or "interval axis",
     * D must be 1.
     */
    UINT16 ca_ndim;

    /**
     * @brief ca_flags
     *
     * Flags
     *
     * The value contains the following bit flags (Bit
     * 0 = LSB):
     *
     * Bit 0: dynamic size flag.
     * If set, the number of scaling points for the
     * array is not fixed but can vary over time.
     * Can only be set for array types "look-up" and
     * "scaling axis".
     *
     * Bit 1: input quantity flag.
     * If set, a channel for the input quantity is
     * specified for each dimension by
     * ca_input_quantity.
     * Can only be set for array types "look-up" and
     * "scaling axis".
     *
     * Bit 2: output quantity flag.
     * If set, a channel for the output quantity is
     * specified by ca_output_quantity.
     * Can only be set for array type "look-up".
     *
     * Bit 3: comparison quantity flag.
     * If set, a channel for the comparison quantity
     * is specified by ca_comparison_quantity.
     * Can only be set for array type "look-up".
     *
     * Bit 4: axis flag.
     * If set, a scaling axis is given for each
     * dimension of the array, either as fixed or as
     * dynamic axis, depending on "fixed axis" flag
     * (bit 5).
     * Can only be set for array types "look-up",
     * "scaling axis" and "classification result"
     *
     * Bit 5: fixed axis flag.
     * If set, the scaling axis is fixed and the axis
     * points are stored as raw values in
     * ca_axis_value list.
     * If not set, the scaling axis may vary over time
     * and the axis points are stored as channel
     * referenced in ca_axis for each dimension.
     * Only relevant if "axis" flag (bit 4) is set.
     * Can only not be set for array types "look-up"
     * and "scaling axis".
     *
     * Bit 6: inverse layout flag.
     * If set, the record layout is "column oriented"
     * instead of "row oriented". See [MCD-2 MC]
     * keywords ROW_DIR and COLUMN_DIR.
     * Only relevant for "CN template" storage type
     * and for number of dimensions > 1.
     *
     * Bit 7: left-open interval flag.
     * If set, the interval ranges for the class
     * interval axes are left-open and right-closed,
     * i.e. ]a,b] = {x | a < x <= b}.
     * If not set, the interval ranges for the class
     * interval axes are left-closed and right-open,
     * i.e. [a,b[ = {x | a <= x < b}.
     * Note that opening or closing is irrelevant for
     * infinite endpoints (there can only be -INF for
     * the left border of the very first interval, or
     * +INF for right border of the very last interval).
     * Only relevant for array type "interval axes".
     * valid since MDF 4.1.0, should not be set for
     * earlier versions
     */
    UINT32 ca_flags;

    /**
     * @brief ca_byte_offset_base
     *
     * Base factor for calculation of Byte offsets for
     * "CN template" storage type.
     *
     * The absolute value of ca_byte_offset_base
     * should be larger than or equal to the size of
     * Bytes required to store a component channel
     * value in the record (all must have the same
     * size). If the values are equal, then the
     * component values are stored contiguously,
     * i.e.next to each other without gaps.
     *
     * If ca_byte_offset_base is negative, the
     * component values are stored with
     * decreasing index, see [MCD-2 MC] keyword
     * INDEX_DECR.
     *
     * Exact formula for calculation of Byte offset
     * for each component channel see below.
     */
    INT32 ca_byte_offset_base;

    /**
     * @brief ca_inval_bit_pos_base
     *
     * Base factor for calculation of invalidation bit
     * positions for CN template storage type.
     *
     * For a simple array which is not part of a
     * nested composition, ca_inval_bit_pos_base
     * = 1 means that the invalidation bits for the
     * component channels are stored without gaps
     * and ca_inval_bit_pos_base = 0 means that
     * all component channels use the same
     * invalidation bit in the record.
     *
     * Exact formula for calculation of invalidation
     * bit position for each component channel see
     * below.
     */
    UINT32 ca_inval_bit_pos_base;

    /**
     * @brief ca_dim_size
     *
     * Maximum number of elements for each
     * dimension N(d).
     *
     * N(d) > 0 for all d.
     *
     * For array type "interval axis", there must be
     * at least two elements, i.e. N(0) > 1.
     */
    std::vector<UINT64> ca_dim_size;

    /**
     * @brief ca_axis_value
     *
     * Only present if "fixed axes" flag (bit 5) is
     * set.
     *
     * List of raw values for (fixed) axis points of
     * each axis.
     *
     * The axis points are stored in a linear
     * sequence for each dimension:
     *
     * - A1(1), A1(2), ..., A1(N(1)),
     * - A2(1), A2(2), ..., A2(N(2)),
     * - ...
     * - AD(1), AD(2), ..., AD(N(D))
     *
     * where Ad(j) is axis point j of dimension d
     * and N(d) is the number of elements for
     * dimension d.
     *
     * Example for D = 2 dimensions (as explained
     * later Y axis for A1 and X axis for A2):
     *
     * - Y(1), Y(2), ..., Y(N(1)),
     * - X(1), X(2), ..., X(N(2))
     *
     * The size of the list must be equal to Σ N(d) =
     * the sum of the number of elements per
     * dimension N(d) over all dimensions D.
     * To convert the raw axis values to physical
     * values, the conversion rule of the respective
     * dimension must be applied (CCBLOCK
     * referenced by ca_cc_axis_conversion[i]). If
     * the CCBLOCK link of a dimension is NIL, the
     * raw values are equal to the physical values
     * (1:1 conversion).
     */
    std::vector<REAL> ca_axis_value;

    /**
     * @brief ca_cycle_count
     *
     * Only present for storage types "CG/DG
     * template".
     *
     * List of cycle counts for each element in case
     * of "CG/DG template" storage.
     *
     * The offsets are stored line-oriented, i.e.
     * element k uses ca_cycle_count[k] (see
     * explanation below)
     *
     * The size of the list must be equal to Π N(d) =
     * the product of the number of elements per
     * dimension N(d) over all dimensions D.
     *
     * Note: ca_cycle_count[0] must be equal to
     * cg_cycle_count of parent CGBLOCK!
     */
    std::vector<UINT64> ca_cycle_count;

    /** @} */
};

}
}
