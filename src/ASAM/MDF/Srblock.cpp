/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Srblock.h"

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Srblock::Srblock() :
    Block(),
    sr_sr_next(0),
    sr_data(0),
    sr_cycle_count(0),
    sr_interval(0.0),
    sr_sync_type(0),
    sr_flags(0),
    sr_reserved()
{
    id = expectedId();
}

void Srblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, sr_sr_next);
    readLittleEndian(file.fs, sr_data);

    /* read Data section */
    readLittleEndian(file.fs, sr_cycle_count);
    readLittleEndian(file.fs, sr_interval);
    readLittleEndian(file.fs, sr_sync_type);
    readLittleEndian(file.fs, sr_flags);
    file.fs.read(reinterpret_cast<char *>(sr_reserved.data()), sr_reserved.size());
}

void Srblock::write(File & file, LINK & link)
{
    /* pre processing */
    link_count = 2;
    length = expectedMinimumBlockSize(file.id.id_ver);

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, sr_sr_next);
    writeLittleEndian(file.fs, sr_data);

    /* write Data section */
    writeLittleEndian(file.fs, sr_cycle_count);
    writeLittleEndian(file.fs, sr_interval);
    writeLittleEndian(file.fs, sr_sync_type);
    writeLittleEndian(file.fs, sr_flags);
    file.fs.write(reinterpret_cast<char *>(sr_reserved.data()), sr_reserved.size());
}

std::array<CHAR, 4> Srblock::expectedId() const
{
    return {{ '#', '#', 'S', 'R' }};
}

UINT64 Srblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(sr_sr_next) +
            sizeof(sr_data) +
            sizeof(sr_cycle_count) +
            sizeof(sr_interval) +
            sizeof(sr_sync_type) +
            sizeof(sr_flags) +
            sizeof(sr_reserved);
        break;
    }
    return size;
}

UINT64 Srblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size += 0;
        break;
    }
    return size;
}

}
}
