/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <iostream>

#define BOOST_TEST_MODULE MinimumBlockSize
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <ASAM/MDF.h>

BOOST_AUTO_TEST_CASE(Atblock)
{
    ASAM::MDF::Atblock atblock;
    BOOST_CHECK(atblock.expectedMinimumBlockSize(400) == 96);
    BOOST_CHECK(atblock.expectedMinimumBlockSize(410) == 96);
    BOOST_CHECK(atblock.expectedMinimumBlockSize(411) == 96);
}

BOOST_AUTO_TEST_CASE(Block)
{
    ASAM::MDF::Block block;
    BOOST_CHECK(block.expectedMinimumBlockSize(400) == 24);
    BOOST_CHECK(block.expectedMinimumBlockSize(410) == 24);
    BOOST_CHECK(block.expectedMinimumBlockSize(411) == 24);
    BOOST_CHECK(block.expectedBlockSize(400) == 24);
    BOOST_CHECK(block.expectedBlockSize(410) == 24);
    BOOST_CHECK(block.expectedBlockSize(411) == 24);
}

BOOST_AUTO_TEST_CASE(Cablock)
{
    ASAM::MDF::Cablock cablock;
    BOOST_CHECK(cablock.expectedMinimumBlockSize(400) == 48);
    BOOST_CHECK(cablock.expectedMinimumBlockSize(410) == 48);
    BOOST_CHECK(cablock.expectedMinimumBlockSize(411) == 48);
}

BOOST_AUTO_TEST_CASE(Ccblock)
{
    ASAM::MDF::Ccblock ccblock;
    BOOST_CHECK(ccblock.expectedMinimumBlockSize(400) == 80);
    BOOST_CHECK(ccblock.expectedMinimumBlockSize(410) == 80);
    BOOST_CHECK(ccblock.expectedMinimumBlockSize(411) == 80);
}

BOOST_AUTO_TEST_CASE(Cgblock)
{
    ASAM::MDF::Cgblock cgblock;
    BOOST_CHECK(cgblock.expectedMinimumBlockSize(400) == 104);
    BOOST_CHECK(cgblock.expectedMinimumBlockSize(410) == 104);
    BOOST_CHECK(cgblock.expectedMinimumBlockSize(411) == 104);
}

BOOST_AUTO_TEST_CASE(Chblock)
{
    ASAM::MDF::Chblock chblock;
    BOOST_CHECK(chblock.expectedMinimumBlockSize(400) == 64);
    BOOST_CHECK(chblock.expectedMinimumBlockSize(410) == 64);
    BOOST_CHECK(chblock.expectedMinimumBlockSize(411) == 64);
}

BOOST_AUTO_TEST_CASE(Cnblock)
{
    ASAM::MDF::Cnblock cnblock;
    BOOST_CHECK(cnblock.expectedMinimumBlockSize(400) == 160);
    BOOST_CHECK(cnblock.expectedMinimumBlockSize(410) == 160);
    BOOST_CHECK(cnblock.expectedMinimumBlockSize(411) == 160);
}

BOOST_AUTO_TEST_CASE(Dgblock)
{
    ASAM::MDF::Dgblock dgblock;
    BOOST_CHECK(dgblock.expectedMinimumBlockSize(400) == 64);
    BOOST_CHECK(dgblock.expectedMinimumBlockSize(410) == 64);
    BOOST_CHECK(dgblock.expectedMinimumBlockSize(411) == 64);
}

BOOST_AUTO_TEST_CASE(Dlblock)
{
    ASAM::MDF::Dlblock dlblock;
    BOOST_CHECK(dlblock.expectedMinimumBlockSize(400) == 40);
    BOOST_CHECK(dlblock.expectedMinimumBlockSize(410) == 40);
    BOOST_CHECK(dlblock.expectedMinimumBlockSize(411) == 40);
}

BOOST_AUTO_TEST_CASE(Dtblock)
{
    ASAM::MDF::Dtblock dtblock;
    BOOST_CHECK(dtblock.expectedMinimumBlockSize(400) == 24);
    BOOST_CHECK(dtblock.expectedMinimumBlockSize(410) == 24);
    BOOST_CHECK(dtblock.expectedMinimumBlockSize(411) == 24);
}

BOOST_AUTO_TEST_CASE(Dzblock)
{
    ASAM::MDF::Dzblock dzblock;
    BOOST_CHECK(dzblock.expectedMinimumBlockSize(410) == 48);
    BOOST_CHECK(dzblock.expectedMinimumBlockSize(411) == 48);
}

BOOST_AUTO_TEST_CASE(Evblock)
{
    ASAM::MDF::Evblock evblock;
    BOOST_CHECK(evblock.expectedMinimumBlockSize(400) == 96);
    BOOST_CHECK(evblock.expectedMinimumBlockSize(410) == 96);
    BOOST_CHECK(evblock.expectedMinimumBlockSize(411) == 96);
}

BOOST_AUTO_TEST_CASE(Fhblock)
{
    ASAM::MDF::Fhblock fhblock;
    BOOST_CHECK(fhblock.expectedMinimumBlockSize(400) == 56);
    BOOST_CHECK(fhblock.expectedMinimumBlockSize(410) == 56);
    BOOST_CHECK(fhblock.expectedMinimumBlockSize(411) == 56);
}

BOOST_AUTO_TEST_CASE(Hdblock)
{
    ASAM::MDF::Hdblock hdblock;
    BOOST_CHECK(hdblock.expectedMinimumBlockSize(400) == 104);
    BOOST_CHECK(hdblock.expectedMinimumBlockSize(410) == 104);
    BOOST_CHECK(hdblock.expectedMinimumBlockSize(411) == 104);
}

BOOST_AUTO_TEST_CASE(Hlblock)
{
    ASAM::MDF::Hlblock hlblock;
    BOOST_CHECK(hlblock.expectedMinimumBlockSize(410) == 40);
    BOOST_CHECK(hlblock.expectedMinimumBlockSize(411) == 40);
}

BOOST_AUTO_TEST_CASE(Mdblock)
{
    ASAM::MDF::Mdblock mdblock;
    BOOST_CHECK(mdblock.expectedMinimumBlockSize(400) == 24);
    BOOST_CHECK(mdblock.expectedMinimumBlockSize(410) == 24);
    BOOST_CHECK(mdblock.expectedMinimumBlockSize(411) == 24);
}

BOOST_AUTO_TEST_CASE(Rdblock)
{
    ASAM::MDF::Rdblock rdblock;
    BOOST_CHECK(rdblock.expectedMinimumBlockSize(400) == 24);
    BOOST_CHECK(rdblock.expectedMinimumBlockSize(410) == 24);
    BOOST_CHECK(rdblock.expectedMinimumBlockSize(411) == 24);
}

BOOST_AUTO_TEST_CASE(Sdblock)
{
    ASAM::MDF::Sdblock sdblock;
    BOOST_CHECK(sdblock.expectedMinimumBlockSize(400) == 24);
    BOOST_CHECK(sdblock.expectedMinimumBlockSize(410) == 24);
    BOOST_CHECK(sdblock.expectedMinimumBlockSize(411) == 24);
}

BOOST_AUTO_TEST_CASE(Siblock)
{
    ASAM::MDF::Siblock siblock;
    BOOST_CHECK(siblock.expectedMinimumBlockSize(400) == 56);
    BOOST_CHECK(siblock.expectedMinimumBlockSize(410) == 56);
    BOOST_CHECK(siblock.expectedMinimumBlockSize(411) == 56);
}

BOOST_AUTO_TEST_CASE(Srblock)
{
    ASAM::MDF::Srblock srblock;
    BOOST_CHECK(srblock.expectedMinimumBlockSize(400) == 64);
    BOOST_CHECK(srblock.expectedMinimumBlockSize(410) == 64);
    BOOST_CHECK(srblock.expectedMinimumBlockSize(411) == 64);
}

BOOST_AUTO_TEST_CASE(Txblock)
{
    ASAM::MDF::Txblock txblock;
    BOOST_CHECK(txblock.expectedMinimumBlockSize(400) == 24);
    BOOST_CHECK(txblock.expectedMinimumBlockSize(410) == 24);
    BOOST_CHECK(txblock.expectedMinimumBlockSize(411) == 24);
}
