/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>

#define BOOST_TEST_MODULE Blocks
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <ASAM/MDF.h>

static bool isEqual(double a, double b, double precision = 0.000000001) // precision is nano (10^-9)
{
    return ((a - b) < precision) && ((b - a) < precision);
}

BOOST_AUTO_TEST_CASE(Atblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/at_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Atblock at;
    at.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(at.id[0] == '#');
    BOOST_REQUIRE(at.id[1] == '#');
    BOOST_REQUIRE(at.id[2] == 'A');
    BOOST_REQUIRE(at.id[3] == 'T');
    for (auto value : at.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(at.length == 0x000000000000021b);
    // 0x0010
    BOOST_CHECK(at.link_count == 0x0000000000000004);
    BOOST_CHECK(at.at_at_next == 0x0000000000006b08);
    // 0x0020
    BOOST_CHECK(at.at_tx_filename == 0x0000000000006670);
    BOOST_CHECK(at.at_tx_mimetype == 0x0000000000006698);
    // 0x0030
    BOOST_CHECK(at.at_md_comment == 0x00000000000066c0);
    BOOST_CHECK(at.at_flags == 0x0005);
    BOOST_CHECK(at.at_creator_index == 0x0001);
    for (auto value : at.at_reserved) {
        BOOST_CHECK(value == 0x00);
    }
    // 0x0040
    BOOST_CHECK(at.at_md5_checksum[0x00] == 0xdd);
    BOOST_CHECK(at.at_md5_checksum[0x01] == 0x66);
    BOOST_CHECK(at.at_md5_checksum[0x02] == 0xb0);
    BOOST_CHECK(at.at_md5_checksum[0x03] == 0x36);
    BOOST_CHECK(at.at_md5_checksum[0x04] == 0xa4);
    BOOST_CHECK(at.at_md5_checksum[0x05] == 0x25);
    BOOST_CHECK(at.at_md5_checksum[0x06] == 0xe1);
    BOOST_CHECK(at.at_md5_checksum[0x07] == 0x7a);
    BOOST_CHECK(at.at_md5_checksum[0x08] == 0x9a);
    BOOST_CHECK(at.at_md5_checksum[0x09] == 0xb8);
    BOOST_CHECK(at.at_md5_checksum[0x0a] == 0x43);
    BOOST_CHECK(at.at_md5_checksum[0x0b] == 0x33);
    BOOST_CHECK(at.at_md5_checksum[0x0c] == 0x5a);
    BOOST_CHECK(at.at_md5_checksum[0x0d] == 0x1c);
    BOOST_CHECK(at.at_md5_checksum[0x0e] == 0x3a);
    BOOST_CHECK(at.at_md5_checksum[0x0f] == 0xbc);
    // 0x0050
    BOOST_CHECK(at.at_original_size == 0x00000000000001bb);
    BOOST_CHECK(at.at_embedded_size == 0x00000000000001bb);
    // 0x0060
    BOOST_REQUIRE(at.at_embedded_data.size() == 0x01bb); // 0x021b - 0x0060
    BOOST_CHECK(at.at_embedded_data[0x00] == 0x41);
    BOOST_CHECK(at.at_embedded_data[0x01] == 0x74);
    BOOST_CHECK(at.at_embedded_data[0x02] == 0x74);
    BOOST_CHECK(at.at_embedded_data[0x03] == 0x61);
    BOOST_CHECK(at.at_embedded_data[0x04] == 0x63);
    BOOST_CHECK(at.at_embedded_data[0x05] == 0x68);
    BOOST_CHECK(at.at_embedded_data[0x06] == 0x6d);
    BOOST_CHECK(at.at_embedded_data[0x07] == 0x65);
    // ...
    BOOST_CHECK(at.at_embedded_data[0x01b3] == 0x6f);
    BOOST_CHECK(at.at_embedded_data[0x01b4] == 0x6c);
    BOOST_CHECK(at.at_embedded_data[0x01b5] == 0x64);
    BOOST_CHECK(at.at_embedded_data[0x01b6] == 0x65);
    BOOST_CHECK(at.at_embedded_data[0x01b7] == 0x72);
    BOOST_CHECK(at.at_embedded_data[0x01b8] == 0x29);
    BOOST_CHECK(at.at_embedded_data[0x01b9] == 0x0d);
    BOOST_CHECK(at.at_embedded_data[0x01ba] == 0x0a);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Cablock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/ca_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Cablock ca;
    ca.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(ca.id[0] == '#');
    BOOST_REQUIRE(ca.id[1] == '#');
    BOOST_REQUIRE(ca.id[2] == 'C');
    BOOST_REQUIRE(ca.id[3] == 'A');
    for (auto value : ca.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(ca.length == 0x0000000000000080);
    // 0x0010
    BOOST_CHECK(ca.link_count == 0x0000000000000009);
    BOOST_CHECK(ca.ca_composition == 0x0000000000000000);
    // 0x0020
    BOOST_REQUIRE(ca.ca_data.size() == 0);
    BOOST_REQUIRE(ca.ca_dynamic_size.size() == 0);
    BOOST_REQUIRE(ca.ca_input_quantity.size() == 0);
    BOOST_REQUIRE(ca.ca_output_quantity.size() == 0);
    BOOST_REQUIRE(ca.ca_comparison_quantity.size() == 0);
    BOOST_REQUIRE(ca.ca_cc_axis_conversion.size() == 2);
    BOOST_CHECK(ca.ca_cc_axis_conversion[0] == 0x0000000000000000);
    BOOST_CHECK(ca.ca_cc_axis_conversion[1] == 0x0000000000000000);
    // 0x0030
    BOOST_REQUIRE(ca.ca_axis.size() == 2); // 2 * (DG+CG+CN)
    BOOST_CHECK(ca.ca_axis[0].dg == 0x0000000000000418);
    BOOST_CHECK(ca.ca_axis[0].cg == 0x00000000000004a0);
    // 0x0040
    BOOST_CHECK(ca.ca_axis[0].cn == 0x00000000000008b8);
    BOOST_CHECK(ca.ca_axis[1].dg == 0x0000000000000418);
    // 0x0050
    BOOST_CHECK(ca.ca_axis[1].cg == 0x00000000000004a0);
    BOOST_CHECK(ca.ca_axis[1].cn == 0x0000000000000528);
    // 0x0060
    BOOST_CHECK(ca.ca_type == 0x04); // classification result
    BOOST_CHECK(ca.ca_storage == 0x00); // CN template
    BOOST_CHECK(ca.ca_ndim == 0x0002);
    BOOST_CHECK(ca.ca_flags == 0x00000010); // axis flag
    BOOST_CHECK(ca.ca_byte_offset_base == 0x00000004);
    BOOST_CHECK(ca.ca_inval_bit_pos_base == 0x00000000);
    // 0x0070
    BOOST_REQUIRE(ca.ca_dim_size.size() == 2);
    BOOST_CHECK(ca.ca_dim_size[0] == 0x0000000000000034);
    BOOST_CHECK(ca.ca_dim_size[1] == 0x000000000000008e);
    // 0x0080
    BOOST_REQUIRE(ca.ca_axis_value.size() == 0);
    BOOST_REQUIRE(ca.ca_cycle_count.size() == 0);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Cablock_001)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/ca_001.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Cablock ca;
    ca.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(ca.id[0] == '#');
    BOOST_REQUIRE(ca.id[1] == '#');
    BOOST_REQUIRE(ca.id[2] == 'C');
    BOOST_REQUIRE(ca.id[3] == 'A');
    for (auto value : ca.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(ca.length == 0x00000000000000c0);
    // 0x0010
    BOOST_CHECK(ca.link_count == 0x0000000000000003);
    BOOST_CHECK(ca.ca_composition == 0x0000000000000000);
    // 0x0020
    BOOST_REQUIRE(ca.ca_data.size() == 0);
    BOOST_REQUIRE(ca.ca_dynamic_size.size() == 0);
    BOOST_REQUIRE(ca.ca_input_quantity.size() == 0);
    BOOST_REQUIRE(ca.ca_output_quantity.size() == 0);
    BOOST_REQUIRE(ca.ca_comparison_quantity.size() == 0);
    BOOST_REQUIRE(ca.ca_cc_axis_conversion.size() == 2);
    BOOST_CHECK(ca.ca_cc_axis_conversion[0] == 0x0000000000000000);
    BOOST_CHECK(ca.ca_cc_axis_conversion[1] == 0x0000000000000000);
    BOOST_REQUIRE(ca.ca_axis.size() == 0);
    // 0x0030
    BOOST_CHECK(ca.ca_type == 0x02); // look-up
    BOOST_CHECK(ca.ca_storage == 0x00); // CN template
    BOOST_CHECK(ca.ca_ndim == 0x0002);
    BOOST_CHECK(ca.ca_flags == 0x00000030); // axis flag and fixed axis flag
    BOOST_CHECK(ca.ca_byte_offset_base == 0x00000001);
    BOOST_CHECK(ca.ca_inval_bit_pos_base == 0x00000000);
    // 0x0040
    BOOST_REQUIRE(ca.ca_dim_size.size() == 2);
    BOOST_CHECK(ca.ca_dim_size[0] == 0x0000000000000006);
    BOOST_CHECK(ca.ca_dim_size[1] == 0x0000000000000008);
    // 0x0050
    BOOST_REQUIRE(ca.ca_axis_value.size() == 6 + 8);
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x00], 0.0));
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x01], 1.0));
    // 0x0060
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x02], 2.0));
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x03], 3.0));
    // 0x0070
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x04], 4.0));
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x05], 5.0));
    // 0x0080
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x06], 0.0));
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x07], 1.0));
    // 0x0090
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x08], 2.0));
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x09], 3.0));
    // 0x00a0
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x0a], 4.0));
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x0b], 5.0));
    // 0x00b0
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x0c], 6.0));
    BOOST_CHECK(isEqual(ca.ca_axis_value[0x0d], 7.0));
    BOOST_REQUIRE(ca.ca_cycle_count.size() == 0);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Cablock_002)
{
    // @note There are currently no ASAM examples for DG template available. This is hand made.

    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/ca_002.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Cablock ca;
    ca.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(ca.id[0] == '#');
    BOOST_REQUIRE(ca.id[1] == '#');
    BOOST_REQUIRE(ca.id[2] == 'C');
    BOOST_REQUIRE(ca.id[3] == 'A');
    for (auto value : ca.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(ca.length == 0x0000000000000080);
    // 0x0010
    BOOST_CHECK(ca.link_count == 0x0000000000000005);
    BOOST_CHECK(ca.ca_composition == 0x0000000000000000);
    // 0x0020
    BOOST_REQUIRE(ca.ca_data.size() == 4);
    BOOST_CHECK(ca.ca_data[0] == 0x0000000000000001);
    BOOST_CHECK(ca.ca_data[1] == 0x0000000000000002);
    // 0x0030
    BOOST_CHECK(ca.ca_data[2] == 0x0000000000000003);
    BOOST_CHECK(ca.ca_data[3] == 0x0000000000000004);
    BOOST_REQUIRE(ca.ca_dynamic_size.size() == 0);
    BOOST_REQUIRE(ca.ca_input_quantity.size() == 0);
    BOOST_REQUIRE(ca.ca_output_quantity.size() == 0);
    BOOST_REQUIRE(ca.ca_comparison_quantity.size() == 0);
    BOOST_REQUIRE(ca.ca_cc_axis_conversion.size() == 0);
    BOOST_REQUIRE(ca.ca_axis.size() == 0);
    // 0x0040
    BOOST_CHECK(ca.ca_type == 0x00); // array
    BOOST_CHECK(ca.ca_storage == 0x02); // DG template
    BOOST_CHECK(ca.ca_ndim == 0x0002);
    BOOST_CHECK(ca.ca_flags == 0x00000000); // no flags set
    BOOST_CHECK(ca.ca_byte_offset_base == 0x00000000);
    BOOST_CHECK(ca.ca_inval_bit_pos_base == 0x00000000);
    // 0x0050
    BOOST_REQUIRE(ca.ca_dim_size.size() == 2);
    BOOST_CHECK(ca.ca_dim_size[0] == 0x0000000000000002);
    BOOST_CHECK(ca.ca_dim_size[1] == 0x0000000000000002);
    // 0x0060
    BOOST_REQUIRE(ca.ca_axis_value.size() == 0);
    BOOST_REQUIRE(ca.ca_cycle_count.size() == 4);
    BOOST_CHECK(ca.ca_cycle_count[0] == 0x0000000000000001);
    BOOST_CHECK(ca.ca_cycle_count[1] == 0x0000000000000002);
    // 0x0070
    BOOST_CHECK(ca.ca_cycle_count[2] == 0x0000000000000003);
    BOOST_CHECK(ca.ca_cycle_count[3] == 0x0000000000000004);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Cablock_003)
{
    // @note There are currently no ASAM examples for flags&0x0f available. This is hand made.

    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/ca_003.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Cablock ca;
    ca.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(ca.id[0] == '#');
    BOOST_REQUIRE(ca.id[1] == '#');
    BOOST_REQUIRE(ca.id[2] == 'C');
    BOOST_REQUIRE(ca.id[3] == 'A');
    for (auto value : ca.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(ca.length == 0x00000000000000d0);
    // 0x0010
    BOOST_CHECK(ca.link_count == 0x0000000000000013);
    BOOST_CHECK(ca.ca_composition == 0x0000000000000000);
    // 0x0020
    BOOST_REQUIRE(ca.ca_data.size() == 0);
    BOOST_REQUIRE(ca.ca_dynamic_size.size() == 2);
    BOOST_CHECK(ca.ca_dynamic_size[0].dg == 0x0000000000000001);
    BOOST_CHECK(ca.ca_dynamic_size[0].cg == 0x0000000000000002);
    // 0x0030
    BOOST_CHECK(ca.ca_dynamic_size[0].cn == 0x0000000000000003);
    BOOST_CHECK(ca.ca_dynamic_size[1].dg == 0x0000000000000004);
    // 0x0040
    BOOST_CHECK(ca.ca_dynamic_size[1].cg == 0x0000000000000005);
    BOOST_CHECK(ca.ca_dynamic_size[1].cn == 0x0000000000000006);
    // 0x0050
    BOOST_REQUIRE(ca.ca_input_quantity.size() == 2);
    BOOST_CHECK(ca.ca_input_quantity[0].dg == 0x0000000000000001);
    BOOST_CHECK(ca.ca_input_quantity[0].cg == 0x0000000000000002);
    // 0x0060
    BOOST_CHECK(ca.ca_input_quantity[0].cn == 0x0000000000000003);
    BOOST_CHECK(ca.ca_input_quantity[1].dg == 0x0000000000000004);
    // 0x0070
    BOOST_CHECK(ca.ca_input_quantity[1].cg == 0x0000000000000005);
    BOOST_CHECK(ca.ca_input_quantity[1].cn == 0x0000000000000006);
    // 0x0080
    BOOST_REQUIRE(ca.ca_output_quantity.size() == 1);
    BOOST_CHECK(ca.ca_output_quantity[0].dg == 0x0000000000000001);
    BOOST_CHECK(ca.ca_output_quantity[0].cg == 0x0000000000000002);
    // 0x0090
    BOOST_CHECK(ca.ca_output_quantity[0].cn == 0x0000000000000003);
    BOOST_REQUIRE(ca.ca_comparison_quantity.size() == 1);
    BOOST_CHECK(ca.ca_comparison_quantity[0].dg == 0x0000000000000001);
    // 0x00a0
    BOOST_CHECK(ca.ca_comparison_quantity[0].cg == 0x0000000000000002);
    BOOST_CHECK(ca.ca_comparison_quantity[0].cn == 0x0000000000000003);
    // 0x00b0
    BOOST_REQUIRE(ca.ca_cc_axis_conversion.size() == 0);
    BOOST_REQUIRE(ca.ca_axis.size() == 0);
    BOOST_CHECK(ca.ca_type == 0x02); // look-up
    BOOST_CHECK(ca.ca_storage == 0x00); // CN template
    BOOST_CHECK(ca.ca_ndim == 0x0002);
    BOOST_CHECK(ca.ca_flags == 0x0000000f); // no flags set
    BOOST_CHECK(ca.ca_byte_offset_base == 0x00000000);
    BOOST_CHECK(ca.ca_inval_bit_pos_base == 0x00000000);
    // 0x00c0
    BOOST_REQUIRE(ca.ca_dim_size.size() == 2);
    BOOST_CHECK(ca.ca_dim_size[0] == 0x0000000000000002);
    BOOST_CHECK(ca.ca_dim_size[1] == 0x0000000000000002);
    BOOST_REQUIRE(ca.ca_axis_value.size() == 0);
    BOOST_REQUIRE(ca.ca_cycle_count.size() == 0);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Ccblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/cc_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Ccblock cc;
    cc.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(cc.id[0] == '#');
    BOOST_REQUIRE(cc.id[1] == '#');
    BOOST_REQUIRE(cc.id[2] == 'C');
    BOOST_REQUIRE(cc.id[3] == 'C');
    for (auto value : cc.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(cc.length == 0x0000000000000060);
    // 0x0010
    BOOST_CHECK(cc.link_count == 0x0000000000000004);
    BOOST_CHECK(cc.cc_tx_name == 0x0000000000000000);
    // 0x0020
    BOOST_CHECK(cc.cc_md_unit == 0x00000000000001d8);
    BOOST_CHECK(cc.cc_md_comment == 0x0000000000000000);
    // 0x0030
    BOOST_CHECK(cc.cc_cc_inverse == 0x0000000000000000);
    BOOST_REQUIRE(cc.cc_ref.size() == 0);
    BOOST_CHECK(cc.cc_type == 0x01);
    BOOST_CHECK(cc.cc_precision == 0x00);
    BOOST_CHECK(cc.cc_flags == 0x0002);
    BOOST_CHECK(cc.cc_ref_count == 0x0000);
    BOOST_CHECK(cc.cc_val_count == 0x0002);
    // 0x0040
    BOOST_CHECK(isEqual(cc.cc_phy_range_min, 0.00022));
    BOOST_CHECK(isEqual(cc.cc_phy_range_max, 10.18022));
    // 0x0050
    BOOST_REQUIRE(cc.cc_val.size() == 2);
    BOOST_CHECK(isEqual(cc.cc_val[0], 0));
    BOOST_CHECK(isEqual(cc.cc_val[1], 0.000000001));

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Cgblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/cg_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Cgblock cg;
    cg.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(cg.id[0] == '#');
    BOOST_REQUIRE(cg.id[1] == '#');
    BOOST_REQUIRE(cg.id[2] == 'C');
    BOOST_REQUIRE(cg.id[3] == 'G');
    for (auto value : cg.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(cg.length == 0x0000000000000068);
    // 0x0010
    BOOST_CHECK(cg.link_count == 0x0000000000000006);
    BOOST_CHECK(cg.cg_cg_next == 0x0000000000000000);
    // 0x0020
    BOOST_CHECK(cg.cg_cn_first == 0x0000000000008120);
    BOOST_CHECK(cg.cg_tx_acq_name == 0x0000000000000000);
    // 0x0030
    BOOST_CHECK(cg.cg_si_acq_source == 0x0000000000000000);
    BOOST_CHECK(cg.cg_sr_first == 0x0000000000000000);
    // 0x0040
    BOOST_CHECK(cg.cg_md_comment == 0x0000000000003f90);
    BOOST_CHECK(cg.cg_record_id == 0x0000000000000000);
    // 0x0050
    BOOST_CHECK(cg.cg_cycle_count == 0x0000000000002710);
    BOOST_CHECK(cg.cg_flags == 0x0000);
    BOOST_CHECK(cg.cg_path_separator == 0x0000);
    for (auto value : cg.cg_reserved) {
        BOOST_CHECK(value == 0x00);
    }
    // 0x0060
    BOOST_CHECK(cg.cg_data_bytes == 0x00000006);
    BOOST_CHECK(cg.cg_inval_bytes == 0x00000000);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Chblock_000)
{
    // @note There are currently no ASAM examples available. This is hand made.

    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/ch_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Chblock ch;
    ch.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(ch.id[0] == '#');
    BOOST_REQUIRE(ch.id[1] == '#');
    BOOST_REQUIRE(ch.id[2] == 'C');
    BOOST_REQUIRE(ch.id[3] == 'H');
    for (auto value : ch.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(ch.length == 0x0000000000000058);
    // 0x0010
    BOOST_CHECK(ch.link_count == 0x0000000000000007);
    BOOST_CHECK(ch.ch_ch_next == 0x0000000000000100);
    // 0x0020
    BOOST_CHECK(ch.ch_ch_first == 0x0000000000000200);
    BOOST_CHECK(ch.ch_tx_name == 0x0000000000000300);
    // 0x0030
    BOOST_CHECK(ch.ch_md_comment == 0x0000000000000400);
    BOOST_REQUIRE(ch.ch_element.size() == 1);
    BOOST_CHECK(ch.ch_element[0].dg == 0x0000000000000500);
    // 0x0040
    BOOST_CHECK(ch.ch_element[0].cg == 0x0000000000000600);
    BOOST_CHECK(ch.ch_element[0].cn == 0x0000000000000700);
    // 0x0050
    BOOST_CHECK(ch.ch_element_count == 0x00000001);
    BOOST_CHECK(ch.ch_type == 0x04); // input variables of function
    for (auto value : ch.ch_reserved) {
        BOOST_CHECK(value == 0x00);
    }

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Cnblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/cn_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Cnblock cn;
    cn.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(cn.id[0] == '#');
    BOOST_REQUIRE(cn.id[1] == '#');
    BOOST_REQUIRE(cn.id[2] == 'C');
    BOOST_REQUIRE(cn.id[3] == 'N');
    for (auto value : cn.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(cn.length == 0x00000000000000a0);
    // 0x0010
    BOOST_CHECK(cn.link_count == 0x0000000000000008);
    BOOST_CHECK(cn.cn_cn_next == 0x00000000000081c0);
    // 0x0020
    BOOST_CHECK(cn.cn_composition == 0x0000000000000000);
    BOOST_CHECK(cn.cn_tx_name == 0x0000000000003fb8);
    // 0x0030
    BOOST_CHECK(cn.cn_si_source == 0x0000000000000000);
    BOOST_CHECK(cn.cn_cc_conversion == 0x0000000000000000);
    // 0x0040
    BOOST_CHECK(cn.cn_data == 0x0000000000000000);
    BOOST_CHECK(cn.cn_md_unit == 0x0000000000000000);
    // 0x0050
    BOOST_CHECK(cn.cn_md_comment == 0x0000000000000000);
    BOOST_REQUIRE(cn.cn_at_reference.size() == 0);
    BOOST_REQUIRE(cn.cn_default_x.size() == 0);
    BOOST_CHECK(cn.cn_type == 0x02);
    BOOST_CHECK(cn.cn_sync_type == 0x01);
    BOOST_CHECK(cn.cn_data_type == 0x00);
    BOOST_CHECK(cn.cn_bit_offset == 0x00);
    BOOST_CHECK(cn.cn_byte_offset == 0x00000000);
    // 0x0060
    BOOST_CHECK(cn.cn_bit_count == 0x00000020);
    BOOST_CHECK(cn.cn_flags == 0x00000000);
    BOOST_CHECK(cn.cn_inval_bit_pos == 0x00000000);
    BOOST_CHECK(cn.cn_precision == 0x00);
    BOOST_CHECK(cn.cn_reserved == 0x00);
    BOOST_CHECK(cn.cn_attachment_count == 0x0000);
    // 0x0070
    BOOST_CHECK(isEqual(cn.cn_val_range_min, 0));
    BOOST_CHECK(isEqual(cn.cn_val_range_max, 0));
    // 0x0080
    BOOST_CHECK(isEqual(cn.cn_limit_min, 0));
    BOOST_CHECK(isEqual(cn.cn_limit_max, 0));
    // 0x0090
    BOOST_CHECK(isEqual(cn.cn_limit_ext_min, 0));
    BOOST_CHECK(isEqual(cn.cn_limit_ext_max, 0));

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Cnblock_001)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/cn_001.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Cnblock cn;
    cn.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(cn.id[0] == '#');
    BOOST_REQUIRE(cn.id[1] == '#');
    BOOST_REQUIRE(cn.id[2] == 'C');
    BOOST_REQUIRE(cn.id[3] == 'N');
    for (auto value : cn.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(cn.length == 0x00000000000000b8);
    // 0x0010
    BOOST_CHECK(cn.link_count == 0x000000000000000b);
    BOOST_CHECK(cn.cn_cn_next == 0x00000000000004c0);
    // 0x0020
    BOOST_CHECK(cn.cn_composition == 0x0000000000000000);
    BOOST_CHECK(cn.cn_tx_name == 0x0000000000000790);
    // 0x0030
    BOOST_CHECK(cn.cn_si_source == 0x0000000000000000);
    BOOST_CHECK(cn.cn_cc_conversion == 0x0000000000000000);
    // 0x0040
    BOOST_CHECK(cn.cn_data == 0x0000000000000000);
    BOOST_CHECK(cn.cn_md_unit == 0x0000000000000000);
    // 0x0050
    BOOST_CHECK(cn.cn_md_comment == 0x00000000000005b8);
    BOOST_REQUIRE(cn.cn_at_reference.size() == 0);
    BOOST_REQUIRE(cn.cn_default_x.size() == 1);
    BOOST_CHECK(cn.cn_default_x[0].dg == 0x0000000000000438);
    // 0x0060
    BOOST_CHECK(cn.cn_default_x[0].cg == 0x00000000000003d0);
    BOOST_CHECK(cn.cn_default_x[0].cn == 0x0000000000000628);
    // 0x0070
    BOOST_CHECK(cn.cn_type == 0x00);
    BOOST_CHECK(cn.cn_sync_type == 0x00);
    BOOST_CHECK(cn.cn_data_type == 0x00);
    BOOST_CHECK(cn.cn_bit_offset == 0x00);
    BOOST_CHECK(cn.cn_byte_offset == 0x00000000);
    BOOST_CHECK(cn.cn_bit_count == 0x00000008);
    BOOST_CHECK(cn.cn_flags == 0x00001000); // Default X axis flag
    // 0x0080
    BOOST_CHECK(cn.cn_inval_bit_pos == 0x00000000);
    BOOST_CHECK(cn.cn_precision == 0x00);
    BOOST_CHECK(cn.cn_reserved == 0x00);
    BOOST_CHECK(cn.cn_attachment_count == 0x0000);
    BOOST_CHECK(isEqual(cn.cn_val_range_min, 0));
    // 0x0090
    BOOST_CHECK(isEqual(cn.cn_val_range_max, 0));
    BOOST_CHECK(isEqual(cn.cn_limit_min, 0));
    // 0x00a0
    BOOST_CHECK(isEqual(cn.cn_limit_max, 0));
    BOOST_CHECK(isEqual(cn.cn_limit_ext_min, 0));
    // 0x00b0
    BOOST_CHECK(isEqual(cn.cn_limit_ext_max, 0));

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Cnblock_002)
{
    // @note There are currently no ASAM examples for attachment available. This is hand made, derived from cn_000.

    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/cn_002.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Cnblock cn;
    cn.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(cn.id[0] == '#');
    BOOST_REQUIRE(cn.id[1] == '#');
    BOOST_REQUIRE(cn.id[2] == 'C');
    BOOST_REQUIRE(cn.id[3] == 'N');
    for (auto value : cn.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(cn.length == 0x00000000000000a8);
    // 0x0010
    BOOST_CHECK(cn.link_count == 0x0000000000000009);
    BOOST_CHECK(cn.cn_cn_next == 0x00000000000081c0);
    // 0x0020
    BOOST_CHECK(cn.cn_composition == 0x0000000000000000);
    BOOST_CHECK(cn.cn_tx_name == 0x0000000000003fb8);
    // 0x0030
    BOOST_CHECK(cn.cn_si_source == 0x0000000000000000);
    BOOST_CHECK(cn.cn_cc_conversion == 0x0000000000000000);
    // 0x0040
    BOOST_CHECK(cn.cn_data == 0x0000000000000000);
    BOOST_CHECK(cn.cn_md_unit == 0x0000000000000000);
    // 0x0050
    BOOST_CHECK(cn.cn_md_comment == 0x0000000000000000);
    BOOST_REQUIRE(cn.cn_at_reference.size() == 1);
    BOOST_CHECK(cn.cn_at_reference[0] == 0x3333333333333333);
    // 0x0060
    BOOST_REQUIRE(cn.cn_default_x.size() == 0);
    BOOST_CHECK(cn.cn_type == 0x02);
    BOOST_CHECK(cn.cn_sync_type == 0x01);
    BOOST_CHECK(cn.cn_data_type == 0x00);
    BOOST_CHECK(cn.cn_bit_offset == 0x00);
    BOOST_CHECK(cn.cn_byte_offset == 0x00000000);
    BOOST_CHECK(cn.cn_bit_count == 0x00000020);
    BOOST_CHECK(cn.cn_flags == 0x00000000);
    // 0x0070
    BOOST_CHECK(cn.cn_inval_bit_pos == 0x00000000);
    BOOST_CHECK(cn.cn_precision == 0x00);
    BOOST_CHECK(cn.cn_reserved == 0x00);
    BOOST_CHECK(cn.cn_attachment_count == 0x0001);
    BOOST_CHECK(isEqual(cn.cn_val_range_min, 0));
    // 0x0080
    BOOST_CHECK(isEqual(cn.cn_val_range_max, 0));
    BOOST_CHECK(isEqual(cn.cn_limit_min, 0));
    // 0x0090
    BOOST_CHECK(isEqual(cn.cn_limit_max, 0));
    BOOST_CHECK(isEqual(cn.cn_limit_ext_min, 0));
    // 0x00a0
    BOOST_CHECK(isEqual(cn.cn_limit_ext_max, 0));

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Dgblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/dg_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Dgblock dg;
    dg.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(dg.id[0] == '#');
    BOOST_REQUIRE(dg.id[1] == '#');
    BOOST_REQUIRE(dg.id[2] == 'D');
    BOOST_REQUIRE(dg.id[3] == 'G');
    for (auto value : dg.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(dg.length == 0x0000000000000040);
    // 0x0010
    BOOST_CHECK(dg.link_count == 0x0000000000000004);
    BOOST_CHECK(dg.dg_dg_next == 0x00000000000084b0);
    // 0x0020
    BOOST_CHECK(dg.dg_cg_first == 0x0000000000008260);
    BOOST_CHECK(dg.dg_data == 0x00000000000000e0);
    // 0x0030
    BOOST_CHECK(dg.dg_md_comment == 0x0000000000000000);
    BOOST_CHECK(dg.dg_rec_id_size == 0x00);
    for (auto value : dg.dg_reserved) {
        BOOST_CHECK(value == 0x00);
    }

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Dlblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/dl_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Dlblock dl;
    dl.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(dl.id[0] == '#');
    BOOST_REQUIRE(dl.id[1] == '#');
    BOOST_REQUIRE(dl.id[2] == 'D');
    BOOST_REQUIRE(dl.id[3] == 'L');
    for (auto value : dl.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(dl.length == 0x0000000000000038);
    // 0x0010
    BOOST_CHECK(dl.link_count == 0x0000000000000002);
    BOOST_CHECK(dl.dl_dl_next == 0x0000000000000000);
    // 0x0020
    BOOST_REQUIRE(dl.dl_data.size() == 1);
    BOOST_CHECK(dl.dl_data[0] == 0x00000000000086f0);
    BOOST_CHECK(dl.dl_flags == 0x01); // equal length flag
    for (auto value : dl.dl_reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_CHECK(dl.dl_count == 0x00000001);
    // 0x0030
    BOOST_REQUIRE(dl.dl_equal_length.size() == 1);
    BOOST_CHECK(dl.dl_equal_length[0] == 0x0000000000040000);
    BOOST_REQUIRE(dl.dl_offset.size() == 0);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Dlblock_001)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/dl_001.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Dlblock dl;
    dl.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(dl.id[0] == '#');
    BOOST_REQUIRE(dl.id[1] == '#');
    BOOST_REQUIRE(dl.id[2] == 'D');
    BOOST_REQUIRE(dl.id[3] == 'L');
    for (auto value : dl.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(dl.length == 0x0000000000000348);
    // 0x0010
    BOOST_CHECK(dl.link_count == 0x0000000000000033);
    BOOST_CHECK(dl.dl_dl_next == 0x0000000000000000);
    // 0x0020
    BOOST_REQUIRE(dl.dl_data.size() == 0x32);
    BOOST_CHECK(dl.dl_data[0x00] == 0x00000000001eb838);
    // ...
    BOOST_CHECK(dl.dl_data[0x31] == 0x00000000001ec478);
    // 0x1b0
    BOOST_CHECK(dl.dl_flags == 0x00); // equal length flag not set
    for (auto value : dl.dl_reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_CHECK(dl.dl_count == 0x00000032);
    BOOST_REQUIRE(dl.dl_equal_length.size() == 0);
    BOOST_REQUIRE(dl.dl_offset.size() == 0x32);
    BOOST_REQUIRE(dl.dl_offset[0x00] == 0x00000000000d04a6);
    // ...
    BOOST_REQUIRE(dl.dl_offset[0x31] == 0x00000000000d0b28);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Dtblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/dt_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Dtblock dt;
    dt.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(dt.id[0] == '#');
    BOOST_REQUIRE(dt.id[1] == '#');
    BOOST_REQUIRE(dt.id[2] == 'D');
    BOOST_REQUIRE(dt.id[3] == 'T');
    for (auto value : dt.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(dt.length == 0x000000000000ea78);
    // 0x0010
    BOOST_CHECK(dt.link_count == 0x0000000000000000);
    BOOST_REQUIRE(dt.dt_data.size() == 0xea60); // 0xea78 - 0x18
    BOOST_CHECK(dt.dt_data[0x00] == 0x00);
    BOOST_CHECK(dt.dt_data[0x01] == 0x00);
    BOOST_CHECK(dt.dt_data[0x02] == 0x00);
    BOOST_CHECK(dt.dt_data[0x03] == 0x00);
    BOOST_CHECK(dt.dt_data[0x04] == 0x00);
    BOOST_CHECK(dt.dt_data[0x05] == 0x00);
    BOOST_CHECK(dt.dt_data[0x06] == 0x01);
    BOOST_CHECK(dt.dt_data[0x07] == 0x00);
    // ...
    BOOST_CHECK(dt.dt_data[0xea58] == 0x12);
    BOOST_CHECK(dt.dt_data[0xea59] == 0x00);
    BOOST_CHECK(dt.dt_data[0xea5a] == 0x0f);
    BOOST_CHECK(dt.dt_data[0xea5b] == 0x27);
    BOOST_CHECK(dt.dt_data[0xea5c] == 0x00);
    BOOST_CHECK(dt.dt_data[0xea5d] == 0x00);
    BOOST_CHECK(dt.dt_data[0xea5e] == 0x13);
    BOOST_CHECK(dt.dt_data[0xea5f] == 0x00);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Dzblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/dz_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Dzblock dz;
    dz.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(dz.id[0] == '#');
    BOOST_REQUIRE(dz.id[1] == '#');
    BOOST_REQUIRE(dz.id[2] == 'D');
    BOOST_REQUIRE(dz.id[3] == 'Z');
    for (auto value : dz.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(dz.length == 0x000000000001d48a);
    // 0x0010
    BOOST_CHECK(dz.link_count == 0x0000000000000000);
    BOOST_CHECK(dz.dz_org_block_type[0] == 'D');
    BOOST_CHECK(dz.dz_org_block_type[1] == 'T');
    BOOST_CHECK(dz.dz_zip_type == 0x00);
    BOOST_CHECK(dz.dz_reserved == 0x00);
    BOOST_CHECK(dz.dz_zip_parameter == 0x00000000);
    // 0x0020
    BOOST_CHECK(dz.dz_org_data_length == 0x000000000004e200);
    BOOST_CHECK(dz.dz_data_length == 0x000000000001d45a);
    // 0x0030
    BOOST_REQUIRE(dz.dz_data.size() == 0x01d45a); // 0x01d48a - 0x30
    BOOST_CHECK(dz.dz_data[0x00] == 0x78);
    BOOST_CHECK(dz.dz_data[0x01] == 0x9c);
    BOOST_CHECK(dz.dz_data[0x02] == 0x74);
    BOOST_CHECK(dz.dz_data[0x03] == 0x9d);
    BOOST_CHECK(dz.dz_data[0x04] == 0x75);
    BOOST_CHECK(dz.dz_data[0x05] == 0x5c);
    BOOST_CHECK(dz.dz_data[0x06] == 0x55);
    BOOST_CHECK(dz.dz_data[0x07] == 0x4b);
    // ...
    BOOST_CHECK(dz.dz_data[0x01d452] == 0xaa);
    BOOST_CHECK(dz.dz_data[0x01d453] == 0xe0);
    BOOST_CHECK(dz.dz_data[0x01d454] == 0xff);
    BOOST_CHECK(dz.dz_data[0x01d455] == 0x00);
    BOOST_CHECK(dz.dz_data[0x01d456] == 0xf4);
    BOOST_CHECK(dz.dz_data[0x01d457] == 0xb1);
    BOOST_CHECK(dz.dz_data[0x01d458] == 0x88);
    BOOST_CHECK(dz.dz_data[0x01d459] == 0x4d);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Evblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/ev_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Evblock ev;
    ev.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(ev.id[0] == '#');
    BOOST_REQUIRE(ev.id[1] == '#');
    BOOST_REQUIRE(ev.id[2] == 'E');
    BOOST_REQUIRE(ev.id[3] == 'V');
    for (auto value : ev.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(ev.length == 0x0000000000000060);
    // 0x0010
    BOOST_CHECK(ev.link_count == 0x0000000000000005);
    BOOST_CHECK(ev.ev_ev_next == 0x0000000000000000);
    // 0x0020
    BOOST_CHECK(ev.ev_ev_parent == 0x0000000000000000);
    BOOST_CHECK(ev.ev_ev_range == 0x0000000000000000);
    // 0x0030
    BOOST_CHECK(ev.ev_tx_name == 0x0000000000000758);
    BOOST_CHECK(ev.ev_md_comment == 0x0000000000000778);
    // 0x0040
    BOOST_REQUIRE(ev.ev_scope.size() == 0);
    BOOST_REQUIRE(ev.ev_at_reference.size() == 0);
    BOOST_CHECK(ev.ev_type == 0x06); // Marker
    BOOST_CHECK(ev.ev_sync_type == 0x01); // calculated synchronization value
    BOOST_CHECK(ev.ev_range_type == 0x00); // event defines a point
    BOOST_CHECK(ev.ev_cause == 0x02); // TOOL
    BOOST_CHECK(ev.ev_flags == 0x00);
    for (auto value : ev.ev_reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_CHECK(ev.ev_scope_count == 0x00000000);
    BOOST_CHECK(ev.ev_attachment_count == 0x0000);
    BOOST_CHECK(ev.ev_creator_index == 0x0000);
    // 0x0050
    BOOST_CHECK(ev.ev_sync_base_value == 0x0000000000000001);
    BOOST_CHECK(isEqual(ev.ev_sync_factor, 0.36));

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Evblock_001)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/ev_001.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Evblock ev;
    ev.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(ev.id[0] == '#');
    BOOST_REQUIRE(ev.id[1] == '#');
    BOOST_REQUIRE(ev.id[2] == 'E');
    BOOST_REQUIRE(ev.id[3] == 'V');
    for (auto value : ev.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(ev.length == 0x0000000000000068);
    // 0x0010
    BOOST_CHECK(ev.link_count == 0x0000000000000006);
    BOOST_CHECK(ev.ev_ev_next == 0x0000000000001c60);
    // 0x0020
    BOOST_CHECK(ev.ev_ev_parent == 0x0000000000000000);
    BOOST_CHECK(ev.ev_ev_range == 0x0000000000000000);
    // 0x0030
    BOOST_CHECK(ev.ev_tx_name == 0x0000000000000000);
    BOOST_CHECK(ev.ev_md_comment == 0x0000000000000000);
    // 0x0040
    BOOST_REQUIRE(ev.ev_scope.size() == 1);
    BOOST_REQUIRE(ev.ev_at_reference.size() == 0);
    BOOST_CHECK(ev.ev_scope[0] == 0x0000000000000db8);
    BOOST_CHECK(ev.ev_type == 0x02); // Acquisition Interrupt
    BOOST_CHECK(ev.ev_sync_type == 0x01); // calculated synchronization value
    BOOST_CHECK(ev.ev_range_type == 0x01); // event defines the beginning of a range
    BOOST_CHECK(ev.ev_cause == 0x02); // TOOL
    BOOST_CHECK(ev.ev_flags == 0x00);
    for (auto value : ev.ev_reserved) {
        BOOST_CHECK(value == 0x00);
    }
    // 0x0050
    BOOST_CHECK(ev.ev_scope_count == 0x00000001);
    BOOST_CHECK(ev.ev_attachment_count == 0x0000);
    BOOST_CHECK(ev.ev_creator_index == 0x0000);
    BOOST_CHECK(ev.ev_sync_base_value == 0x0000000000000001);
    // 0x0060
    BOOST_CHECK(isEqual(ev.ev_sync_factor, 4.724999940));

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Evblock_002)
{
    // @note There are currently no ASAM examples for attachment available. This is hand made, derived from ev_000.

    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/ev_002.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Evblock ev;
    ev.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(ev.id[0] == '#');
    BOOST_REQUIRE(ev.id[1] == '#');
    BOOST_REQUIRE(ev.id[2] == 'E');
    BOOST_REQUIRE(ev.id[3] == 'V');
    for (auto value : ev.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(ev.length == 0x0000000000000068);
    // 0x0010
    BOOST_CHECK(ev.link_count == 0x0000000000000006);
    BOOST_CHECK(ev.ev_ev_next == 0x0000000000000000);
    // 0x0020
    BOOST_CHECK(ev.ev_ev_parent == 0x0000000000000000);
    BOOST_CHECK(ev.ev_ev_range == 0x0000000000000000);
    // 0x0030
    BOOST_CHECK(ev.ev_tx_name == 0x0000000000000758);
    BOOST_CHECK(ev.ev_md_comment == 0x0000000000000778);
    // 0x0040
    BOOST_REQUIRE(ev.ev_scope.size() == 0);
    BOOST_REQUIRE(ev.ev_at_reference.size() == 1);
    BOOST_CHECK(ev.ev_at_reference[0] == 0x3333333333333333);
    BOOST_CHECK(ev.ev_type == 0x06); // Marker
    BOOST_CHECK(ev.ev_sync_type == 0x01); // calculated synchronization value
    BOOST_CHECK(ev.ev_range_type == 0x00); // event defines a point
    BOOST_CHECK(ev.ev_cause == 0x02); // TOOL
    BOOST_CHECK(ev.ev_flags == 0x00);
    for (auto value : ev.ev_reserved) {
        BOOST_CHECK(value == 0x00);
    }
    // 0x0050
    BOOST_CHECK(ev.ev_scope_count == 0x00000000);
    BOOST_CHECK(ev.ev_attachment_count == 0x0001);
    BOOST_CHECK(ev.ev_creator_index == 0x0000);
    BOOST_CHECK(ev.ev_sync_base_value == 0x0000000000000001);
    // 0x0060
    BOOST_CHECK(isEqual(ev.ev_sync_factor, 0.36));

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Fhblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/fh_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Fhblock fh;
    fh.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(fh.id[0] == '#');
    BOOST_REQUIRE(fh.id[1] == '#');
    BOOST_REQUIRE(fh.id[2] == 'F');
    BOOST_REQUIRE(fh.id[3] == 'H');
    for (auto value : fh.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(fh.length == 0x0000000000000038);
    // 0x0010
    BOOST_CHECK(fh.link_count == 0x0000000000000002);
    BOOST_CHECK(fh.fh_fh_next == 0x0000000000000000);
    // 0x0020
    BOOST_CHECK(fh.fh_md_comment == 0x00000000000084f0);
    BOOST_CHECK(fh.fh_time_ns == 0x1241c5899cc14a00);
    // 0x0030
    BOOST_CHECK(fh.fh_tz_offset_min == 0x003c);
    BOOST_CHECK(fh.fh_dst_offset_min == 0x003c);
    BOOST_CHECK(fh.fh_time_flags == 0x02);
    for (auto value : fh.fh_reserved) {
        BOOST_CHECK(value == 0x00);
    }

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Hdblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/hd_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Hdblock hd;
    hd.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(hd.id[0] == '#');
    BOOST_REQUIRE(hd.id[1] == '#');
    BOOST_REQUIRE(hd.id[2] == 'H');
    BOOST_REQUIRE(hd.id[3] == 'D');
    for (auto value : hd.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(hd.length == 0x0000000000000068);
    // 0x0010
    BOOST_CHECK(hd.link_count == 0x0000000000000006);
    BOOST_CHECK(hd.hd_dg_first == 0x0000000000008470);
    // 0x0020
    BOOST_CHECK(hd.hd_fh_first == 0x00000000000000a8);
    BOOST_CHECK(hd.hd_ch_first == 0x0000000000000000);
    // 0x0030
    BOOST_CHECK(hd.hd_at_first == 0x0000000000000000);
    BOOST_CHECK(hd.hd_ev_first == 0x0000000000000000);
    // 0x0040
    BOOST_CHECK(hd.hd_md_comment == 0x0000000000007f70);
    BOOST_CHECK(hd.hd_start_time_ns == 0x1241c589cf6b4280);
    // 0x0050
    BOOST_CHECK(hd.hd_tz_offset_min == 0x003c);
    BOOST_CHECK(hd.hd_dst_offset_min == 0x003c);
    BOOST_CHECK(hd.hd_time_flags == 0x02);
    BOOST_CHECK(hd.hd_time_class == 0x00);
    BOOST_CHECK(hd.hd_flags == 0x00);
    BOOST_CHECK(hd.hd_reserved == 0x00);
    BOOST_CHECK(isEqual(hd.hd_start_angle_rad, 0));
    // 0x0060
    BOOST_CHECK(isEqual(hd.hd_start_distance_m, 0));

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Hlblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/hl_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Hlblock hl;
    hl.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(hl.id[0] == '#');
    BOOST_REQUIRE(hl.id[1] == '#');
    BOOST_REQUIRE(hl.id[2] == 'H');
    BOOST_REQUIRE(hl.id[3] == 'L');
    for (auto value : hl.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(hl.length == 0x0000000000000028);
    // 0x0010
    BOOST_CHECK(hl.link_count == 0x0000000000000001);
    BOOST_CHECK(hl.hl_dl_first == 0x000000000001e268);
    // 0x0020
    BOOST_CHECK(hl.hl_flags == 0x0001);
    BOOST_CHECK(hl.hl_zip_type == 0x00);
    for (auto value : hl.hl_reserved) {
        BOOST_CHECK(value == 0x00);
    }

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Idblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/id_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Idblock id;
    id.read(file);
    // 0x0000
    BOOST_CHECK(id.id_file[0] == 'M');
    BOOST_CHECK(id.id_file[1] == 'D');
    BOOST_CHECK(id.id_file[2] == 'F');
    BOOST_CHECK(id.id_file[3] == ' ');
    BOOST_CHECK(id.id_file[4] == ' ');
    BOOST_CHECK(id.id_file[5] == ' ');
    BOOST_CHECK(id.id_file[6] == ' ');
    BOOST_CHECK(id.id_file[7] == ' ');
    BOOST_CHECK(id.id_vers[0] == '4');
    BOOST_CHECK(id.id_vers[1] == '.');
    BOOST_CHECK(id.id_vers[2] == '1');
    BOOST_CHECK(id.id_vers[3] == '0');
    BOOST_CHECK(id.id_vers[4] == ' ');
    BOOST_CHECK(id.id_vers[5] == ' ');
    BOOST_CHECK(id.id_vers[6] == ' ');
    BOOST_CHECK(id.id_vers[7] == ' ');
    // 0x0010
    BOOST_CHECK(id.id_prog[0] == 'M');
    BOOST_CHECK(id.id_prog[1] == 'D');
    BOOST_CHECK(id.id_prog[2] == 'F');
    BOOST_CHECK(id.id_prog[3] == '_');
    BOOST_CHECK(id.id_prog[4] == 'I');
    BOOST_CHECK(id.id_prog[5] == 'P');
    BOOST_CHECK(id.id_prog[6] == 0x00);
    BOOST_CHECK(id.id_prog[7] == 0x00);
    for (auto value : id.id_reserved1) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_CHECK(id.id_ver == 410);
    // 0x001e
    for (auto value : id.id_reserved2) {
        BOOST_CHECK(value == 0x00);
    }
    // 0x003c
    BOOST_CHECK(id.id_unfin_flags == 0x0000);
    BOOST_CHECK(id.id_custom_unfin_flags == 0x0000);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Mdblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/md_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Mdblock md;
    md.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(md.id[0] == '#');
    BOOST_REQUIRE(md.id[1] == '#');
    BOOST_REQUIRE(md.id[2] == 'M');
    BOOST_REQUIRE(md.id[3] == 'D');
    for (auto value : md.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(md.length == 0x00000000000001b0);
    // 0x0010
    BOOST_CHECK(md.link_count == 0x0000000000000000);
    std::string text(md.md_data.begin(), md.md_data.end() - 1);
    BOOST_CHECK(text ==
                "<HDcomment xmlns=\"http://www.asam.net/mdf/v4\">"
                "<TX>ASAM MDF 4.0 Example file created by ETAS. Contents: 2 simple channel groups containing ints and floats in little endian format.</TX>"
                "<time_source>PC timer</time_source>"
                "<common_properties>"
                "<e name=\"subject\">Example</e>"
                "<e name=\"project\">ASAM Example Files</e>"
                "<e name=\"department\">ETAS GmbH</e>"
                "<e name=\"author\">Tobias Langner</e>"
                "</common_properties>"
                "</HDcomment>");

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Rdblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/rd_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Rdblock rd;
    rd.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(rd.id[0] == '#');
    BOOST_REQUIRE(rd.id[1] == '#');
    BOOST_REQUIRE(rd.id[2] == 'R');
    BOOST_REQUIRE(rd.id[3] == 'D');
    for (auto value : rd.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(rd.length == 0x000000000000048c);
    // 0x0010
    BOOST_CHECK(rd.link_count == 0x0000000000000000);
    BOOST_REQUIRE(rd.rd_data.size() == 0x0474); // 0x048c - 0x18
    BOOST_CHECK(rd.rd_data[0x00] == 0x00);
    BOOST_CHECK(rd.rd_data[0x01] == 0x00);
    BOOST_CHECK(rd.rd_data[0x02] == 0x00);
    BOOST_CHECK(rd.rd_data[0x03] == 0x00);
    BOOST_CHECK(rd.rd_data[0x04] == 0x00);
    BOOST_CHECK(rd.rd_data[0x05] == 0x00);
    BOOST_CHECK(rd.rd_data[0x06] == 0x00);
    BOOST_CHECK(rd.rd_data[0x07] == 0x00);
    // ...
    BOOST_CHECK(rd.rd_data[0x046c] == 0x03);
    BOOST_CHECK(rd.rd_data[0x046d] == 0x00);
    BOOST_CHECK(rd.rd_data[0x046e] == 0x00);
    BOOST_CHECK(rd.rd_data[0x046f] == 0x00);
    BOOST_CHECK(rd.rd_data[0x0470] == 0x00);
    BOOST_CHECK(rd.rd_data[0x0471] == 0x00);
    BOOST_CHECK(rd.rd_data[0x0472] == 0x00);
    BOOST_CHECK(rd.rd_data[0x0473] == 0x7f);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Sdblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/sd_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Sdblock sd;
    sd.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(sd.id[0] == '#');
    BOOST_REQUIRE(sd.id[1] == '#');
    BOOST_REQUIRE(sd.id[2] == 'S');
    BOOST_REQUIRE(sd.id[3] == 'D');
    for (auto value : sd.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(sd.length == 0x0000000000000042);
    // 0x0010
    BOOST_CHECK(sd.link_count == 0x0000000000000000);
    BOOST_REQUIRE(sd.sd_data.size() == 0x2a); // 0x42 - 0x18
    BOOST_CHECK(sd.sd_data[0x00] == 0x26);
    BOOST_CHECK(sd.sd_data[0x01] == 0x00);
    BOOST_CHECK(sd.sd_data[0x02] == 0x00);
    BOOST_CHECK(sd.sd_data[0x03] == 0x00);
    BOOST_CHECK(sd.sd_data[0x04] == 0x31);
    BOOST_CHECK(sd.sd_data[0x05] == 0x20);
    BOOST_CHECK(sd.sd_data[0x06] == 0x52);
    BOOST_CHECK(sd.sd_data[0x07] == 0x65);
    // ...
    BOOST_CHECK(sd.sd_data[0x22] == 0x73);
    BOOST_CHECK(sd.sd_data[0x23] == 0x74);
    BOOST_CHECK(sd.sd_data[0x24] == 0x61);
    BOOST_CHECK(sd.sd_data[0x25] == 0x72);
    BOOST_CHECK(sd.sd_data[0x26] == 0x74);
    BOOST_CHECK(sd.sd_data[0x27] == 0x65);
    BOOST_CHECK(sd.sd_data[0x28] == 0x74);
    BOOST_CHECK(sd.sd_data[0x29] == 0x00);

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Siblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/si_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Siblock si;
    si.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(si.id[0] == '#');
    BOOST_REQUIRE(si.id[1] == '#');
    BOOST_REQUIRE(si.id[2] == 'S');
    BOOST_REQUIRE(si.id[3] == 'I');
    for (auto value : si.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(si.length == 0x0000000000000038);
    // 0x0010
    BOOST_CHECK(si.link_count == 0x0000000000000003);
    BOOST_CHECK(si.si_tx_name == 0x0000000000000000);
    // 0x0020
    BOOST_CHECK(si.si_tx_path == 0x00000000000001f8);
    BOOST_CHECK(si.si_md_comment == 0x0000000000000000);
    // 0x0030
    BOOST_CHECK(si.si_type == 0x01); // ECU
    BOOST_CHECK(si.si_bus_type == 0x02); // CAN
    BOOST_CHECK(si.si_flags == 0x00);
    for (auto value : si.si_reserved) {
        BOOST_CHECK(value == 0x00);
    }

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Srblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/sr_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Srblock sr;
    sr.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(sr.id[0] == '#');
    BOOST_REQUIRE(sr.id[1] == '#');
    BOOST_REQUIRE(sr.id[2] == 'S');
    BOOST_REQUIRE(sr.id[3] == 'R');
    for (auto value : sr.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(sr.length == 0x0000000000000040);
    // 0x0010
    BOOST_CHECK(sr.link_count == 0x0000000000000002);
    BOOST_CHECK(sr.sr_sr_next == 0x0000000000000000);
    // 0x0020
    BOOST_CHECK(sr.sr_data == 0x00000000002ef630);
    BOOST_CHECK(sr.sr_cycle_count == 0x0000000000002582);
    // 0x0030
    BOOST_CHECK(isEqual(sr.sr_interval, 0.01));
    BOOST_CHECK(sr.sr_sync_type == 0x01); // sr_interval contains time interval in seconds
    BOOST_CHECK(sr.sr_flags == 0x00);
    for (auto value : sr.sr_reserved) {
        BOOST_CHECK(value == 0x00);
    }

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

BOOST_AUTO_TEST_CASE(Txblock_000)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/tx_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Txblock tx;
    tx.read(file, 0);
    // 0x0000
    BOOST_REQUIRE(tx.id[0] == '#');
    BOOST_REQUIRE(tx.id[1] == '#');
    BOOST_REQUIRE(tx.id[2] == 'T');
    BOOST_REQUIRE(tx.id[3] == 'X');
    for (auto value : tx.reserved) {
        BOOST_CHECK(value == 0x00);
    }
    BOOST_REQUIRE(tx.length == 0x0000000000000026);
    // 0x0010
    BOOST_CHECK(tx.link_count == 0x0000000000000000);
    std::string text(tx.tx_data.begin(), tx.tx_data.end() - 1);
    BOOST_CHECK(text == "Integer Types");

    /* check end-of-file */
    char c;
    file.fs.read(&c, 1);
    BOOST_REQUIRE(file.fs.eof());

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}
