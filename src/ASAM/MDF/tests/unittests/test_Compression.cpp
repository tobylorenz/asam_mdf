/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <cstdlib>
#include <iostream>

#define BOOST_TEST_MODULE Compression
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <ASAM/MDF.h>
#include <ASAM/MDF/Compression.h>

/*
 * Test the transposition function.
 */
BOOST_AUTO_TEST_CASE(TestTransposition)
{
    /* Set data according to position */
    std::vector<ASAM::MDF::BYTE> dataIn;
    dataIn.resize(8);
    dataIn[0] = 0;
    dataIn[1] = 1;
    dataIn[2] = 2;
    dataIn[3] = 3;
    dataIn[4] = 4;
    dataIn[5] = 5;
    dataIn[6] = 6;
    dataIn[7] = 7;

    /* Transposition */
    std::vector<ASAM::MDF::BYTE> dataTransposed;
    ASAM::MDF::UINT32 mc = 3;
    ASAM::MDF::UINT32 nc = dataIn.size() % mc;
    ASAM::MDF::transposeData(dataTransposed, dataIn, mc, nc);
    BOOST_REQUIRE(dataTransposed.size() == 8);
    BOOST_CHECK(dataTransposed[0] == 0);
    BOOST_CHECK(dataTransposed[1] == 3);
    BOOST_CHECK(dataTransposed[2] == 1);
    BOOST_CHECK(dataTransposed[3] == 4);
    BOOST_CHECK(dataTransposed[4] == 2);
    BOOST_CHECK(dataTransposed[5] == 5);
    BOOST_CHECK(dataTransposed[6] == 6); // outside of 3*2 table
    BOOST_CHECK(dataTransposed[7] == 7);

    /* Transposition */
    std::vector<ASAM::MDF::BYTE> dataOut;
    ASAM::MDF::transposeData(dataOut, dataTransposed, nc, mc);
    BOOST_REQUIRE(dataOut.size() == 8);
    BOOST_CHECK(dataOut[0] == 0);
    BOOST_CHECK(dataOut[1] == 1);
    BOOST_CHECK(dataOut[2] == 2);
    BOOST_CHECK(dataOut[3] == 3);
    BOOST_CHECK(dataOut[4] == 4);
    BOOST_CHECK(dataOut[5] == 5);
    BOOST_CHECK(dataOut[6] == 6); // outside of 3*2 table
    BOOST_CHECK(dataOut[7] == 7);
}

BOOST_AUTO_TEST_CASE(TestDt0)
{
    ASAM::MDF::Dtblock dtIn;
    ASAM::MDF::Dzblock dz;
    ASAM::MDF::Dtblock dtOut;
    uint16_t size = rand() % 1024;

    /* random data */
    dtIn.dt_data.resize(size);
    for (unsigned int i = 0; i < dtIn.dt_data.size(); ++i) {
        dtIn.dt_data[i] = rand() % 255;
    }

    /* Compress */
    dz.dz_zip_type = 0; // Deflate
    dz.dz_zip_parameter = 0;
    dz.deflate(dtIn);
    BOOST_CHECK(dz.dz_data.size() == dz.dz_data_length);
    BOOST_CHECK(dz.dz_org_block_type[0] == 'D');
    BOOST_CHECK(dz.dz_org_block_type[1] == 'T');
    BOOST_CHECK(dz.dz_org_data_length == dtIn.dt_data.size());

    /* Uncompress */
    dz.inflate(dtOut);
    BOOST_REQUIRE(dtOut.dt_data.size() == dtIn.dt_data.size());
    for (unsigned int i = 0; i < dtOut.dt_data.size(); ++i) {
        BOOST_CHECK(dtOut.dt_data[i] == dtIn.dt_data[i]);
    }
}

BOOST_AUTO_TEST_CASE(TestDt1)
{
    ASAM::MDF::Dtblock dtIn;
    ASAM::MDF::Dzblock dz;
    ASAM::MDF::Dtblock dtOut;
    uint16_t columns = 2 + rand() % 9; // 2..10
    uint16_t size = columns * (1 + (rand() % 10)); // columns * 1..10

    /* random data */
    dtIn.dt_data.resize(size);
    for (unsigned int i = 0; i < dtIn.dt_data.size(); ++i) {
        dtIn.dt_data[i] = rand() % 255;
    }

    /* Compress */
    dz.dz_zip_type = 1; // Transposition + Deflate
    dz.dz_zip_parameter = columns;
    dz.deflate(dtIn);
    BOOST_CHECK(dz.dz_data.size() == dz.dz_data_length);
    BOOST_CHECK(dz.dz_org_block_type[0] == 'D');
    BOOST_CHECK(dz.dz_org_block_type[1] == 'T');
    BOOST_CHECK(dz.dz_org_data_length == dtIn.dt_data.size());

    /* Uncompress */
    dz.inflate(dtOut);
    BOOST_REQUIRE(dtOut.dt_data.size() == dtIn.dt_data.size());
    for (unsigned int i = 0; i < dtOut.dt_data.size(); ++i) {
        BOOST_CHECK(dtOut.dt_data[i] == dtIn.dt_data[i]);
    }
}

BOOST_AUTO_TEST_CASE(TestSd0)
{
    ASAM::MDF::Sdblock sdIn;
    ASAM::MDF::Dzblock dz;
    ASAM::MDF::Sdblock sdOut;
    uint16_t size = rand() % 1024;

    /* random data */
    sdIn.sd_data.resize(size);
    for (unsigned int i = 0; i < sdIn.sd_data.size(); ++i) {
        sdIn.sd_data[i] = rand() % 255;
    }

    /* Compress */
    dz.dz_zip_type = 0; // Deflate
    dz.dz_zip_parameter = 0;
    dz.deflate(sdIn);
    BOOST_CHECK(dz.dz_data.size() == dz.dz_data_length);
    BOOST_CHECK(dz.dz_org_block_type[0] == 'S');
    BOOST_CHECK(dz.dz_org_block_type[1] == 'D');
    BOOST_CHECK(dz.dz_org_data_length == sdIn.sd_data.size());

    /* Uncompress */
    dz.inflate(sdOut);
    BOOST_REQUIRE(sdOut.sd_data.size() == sdIn.sd_data.size());
    for (unsigned int i = 0; i < sdOut.sd_data.size(); ++i) {
        BOOST_CHECK(sdOut.sd_data[i] == sdIn.sd_data[i]);
    }
}

BOOST_AUTO_TEST_CASE(TestSd1)
{
    ASAM::MDF::Sdblock sdIn;
    ASAM::MDF::Dzblock dz;
    ASAM::MDF::Sdblock sdOut;
    uint16_t columns = 2 + rand() % 9; // 2..10
    uint16_t size = columns * (1 + (rand() % 10)); // columns * 1..10

    /* random data */
    sdIn.sd_data.resize(size);
    for (unsigned int i = 0; i < sdIn.sd_data.size(); ++i) {
        sdIn.sd_data[i] = rand() % 255;
    }

    /* Compress */
    dz.dz_zip_type = 1; // Deflate
    dz.dz_zip_parameter = columns;
    dz.deflate(sdIn);
    BOOST_CHECK(dz.dz_data.size() == dz.dz_data_length);
    BOOST_CHECK(dz.dz_org_block_type[0] == 'S');
    BOOST_CHECK(dz.dz_org_block_type[1] == 'D');
    BOOST_CHECK(dz.dz_org_data_length == sdIn.sd_data.size());

    /* Uncompress */
    dz.inflate(sdOut);
    BOOST_REQUIRE(sdOut.sd_data.size() == sdIn.sd_data.size());
    for (unsigned int i = 0; i < sdOut.sd_data.size(); ++i) {
        BOOST_CHECK(sdOut.sd_data[i] == sdIn.sd_data[i]);
    }
}

BOOST_AUTO_TEST_CASE(TestRd0)
{
    ASAM::MDF::Rdblock rdIn;
    ASAM::MDF::Dzblock dz;
    ASAM::MDF::Rdblock rdOut;
    uint16_t size = rand() % 1024;

    /* random data */
    rdIn.rd_data.resize(size);
    for (unsigned int i = 0; i < rdIn.rd_data.size(); ++i) {
        rdIn.rd_data[i] = rand() % 255;
    }

    /* Compress */
    dz.dz_zip_type = 0; // Deflate
    dz.dz_zip_parameter = 0;
    dz.deflate(rdIn);
    BOOST_CHECK(dz.dz_data.size() == dz.dz_data_length);
    BOOST_CHECK(dz.dz_org_block_type[0] == 'R');
    BOOST_CHECK(dz.dz_org_block_type[1] == 'D');
    BOOST_CHECK(dz.dz_org_data_length == rdIn.rd_data.size());

    /* Uncompress */
    dz.inflate(rdOut);
    BOOST_REQUIRE(rdOut.rd_data.size() == rdIn.rd_data.size());
    for (unsigned int i = 0; i < rdOut.rd_data.size(); ++i) {
        BOOST_CHECK(rdOut.rd_data[i] == rdIn.rd_data[i]);
    }
}

BOOST_AUTO_TEST_CASE(TestRd1)
{
    ASAM::MDF::Rdblock rdIn;
    ASAM::MDF::Dzblock dz;
    ASAM::MDF::Rdblock rdOut;
    uint16_t columns = 2 + rand() % 9; // 2..10
    uint16_t size = columns * (1 + (rand() % 10)); // columns * 1..10

    /* random data */
    rdIn.rd_data.resize(size);
    for (unsigned int i = 0; i < rdIn.rd_data.size(); ++i) {
        rdIn.rd_data[i] = rand() % 255;
    }

    /* Compress */
    dz.dz_zip_type = 1; // Deflate
    dz.dz_zip_parameter = columns;
    dz.deflate(rdIn);
    BOOST_CHECK(dz.dz_data.size() == dz.dz_data_length);
    BOOST_CHECK(dz.dz_org_block_type[0] == 'R');
    BOOST_CHECK(dz.dz_org_block_type[1] == 'D');
    BOOST_CHECK(dz.dz_org_data_length == rdIn.rd_data.size());

    /* Uncompress */
    dz.inflate(rdOut);
    BOOST_REQUIRE(rdOut.rd_data.size() == rdIn.rd_data.size());
    for (unsigned int i = 0; i < rdOut.rd_data.size(); ++i) {
        BOOST_CHECK(rdOut.rd_data[i] == rdIn.rd_data[i]);
    }
}

/*
 * The compressed block is taken from an example.
 * This test only checks what was used as compression
 * setting. It shows that default compression was used.
 */
BOOST_AUTO_TEST_CASE(DefaultCompression)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/blocks/dz_000.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    ASAM::MDF::Dzblock dz;
    dz.read(file, 0);

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());

    /* inflate data */
    std::vector<ASAM::MDF::BYTE> inflatedData;
    inflatedData.resize(dz.dz_org_data_length);
    ASAM::MDF::inflateData(inflatedData, dz.dz_data);

    /* deflate data */
    std::vector<ASAM::MDF::BYTE> deflatedData;
    ASAM::MDF::deflateData(deflatedData, inflatedData);
    BOOST_REQUIRE(deflatedData.size() == dz.dz_data.size());
    BOOST_CHECK_EQUAL_COLLECTIONS(dz.dz_data.begin(), dz.dz_data.end(), deflatedData.begin(), deflatedData.end());

    // Z_DEFAULT_COMPRESSION is the default compression.
    // All other values resulted in different results.
}

/*
 * This tests that an exception is thrown in deflateData.
 */
BOOST_AUTO_TEST_CASE(DeflateDataException)
{
    std::vector<ASAM::MDF::BYTE> dataOut;
    std::vector<ASAM::MDF::BYTE> dataIn; // no data in

    /* check exception */
    bool exception = false;
    try {
        ASAM::MDF::deflateData(dataOut, dataIn, 10); // undefined compression level
    } catch (ASAM::MDF::CompressionError & e) {
        std::cout << e.what() << std::endl;
        exception = true;
    }
    BOOST_CHECK(exception);
}

/*
 * This tests that an exception is thrown in deflateData.
 */
BOOST_AUTO_TEST_CASE(InflateDataException)
{
    std::vector<ASAM::MDF::BYTE> dataOut;
    std::vector<ASAM::MDF::BYTE> dataIn; // no data in

    /* check exception */
    bool exception = false;
    try {
        ASAM::MDF::inflateData(dataOut, dataIn);
    } catch (ASAM::MDF::CompressionError & e) {
        std::cout << e.what() << std::endl;
        exception = true;
    }
    BOOST_CHECK(exception);
}
