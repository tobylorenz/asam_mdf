/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <cstdlib>
#include <iostream>

#define BOOST_TEST_MODULE Endianess
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>

#include <ASAM/MDF.h>
#include <ASAM/MDF/Endianess.h>

/*
 * Test the transposition function.
 */
BOOST_AUTO_TEST_CASE(TestByteSwap)
{
    uint16_t ui16a = 0x1122;
    uint16_t ui16b = ASAM::MDF::byteSwap(ui16a);
    BOOST_CHECK(ui16b = 0x2211);

    uint32_t ui32a = 0x11223344;
    uint32_t ui32b = ASAM::MDF::byteSwap(ui32a);
    BOOST_CHECK(ui32b = 0x44332211);

    uint64_t ui64a = 0x1122334455667788;
    uint64_t ui64b = ASAM::MDF::byteSwap(ui64a);
    BOOST_CHECK(ui64b = 0x8877665544332211);
}
