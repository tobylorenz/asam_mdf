/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <fstream>
#include <iostream>
#include <string>

#define BOOST_TEST_MODULE AllBlocks
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>

#include <ASAM/MDF.h>

/*
 * Read all files from the input directory and write the same information
 * to the output directory.
 */
BOOST_AUTO_TEST_CASE(AllBlocks)
{
    /* input directory */
    boost::filesystem::path indir(CMAKE_CURRENT_SOURCE_DIR "/blocks/");

    /* output directory */
    boost::filesystem::path outdir(CMAKE_CURRENT_BINARY_DIR "/blocks/");
    if (!exists(outdir)) {
        BOOST_REQUIRE(create_directory(outdir));
    }

    for (boost::filesystem::directory_entry & x : boost::filesystem::directory_iterator(indir)) {
        std::string eventFile = x.path().filename().string();
        std::cout << eventFile << std::endl;

        /* open input file */
        ASAM::MDF::File filein;
        boost::filesystem::path infile(CMAKE_CURRENT_SOURCE_DIR "/blocks/" + eventFile);
        filein.fs.open(infile.string(), std::fstream::in);
        BOOST_REQUIRE(filein.fs.is_open());

        /* open output file */
        ASAM::MDF::File fileout;
        boost::filesystem::path outfile(CMAKE_CURRENT_BINARY_DIR "/blocks/" + eventFile);
        fileout.fs.open(outfile.string(), std::fstream::out);
        BOOST_REQUIRE(fileout.fs.is_open());

        /* get block type */
        ASAM::MDF::LINK link = 0;
        ASAM::MDF::BlockType blockType = ASAM::MDF::Block::blockType(filein, link);

        /* copy blocks */
        if (blockType == ASAM::MDF::BlockType::Unknown) {
            ASAM::MDF::Idblock idblock;
            idblock.read(filein);
            idblock.write(fileout);
        } else {
            /* read block */
            ASAM::MDF::Block * block = nullptr;
            switch (blockType) {
            case ASAM::MDF::BlockType::AT:
                block = new ASAM::MDF::Atblock();
                break;
            case ASAM::MDF::BlockType::CA:
                block = new ASAM::MDF::Cablock();
                break;
            case ASAM::MDF::BlockType::CC:
                block = new ASAM::MDF::Ccblock();
                break;
            case ASAM::MDF::BlockType::CG:
                block = new ASAM::MDF::Cgblock();
                break;
            case ASAM::MDF::BlockType::CH:
                block = new ASAM::MDF::Chblock();
                break;
            case ASAM::MDF::BlockType::CN:
                block = new ASAM::MDF::Cnblock();
                break;
            case ASAM::MDF::BlockType::DG:
                block = new ASAM::MDF::Dgblock();
                break;
            case ASAM::MDF::BlockType::DL:
                block = new ASAM::MDF::Dlblock();
                break;
            case ASAM::MDF::BlockType::DT:
                block = new ASAM::MDF::Dtblock();
                break;
            case ASAM::MDF::BlockType::DZ:
                block = new ASAM::MDF::Dzblock();
                break;
            case ASAM::MDF::BlockType::EV:
                block = new ASAM::MDF::Evblock();
                break;
            case ASAM::MDF::BlockType::FH:
                block = new ASAM::MDF::Fhblock();
                break;
            case ASAM::MDF::BlockType::HD:
                block = new ASAM::MDF::Hdblock();
                break;
            case ASAM::MDF::BlockType::HL:
                block = new ASAM::MDF::Hlblock();
                break;
            case ASAM::MDF::BlockType::MD:
                block = new ASAM::MDF::Mdblock();
                break;
            case ASAM::MDF::BlockType::RD:
                block = new ASAM::MDF::Rdblock();
                break;
            case ASAM::MDF::BlockType::SD:
                block = new ASAM::MDF::Sdblock();
                break;
            case ASAM::MDF::BlockType::SI:
                block = new ASAM::MDF::Siblock();
                break;
            case ASAM::MDF::BlockType::SR:
                block = new ASAM::MDF::Srblock();
                break;
            case ASAM::MDF::BlockType::TX:
                block = new ASAM::MDF::Txblock();
                break;
            case ASAM::MDF::BlockType::Unknown:
                break;
            }

            /* copy block */
            if (block) {
                block->read(filein, link);
                block->write(fileout, link);
                delete block;
                BOOST_REQUIRE(link == 0);
            }
        }

        /* close files */
        filein.close();
        fileout.close();

        /* compare files */
        std::ifstream ifs1(infile.c_str());
        std::ifstream ifs2(outfile.c_str());
        std::istream_iterator<char> b1(ifs1), e1;
        std::istream_iterator<char> b2(ifs2), e2;
        BOOST_CHECK_EQUAL_COLLECTIONS(b1, e1, b2, e2);
    }
}
