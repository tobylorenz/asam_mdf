/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>

#define BOOST_TEST_MODULE File
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include <boost/test/unit_test.hpp>
#include <boost/filesystem.hpp>

#include <ASAM/MDF.h>

static bool isEqual(double a, double b, double precision = 0.000000001) // precision is nano (10^-9)
{
    return ((a - b) < precision) && ((b - a) < precision);
}

/*
 * Test standard file open/save operations.
 */
BOOST_AUTO_TEST_CASE(FileOpenSave)
{
    /* input directory */
    boost::filesystem::path indir(CMAKE_CURRENT_SOURCE_DIR "/data/");

    /* output directory */
    boost::filesystem::path outdir(CMAKE_CURRENT_BINARY_DIR "/data/");
    if (!exists(outdir)) {
        BOOST_REQUIRE(create_directory(outdir));
    }

    /* open file */
    ASAM::MDF::File filein;
    std::string fileinName = CMAKE_CURRENT_SOURCE_DIR "/data/FileFinalized.mf4";
    filein.open(fileinName, ASAM::MDF::File::OpenMode::Read);
    BOOST_REQUIRE(filein.is_open());

    /* check idblock */
    BOOST_CHECK(filein.id.finalized() == true);
    BOOST_CHECK(filein.id.id_ver == 411);
    BOOST_CHECK(filein.id.id_unfin_flags == 0);
    BOOST_CHECK(filein.id.id_custom_unfin_flags == 0);

    /* check hdblock */
    BOOST_CHECK(filein.hd.hd_dg_first == 0x0000000000000001);
    BOOST_CHECK(filein.hd.hd_fh_first == 0x0000000000000002);
    BOOST_CHECK(filein.hd.hd_ch_first == 0x0000000000000003);
    BOOST_CHECK(filein.hd.hd_at_first == 0x0000000000000004);
    BOOST_CHECK(filein.hd.hd_ev_first == 0x0000000000000005);
    BOOST_CHECK(filein.hd.hd_md_comment == 0x0000000000000006);
    BOOST_CHECK(filein.hd.hd_start_time_ns == 0x0000000000000007);
    BOOST_CHECK(filein.hd.hd_tz_offset_min == 0x0008);
    BOOST_CHECK(filein.hd.hd_dst_offset_min == 0x0009);
    BOOST_CHECK(filein.hd.hd_time_flags == 0x0a);
    BOOST_CHECK(filein.hd.hd_time_class == 0x0b);
    BOOST_CHECK(filein.hd.hd_flags == 0x0c);
    BOOST_CHECK(isEqual(filein.hd.hd_start_angle_rad, 0.0));
    BOOST_CHECK(isEqual(filein.hd.hd_start_distance_m, 0.0));

    /* check end-of-file */
    char c;
    filein.fs.read(&c, 1);
    BOOST_REQUIRE(filein.eof());

    /* close file */
    filein.close();
    BOOST_REQUIRE(!filein.is_open());

    /* open file */
    ASAM::MDF::File fileout;
    std::string fileoutName = CMAKE_CURRENT_BINARY_DIR "/data/FileFinalized.mf4";
    fileout.open(fileoutName, ASAM::MDF::File::OpenMode::Write);
    BOOST_REQUIRE(fileout.is_open());

    /* copy blocks */
    fileout.id = filein.id;
    fileout.hd = filein.hd;
    fileout.id.write(fileout);
    ASAM::MDF::LINK link = 64;
    fileout.hd.write(fileout, link);

    /* close file */
    fileout.close();
    BOOST_REQUIRE(!fileout.is_open());

    /* open file */
    ASAM::MDF::File fileapp;
    fileapp.open(fileoutName, ASAM::MDF::File::OpenMode::Append);
    BOOST_REQUIRE(fileapp.is_open());

    /* check idblock */
    BOOST_CHECK(fileapp.id.finalized() == true);
    BOOST_CHECK(fileapp.id.id_ver == 411);
    BOOST_CHECK(fileapp.id.id_unfin_flags == 0);
    BOOST_CHECK(fileapp.id.id_custom_unfin_flags == 0);

    /* check hdblock */
    BOOST_CHECK(fileapp.hd.hd_dg_first == 0x0000000000000001);
    BOOST_CHECK(fileapp.hd.hd_fh_first == 0x0000000000000002);
    BOOST_CHECK(fileapp.hd.hd_ch_first == 0x0000000000000003);
    BOOST_CHECK(fileapp.hd.hd_at_first == 0x0000000000000004);
    BOOST_CHECK(fileapp.hd.hd_ev_first == 0x0000000000000005);
    BOOST_CHECK(fileapp.hd.hd_md_comment == 0x0000000000000006);
    BOOST_CHECK(fileapp.hd.hd_start_time_ns == 0x0000000000000007);
    BOOST_CHECK(fileapp.hd.hd_tz_offset_min == 0x0008);
    BOOST_CHECK(fileapp.hd.hd_dst_offset_min == 0x0009);
    BOOST_CHECK(fileapp.hd.hd_time_flags == 0x0a);
    BOOST_CHECK(fileapp.hd.hd_time_class == 0x0b);
    BOOST_CHECK(fileapp.hd.hd_flags == 0x0c);
    BOOST_CHECK(isEqual(fileapp.hd.hd_start_angle_rad, 0.0));
    BOOST_CHECK(isEqual(fileapp.hd.hd_start_distance_m, 0.0));

    /* close file */
    fileapp.close();
    BOOST_REQUIRE(!fileapp.is_open());
}

/*
 * Test that file unfinalized file is recognized.
 */
BOOST_AUTO_TEST_CASE(FileOpenUnfinalized)
{
    /* open file */
    ASAM::MDF::File file;
    file.open(CMAKE_CURRENT_SOURCE_DIR "/data/FileUnfinalized.mf4", ASAM::MDF::File::OpenMode::Read);
    BOOST_REQUIRE(file.is_open());

    /* check idblock */
    BOOST_CHECK(file.id.finalized() == false);
    BOOST_CHECK(file.id.id_ver == 411);
    BOOST_CHECK(file.id.id_unfin_flags == 0);
    BOOST_CHECK(file.id.id_custom_unfin_flags == 0);

    /* close file */
    file.close();
    BOOST_REQUIRE(!file.is_open());
}

/*
 * Test the UpdateFinalized function.
 */
BOOST_AUTO_TEST_CASE(UpdateFinalized)
{
    ASAM::MDF::Idblock id;
    BOOST_CHECK(id.finalized());

    /* set to unfinalized */
    id.id_unfin_flags = 1;
    id.updateFinalized();
    BOOST_CHECK(!id.finalized());

    /* set to finalized */
    id.id_unfin_flags = 0;
    id.updateFinalized();
    BOOST_CHECK(id.finalized());
}

/*
 * Test for exception if version too low.
 */
BOOST_AUTO_TEST_CASE(Version3)
{
    /* open file */
    ASAM::MDF::File file;
    bool exception = false;
    try {
        file.open(CMAKE_CURRENT_SOURCE_DIR "/data/Version3.mf4", ASAM::MDF::File::OpenMode::Read);
    } catch (ASAM::MDF::UnsupportedVersion & e) {
        std::cout << e.what() << std::endl;
        exception = true;
    }
    BOOST_CHECK(exception);
    BOOST_REQUIRE(file.is_open());

    /* check idblock */
    BOOST_CHECK(file.id.id_ver == 330);

    /* close file */
    file.close();
    BOOST_REQUIRE(!file.is_open());

    /* append file */
    exception = false;
    try {
        file.open(CMAKE_CURRENT_SOURCE_DIR "/data/Version3.mf4", ASAM::MDF::File::OpenMode::Append);
    } catch (ASAM::MDF::UnsupportedVersion & e) {
        std::cout << e.what() << std::endl;
        exception = true;
    }
    BOOST_CHECK(exception);
    BOOST_REQUIRE(file.is_open());

    /* check idblock */
    BOOST_CHECK(file.id.id_ver == 330);

    /* close file */
    file.close();
    BOOST_REQUIRE(!file.is_open());
}

/*
 * Test for exception if version too high.
 */
BOOST_AUTO_TEST_CASE(Version5)
{
    /* open file */
    ASAM::MDF::File file;
    bool exception = false;
    try {
        file.open(CMAKE_CURRENT_SOURCE_DIR "/data/Version5.mf4", ASAM::MDF::File::OpenMode::Read);
    } catch (ASAM::MDF::UnsupportedVersion & e) {
        std::cout << e.what() << std::endl;
        exception = true;
    }
    BOOST_CHECK(exception);
    BOOST_REQUIRE(file.is_open());

    /* check idblock */
    BOOST_CHECK(file.id.id_ver == 500);

    /* close file */
    file.close();
    BOOST_REQUIRE(!file.is_open());

    /* append file */
    exception = false;
    try {
        file.open(CMAKE_CURRENT_SOURCE_DIR "/data/Version5.mf4", ASAM::MDF::File::OpenMode::Append);
    } catch (ASAM::MDF::UnsupportedVersion & e) {
        std::cout << e.what() << std::endl;
        exception = true;
    }
    BOOST_CHECK(exception);
    BOOST_REQUIRE(file.is_open());

    /* check idblock */
    BOOST_CHECK(file.id.id_ver == 500);

    /* close file */
    file.close();
    BOOST_REQUIRE(!file.is_open());
}

/*
 * Test file not open in all modes.
 */
BOOST_AUTO_TEST_CASE(FileNotOpen)
{
    ASAM::MDF::File file;

    /* open file for read */
    file.open(CMAKE_CURRENT_SOURCE_DIR "/NoDir/NoFile.mf4", ASAM::MDF::File::OpenMode::Read);
    BOOST_CHECK(!file.is_open());

    /* open file for write */
    file.open(CMAKE_CURRENT_BINARY_DIR "/NoDir/NoFile.mf4", ASAM::MDF::File::OpenMode::Write);
    BOOST_CHECK(!file.is_open());

    /* open file for write */
    file.open(CMAKE_CURRENT_BINARY_DIR "/NoDir/NoFile.mf4", ASAM::MDF::File::OpenMode::Append);
    BOOST_CHECK(!file.is_open());
}

/*
 * Test for wrong file identifier exception.
 */
BOOST_AUTO_TEST_CASE(WrongFileIdentifier)
{
    /* open file for read */
    ASAM::MDF::File file;
    bool exception = false;
    try {
        file.open(CMAKE_CURRENT_SOURCE_DIR "/data/WrongIdentifier.mf4", ASAM::MDF::File::OpenMode::Read);
    } catch  (ASAM::MDF::UnexpectedIdentifier & e) {
        std::cout << e.what() << std::endl;
        exception = true;
    }
    BOOST_CHECK(exception);

    /* close file */
    file.close();
    BOOST_REQUIRE(!file.is_open());
}

/*
 * Test undefined block identifier exception.
 */
BOOST_AUTO_TEST_CASE(WrongBlockIdentifier)
{
    /* open file */
    ASAM::MDF::File file;
    std::string fileinName = CMAKE_CURRENT_SOURCE_DIR "/data/FileFinalized.mf4";
    file.open(fileinName, ASAM::MDF::File::OpenMode::Read);
    BOOST_REQUIRE(file.is_open());

    /* now read somewhere, where there is definitely no block. */
    bool exception = false;
    try {
        ASAM::MDF::Block block;
        block.read(file, 8);
    } catch (ASAM::MDF::UnexpectedIdentifier & e) {
        std::cout << e.what() << std::endl;
        exception = true;
    }
    BOOST_CHECK(exception);

    /* close file */
    file.close();
    BOOST_CHECK(!file.is_open());
}

/*
 * Test eight byte alignment.
 */
BOOST_AUTO_TEST_CASE(EightByteAlignment)
{
    /* open file for read */
    ASAM::MDF::File file;
    file.open(CMAKE_CURRENT_BINARY_DIR "/data/EightByteAlignment.mf4", ASAM::MDF::File::OpenMode::Write);
    BOOST_REQUIRE(file.is_open());

    /* Tx Block */
    ASAM::MDF::LINK link = 0;
    ASAM::MDF::Txblock tx1;
    std::string text = "Test";
    tx1.tx_data.resize(text.size() + 1);
    std::copy(text.begin(), text.end(), tx1.tx_data.begin());
    BOOST_CHECK(tx1.tx_data.size() == 5);
    tx1.write(file, link);
    BOOST_CHECK(link == 0xa8);
    BOOST_CHECK(file.fs.tellp() == 0xc5);

    /* Tx Block */
    link = 0;
    ASAM::MDF::Txblock tx2;
    tx2.tx_data.resize(text.size() + 1);
    std::copy(text.begin(), text.end(), tx2.tx_data.begin());
    BOOST_CHECK(tx2.tx_data.size() == 5);
    tx2.write(file, link);
    BOOST_CHECK(link == 0xc8); // and not 0xc5

    /* close file */
    file.close();
    BOOST_REQUIRE(!file.is_open());
}

/*
 * Test for block size too low exception.
 */
BOOST_AUTO_TEST_CASE(Cgblock_low)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/data/cg_low.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    bool exception = false;
    try {
        ASAM::MDF::Cgblock cg;
        cg.read(file, 0);
    } catch (ASAM::MDF::UnexpectedBlockSize & e) {
        std::cout << e.what() << std::endl;
        exception = true;
    }
    BOOST_CHECK(exception);

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

/*
 * Test for block size too high exception.
 */
BOOST_AUTO_TEST_CASE(Cgblock_high)
{
    /* open file */
    ASAM::MDF::File file;
    file.fs.open(CMAKE_CURRENT_SOURCE_DIR "/data/cg_high.mf4block", std::fstream::in);
    BOOST_REQUIRE(file.fs.is_open());

    /* check block */
    bool exception = false;
    try {
        ASAM::MDF::Cgblock cg;
        cg.read(file, 0);
    } catch (ASAM::MDF::UnexpectedBlockSize & e) {
        std::cout << e.what() << std::endl;
        exception = true;
    }
    BOOST_CHECK(exception);

    /* close file */
    file.fs.close();
    BOOST_REQUIRE(!file.fs.is_open());
}

/*
 * Test the setVersion function.
 */
BOOST_AUTO_TEST_CASE(SetVersion)
{
    ASAM::MDF::Idblock id;
    BOOST_CHECK(id.id_vers[0] == '4');
    BOOST_CHECK(id.id_vers[1] == '.');
    BOOST_CHECK(id.id_vers[2] == '1');
    BOOST_CHECK(id.id_vers[3] == '1');
    BOOST_CHECK(id.id_vers[4] == ' ');
    BOOST_CHECK(id.id_vers[5] == ' ');
    BOOST_CHECK(id.id_vers[6] == ' ');
    BOOST_CHECK(id.id_vers[7] == ' ');
    BOOST_CHECK(id.id_ver == 411);

    id.setVersion(331);
    BOOST_CHECK(id.id_vers[0] == '3');
    BOOST_CHECK(id.id_vers[1] == '.');
    BOOST_CHECK(id.id_vers[2] == '3');
    BOOST_CHECK(id.id_vers[3] == '1');
    BOOST_CHECK(id.id_vers[4] == ' ');
    BOOST_CHECK(id.id_vers[5] == ' ');
    BOOST_CHECK(id.id_vers[6] == ' ');
    BOOST_CHECK(id.id_vers[7] == ' ');
    BOOST_CHECK(id.id_ver == 331);
}
