#include <array>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "ASAM/MDF.h"

/* forward declarations */
void showIdblock(ASAM::MDF::File & file, ASAM::MDF::Idblock & id, std::string indention);
void showHdblock(ASAM::MDF::File & file, ASAM::MDF::Hdblock & hd, std::string indention);
void showMdblock(ASAM::MDF::File & file, ASAM::MDF::Mdblock & md, std::string indention);
void showTxblock(ASAM::MDF::File & file, ASAM::MDF::Txblock & tx, std::string indention);
void showFhblock(ASAM::MDF::File & file, ASAM::MDF::Fhblock & fh, std::string indention);
void showChblock(ASAM::MDF::File & file, ASAM::MDF::Chblock & ch, std::string indention);
void showAtblock(ASAM::MDF::File & file, ASAM::MDF::Atblock & at, std::string indention);
void showEvblock(ASAM::MDF::File & file, ASAM::MDF::Evblock & ev, std::string indention);
void showDgblock(ASAM::MDF::File & file, ASAM::MDF::Dgblock & dg, std::string indention);
void showCgblock(ASAM::MDF::File & file, ASAM::MDF::Cgblock & cg, std::string indention);
void showSiblock(ASAM::MDF::File & file, ASAM::MDF::Siblock & si, std::string indention);
void showCnblock(ASAM::MDF::File & file, ASAM::MDF::Cnblock & cn, std::string indention);
void showCcblock(ASAM::MDF::File & file, ASAM::MDF::Ccblock & cc, std::string indention);
void showCablock(ASAM::MDF::File & file, ASAM::MDF::Cablock & ca, std::string indention);
void showDtblock(ASAM::MDF::File & file, ASAM::MDF::Dtblock & dt, std::string indention);
void showSrblock(ASAM::MDF::File & file, ASAM::MDF::Srblock & sr, std::string indention);
void showRdblock(ASAM::MDF::File & file, ASAM::MDF::Rdblock & rd, std::string indention);
void showSdblock(ASAM::MDF::File & file, ASAM::MDF::Sdblock & sd, std::string indention);
void showDlblock(ASAM::MDF::File & file, ASAM::MDF::Dlblock & dl, std::string indention);
void showDzblock(ASAM::MDF::File & file, ASAM::MDF::Dzblock & dz, std::string indention);
void showHlblock(ASAM::MDF::File & file, ASAM::MDF::Hlblock & hl, std::string indention);

/**
 * Show IDBLOCK
 *
 * @param id IDBLOCK
 * @param indention Level of indention
 */
void showIdblock(ASAM::MDF::File &, ASAM::MDF::Idblock & id, std::string indention)
{
    /* prepare data */
    char id_file[sizeof(id.id_file) + 1];
    memset(id_file, 0, sizeof(id_file));
    memcpy(id_file, id.id_file.data(), sizeof(id.id_file));

    char id_vers[sizeof(id.id_vers) + 1];
    memset(id_vers, 0, sizeof(id_vers));
    memcpy(id_vers, id.id_vers.data(), sizeof(id.id_vers));

    char id_prog[sizeof(id.id_prog) + 1];
    memset(id_prog, 0, sizeof(id_prog));
    memcpy(id_prog, id.id_prog.data(), sizeof(id.id_prog));

    /* show data */
    std::cout
            << indention
            << "ID"
            << " file=" << id_file
            << " vers=" << id_vers
            << " prog=" << id_prog
            << " ver=" << id.id_ver
            << " unfin_flags=" << id.id_unfin_flags
            << " custom_unfin_flags=" << id.id_custom_unfin_flags
            << std::endl;
}

/**
 * Show HDBLOCK
 *
 * @param file MDF file
 * @param hd HDBLOCK
 * @param indention Level of indention
 */
void showHdblock(ASAM::MDF::File & file, ASAM::MDF::Hdblock & hd, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "HD"
            << " start_time_ns=" << hd.hd_start_time_ns
            << " tz_offset_min=" << hd.hd_tz_offset_min
            << " dst_offset_min=" << hd.hd_dst_offset_min
            << " time_flags=" << static_cast<uint16_t>(hd.hd_time_flags)
            << " time_class=" << static_cast<uint16_t>(hd.hd_time_class)
            << " flags=" << static_cast<uint16_t>(hd.hd_flags)
            << " start_angle_rad=" << hd.hd_start_angle_rad
            << " start_distance_m=" << hd.hd_start_distance_m
            << std::endl;

    /* follow links */
    ASAM::MDF::LINK dglink = hd.hd_dg_first;
    while (dglink) {
        ASAM::MDF::Dgblock dgblock;
        dgblock.read(file, dglink);
        showDgblock(file, dgblock, indention + " ");
        dglink = dgblock.dg_dg_next;
    }
    ASAM::MDF::LINK fhlink = hd.hd_fh_first;
    while (fhlink) {
        ASAM::MDF::Fhblock fhblock;
        fhblock.read(file, fhlink);
        showFhblock(file, fhblock, indention + " ");
        fhlink = fhblock.fh_fh_next;
    }
    ASAM::MDF::LINK chlink = hd.hd_ch_first;
    while (chlink) {
        ASAM::MDF::Chblock chblock;
        chblock.read(file, chlink);
        showChblock(file, chblock, indention + " ");
        chlink = chblock.ch_ch_next;
    }
    ASAM::MDF::LINK atlink = hd.hd_at_first;
    while (atlink) {
        ASAM::MDF::Atblock atblock;
        atblock.read(file, atlink);
        showAtblock(file, atblock, indention + " ");
        atlink = atblock.at_at_next;
    }
    ASAM::MDF::LINK evlink = hd.hd_ev_first;
    while (evlink) {
        ASAM::MDF::Evblock evblock;
        evblock.read(file, evlink);
        showEvblock(file, evblock, indention + " ");
        evlink = evblock.ev_ev_next;
    }
    ASAM::MDF::LINK mdlink = hd.hd_md_comment;
    if (mdlink) {
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
}

/**
 * Show MDBLOCK
 *
 * @param md MDBLOCK
 * @param indention Level of indention
 */
void showMdblock(ASAM::MDF::File &, ASAM::MDF::Mdblock & md, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "MD"
            << " data=" << md.md_data.data()
            << std::endl;
}

/**
 * Show TXBLOCK
 *
 * @param tx TXBLOCK
 * @param indention Level of indention
 */
void showTxblock(ASAM::MDF::File &, ASAM::MDF::Txblock & tx, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "TX"
            << " data=" << tx.tx_data.data()
            << std::endl;
}

/**
 * Show FHBLOCK
 *
 * @param file MDF file
 * @param fh FHBLOCK
 * @param indention Level of indention
 */
void showFhblock(ASAM::MDF::File & file, ASAM::MDF::Fhblock & fh, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "FH"
            << " time_ns=" << fh.fh_time_ns
            << " tz_offset_min=" << fh.fh_tz_offset_min
            << " dst_offset_min=" << fh.fh_dst_offset_min
            << " time_flags=" << static_cast<uint16_t>(fh.fh_time_flags)
            << std::endl;

    /* follow links */
    // fh_fh_next is already covered
    ASAM::MDF::LINK mdlink = fh.fh_md_comment;
    if (mdlink) {
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
}

/**
 * Show CHBLOCK
 *
 * @param file MDF file
 * @param ch CHBLOCK
 * @param indention Level of indention
 */
void showChblock(ASAM::MDF::File & file, ASAM::MDF::Chblock & ch, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "CH"
            << " element_count=" << ch.ch_element_count
            << " type=" << static_cast<uint16_t>(ch.ch_type)
            << std::endl;

    /* follow links */
    // ch_ch_next is already covered
    ASAM::MDF::LINK chlink = ch.ch_ch_first;
    while (chlink) {
        ASAM::MDF::Chblock chblock;
        chblock.read(file, chlink);
        showChblock(file, chblock, indention + " ");
        chlink = chblock.ch_ch_next;
    }
    ASAM::MDF::LINK txlink = ch.ch_tx_name;
    if (txlink) {
        ASAM::MDF::Txblock txblock;
        txblock.read(file, txlink);
        showTxblock(file, txblock, indention + " ");
    }
    ASAM::MDF::LINK mdlink = ch.ch_md_comment;
    if (mdlink) {
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
    for (unsigned int i = 0; i < ch.ch_element_count; ++i) {
        ASAM::MDF::Dgblock dgblock;
        dgblock.read(file, ch.ch_element[i].dg);
        showDgblock(file, dgblock, indention + " ");

        ASAM::MDF::Cgblock cgblock;
        cgblock.read(file, ch.ch_element[i].cg);
        showCgblock(file, cgblock, indention + " ");

        ASAM::MDF::Cnblock cnblock;
        cnblock.read(file, ch.ch_element[i].cn);
        showCnblock(file, cnblock, indention + " ");
    }
}

/**
 * Show ATBLOCK
 *
 * @param file MDF file
 * @param at ATBLOCK
 * @param indention Level of indention
 */
void showAtblock(ASAM::MDF::File & file, ASAM::MDF::Atblock & at, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "AT"
            << " flags=" << at.at_flags
            << " creator_index=" << at.at_creator_index
            << " md5_checksum=" << at.at_md5_checksum.data()
            << " original_size=" << at.at_original_size
            << " embedded_size=" << at.at_embedded_size
            << " embedded_data=" << at.at_embedded_data.data()
            << std::endl;

    /* follow links */
    // at_at_next is already covered
    ASAM::MDF::LINK txlink = at.at_tx_filename;
    if (txlink) {
        ASAM::MDF::Txblock txblock;
        txblock.read(file, txlink);
        showTxblock(file, txblock, indention + " ");
    }
    txlink = at.at_tx_mimetype;
    if (txlink) {
        ASAM::MDF::Txblock txblock;
        txblock.read(file, txlink);
        showTxblock(file, txblock, indention + " ");
    }
    ASAM::MDF::LINK mdlink = at.at_md_comment;
    if (mdlink) {
        /* @todo The specification only mentions links to MD here.
         * However the Examples/ChannelInfo/AttachmentRef/Vector_AttachmentRef.mf4
         * shows that also links to TX are used.
         * I expect that this is just missing in the documentation.
         */
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
}

/**
 * Show EVBLOCK
 *
 * @param file MDF file
 * @param ev EVBLOCK
 * @param indention Level of indention
 */
void showEvblock(ASAM::MDF::File & file, ASAM::MDF::Evblock & ev, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "EV"
            << " type=" << static_cast<uint16_t>(ev.ev_type)
            << " sync_type=" << static_cast<uint16_t>(ev.ev_sync_type)
            << " range_type=" << static_cast<uint16_t>(ev.ev_range_type)
            << " cause=" << static_cast<uint16_t>(ev.ev_cause)
            << " flags=" << static_cast<uint16_t>(ev.ev_flags)
            << " scope_count=" << ev.ev_scope_count
            << " attachment_count=" << ev.ev_attachment_count
            << " creator_index=" << ev.ev_creator_index
            << " sync_base_value=" << ev.ev_sync_base_value
            << " sync_factor=" << ev.ev_sync_factor
            << std::endl;

    /* follow links */
    // ev_ev_next is already covered
    // @todo ev_ev_parent
    // @todo ev_ev_range
    ASAM::MDF::LINK txlink = ev.ev_tx_name;
    if (txlink) {
        ASAM::MDF::Txblock txblock;
        txblock.read(file, txlink);
        showTxblock(file, txblock, indention + " ");
    }
    ASAM::MDF::LINK mdlink = ev.ev_md_comment;
    if (mdlink) {
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
    // @todo ev_scope
    // @todo ev_at_reference
}

/**
 * Show DGBLOCK
 *
 * @param file MDF file
 * @param dg DGBLOCK
 * @param indention Level of indention
 */
void showDgblock(ASAM::MDF::File & file, ASAM::MDF::Dgblock & dg, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "DG"
            << " rec_id_size=" << static_cast<uint16_t>(dg.dg_rec_id_size)
            << std::endl;

    /* follow links */
    // dg_dg_next is already covered
    ASAM::MDF::LINK cglink = dg.dg_cg_first;
    while (cglink) {
        ASAM::MDF::Cgblock cgblock;
        cgblock.read(file, cglink);
        showCgblock(file, cgblock, indention + " ");
        cglink = cgblock.cg_cg_next;
    }
    // @todo showBlock(dg.dg_data(), indention + " ");
    ASAM::MDF::LINK mdlink = dg.dg_md_comment;
    if (mdlink) {
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
}

/**
 * Show CGBLOCK
 *
 * @param file MDF file
 * @param cg CGBLOCK
 * @param indention Level of indention
 */
void showCgblock(ASAM::MDF::File & file, ASAM::MDF::Cgblock & cg, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "CG"
            << " record_id=" << cg.cg_record_id
            << " cycle_count=" << cg.cg_cycle_count
            << " flags=" << cg.cg_flags
            << " path_separator=" << cg.cg_path_separator
            << " data_bytes=" << cg.cg_data_bytes
            << " inval_bytes=" << cg.cg_inval_bytes
            << std::endl;

    /* follow links */
    // cg_cg_next is already covered
    ASAM::MDF::LINK cnlink = cg.cg_cn_first;
    while (cnlink) {
        ASAM::MDF::Cnblock cnblock;
        cnblock.read(file, cnlink);
        showCnblock(file, cnblock, indention + " ");
        cnlink = cnblock.cn_cn_next;
    }
    ASAM::MDF::LINK txlink = cg.cg_tx_acq_name;
    if (txlink) {
        ASAM::MDF::Txblock txblock;
        txblock.read(file, txlink);
        showTxblock(file, txblock, indention + " ");
    }
    ASAM::MDF::LINK silink = cg.cg_si_acq_source;
    if (silink) {
        ASAM::MDF::Siblock siblock;
        siblock.read(file, silink);
        showSiblock(file, siblock, indention + " ");
    }
    ASAM::MDF::LINK srlink = cg.cg_sr_first;
    while (srlink) {
        ASAM::MDF::Srblock srblock;
        srblock.read(file, srlink);
        showSrblock(file, srblock, indention + " ");
        srlink = srblock.sr_sr_next;
    }
    ASAM::MDF::LINK mdlink = cg.cg_md_comment;
    if (mdlink) {
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
}

/**
 * Show SIBLOCK
 *
 * @param file MDF file
 * @param si SIBLOCK
 * @param indention Level of indention
 */
void showSiblock(ASAM::MDF::File & file, ASAM::MDF::Siblock & si, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "SI"
            << " type=" << static_cast<uint16_t>(si.si_type)
            << " bus_type=" << static_cast<uint16_t>(si.si_bus_type)
            << " flags=" << static_cast<uint16_t>(si.si_flags)
            << std::endl;

    /* follow links */
    ASAM::MDF::LINK txlink = si.si_tx_name;
    if (txlink) {
        ASAM::MDF::Txblock txblock;
        txblock.read(file, txlink);
        showTxblock(file, txblock, indention + " ");
    }
    txlink = si.si_tx_path;
    if (txlink) {
        ASAM::MDF::Txblock txblock;
        txblock.read(file, txlink);
        showTxblock(file, txblock, indention + " ");
    }
    ASAM::MDF::LINK mdlink = si.si_md_comment;
    if (mdlink) {
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
}

/**
 * Show CNBLOCK
 *
 * @param file MDF file
 * @param cn CNBLOCK
 * @param indention Level of indention
 */
void showCnblock(ASAM::MDF::File & file, ASAM::MDF::Cnblock & cn, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "CN"
            << " type=" << static_cast<uint16_t>(cn.cn_type)
            << " sync_type=" << static_cast<uint16_t>(cn.cn_sync_type)
            << " data_type=" << static_cast<uint16_t>(cn.cn_data_type)
            << " bit_offset=" << static_cast<uint16_t>(cn.cn_bit_offset)
            << " byte_offset=" << cn.cn_byte_offset
            << " bit_count=" << cn.cn_bit_count
            << " flags=" << cn.cn_flags
            << " inval_bit_pos=" << cn.cn_inval_bit_pos
            << " precision=" << static_cast<uint16_t>(cn.cn_precision)
            << " attachment_count=" << cn.cn_attachment_count
            << " val_range=" << cn.cn_val_range_min << ".." << cn.cn_val_range_max
            << " limit=" << cn.cn_limit_min << ".." << cn.cn_limit_max
            << " limit_ext=" << cn.cn_limit_ext_min << ".." << cn.cn_limit_ext_max
            << std::endl;

    /* follow links */
    // cn_cn_next is already covered
    // @todo showCnblock(cn.cn_composition(), indention + " ");
    ASAM::MDF::LINK txlink = cn.cn_tx_name;
    if (txlink) {
        ASAM::MDF::Txblock txblock;
        txblock.read(file, txlink);
        showTxblock(file, txblock, indention + " ");
    }
    ASAM::MDF::LINK silink = cn.cn_si_source;
    if (silink) {
        ASAM::MDF::Siblock siblock;
        siblock.read(file, silink);
        showSiblock(file, siblock, indention + " ");
    }
    ASAM::MDF::LINK cclink = cn.cn_cc_conversion;
    if (cclink) {
        ASAM::MDF::Ccblock ccblock;
        ccblock.read(file, cclink);
        showCcblock(file, ccblock, indention + " ");
    }
    // @todo cn_data
    ASAM::MDF::LINK mdlink = cn.cn_md_unit;
    if (mdlink) {
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
    mdlink = cn.cn_md_comment;
    if (mdlink) {
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
    // @todo cn_at_reference
    // @todo cn_default_x
}

/**
 * Show CCBLOCK
 *
 * @param file MDF file
 * @param cc CCBLOCK
 * @param indention Level of indention
 */
void showCcblock(ASAM::MDF::File & file, ASAM::MDF::Ccblock & cc, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "CC"
            << " type=" << static_cast<uint16_t>(cc.cc_type)
            << " precision=" << static_cast<uint16_t>(cc.cc_precision)
            << " flags=" << cc.cc_flags
            << " ref_count=" << cc.cc_ref_count
            << " val_count=" << cc.cc_val_count
            << " phy_range=" << cc.cc_phy_range_min << ".." << cc.cc_phy_range_max
            // @todo cc_val
            << std::endl;

    /* follow links */
    ASAM::MDF::LINK txlink = cc.cc_tx_name;
    if (txlink) {
        ASAM::MDF::Txblock txblock;
        txblock.read(file, txlink);
        showTxblock(file, txblock, indention + " ");
    }
    ASAM::MDF::LINK mdlink = cc.cc_md_unit;
    if (mdlink) {
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
    mdlink = cc.cc_md_comment;
    if (mdlink) {
        switch (ASAM::MDF::Block::blockType(file, mdlink)) {
        case ASAM::MDF::BlockType::TX: {
            ASAM::MDF::Txblock txblock;
            txblock.read(file, mdlink);
            showTxblock(file, txblock, indention + " ");
        }
        break;
        case ASAM::MDF::BlockType::MD: {
            ASAM::MDF::Mdblock mdblock;
            mdblock.read(file, mdlink);
            showMdblock(file, mdblock, indention + " ");
        }
        break;
        default:
            throw ASAM::MDF::UnexpectedIdentifier(mdlink);
        }
    }
    ASAM::MDF::LINK cclink = cc.cc_cc_inverse;
    if (cclink) {
        ASAM::MDF::Ccblock ccblock;
        ccblock.read(file, cclink);
        showCcblock(file, ccblock, indention + " ");
    }
    // @todo cc_ref
}

/**
 * Show CABLOCK
 *
 * @param ca CABLOCK
 * @param indention Level of indention
 */
void showCablock(ASAM::MDF::File &, ASAM::MDF::Cablock & ca, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "CA"
            << " type=" << static_cast<uint16_t>(ca.ca_type)
            << " storage=" << static_cast<uint16_t>(ca.ca_storage)
            << " ndim=" << ca.ca_ndim
            << " flags=" << ca.ca_flags
            << " byte_offset_base=" << ca.ca_byte_offset_base
            << " inval_bit_pos_base=" << ca.ca_inval_bit_pos_base
            // @todo ca_dim_size
            // @todo ca_axis_value
            // @todo ca_cycle_count
            << std::endl;

    /* follow links */
    // @todo ca_composition
    // @todo ca_data
    // @todo ca_dynamic_size
    // @todo ca_input_quantity
    // @todo ca_output_quantity
    // @todo ca_comparison_quantity
    // @todo ca_cc_axis_conversion
    // @todo ca_axis
}

/**
 * Show DTBLOCK
 *
 * @param dt DTBLOCK
 * @param indention Level of indention
 */
void showDtblock(ASAM::MDF::File &, ASAM::MDF::Dtblock & dt, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "DT"
            << " data=" << dt.dt_data.data()
            << std::endl;
}

/**
 * Show SRBLOCK
 *
 * @param sr SRBLOCK
 * @param indention Level of indention
 */
void showSrblock(ASAM::MDF::File &, ASAM::MDF::Srblock & sr, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "SR"
            << " cycle_count=" << sr.sr_cycle_count
            << " interval=" << sr.sr_interval
            << " sync_type=" << static_cast<uint16_t>(sr.sr_sync_type)
            << " flags=" << static_cast<uint16_t>(sr.sr_flags)
            << std::endl;

    /* follow links */
    // sr_sr_next is already covered
    // @todo sr_data
}

/**
 * Show RDBLOCK
 *
 * @param rd RDBLOCK
 * @param indention Level of indention
 */
void showRdblock(ASAM::MDF::File &, ASAM::MDF::Rdblock & rd, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "RD"
            << " data=" << rd.rd_data.data()
            << std::endl;
}

/**
 * Show SDBLOCK
 *
 * @param sd SDBLOCK
 * @param indention Level of indention
 */
void showSdblock(ASAM::MDF::File &, ASAM::MDF::Sdblock & sd, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "SD"
            << " data=" << sd.sd_data.data()
            << std::endl;
}

/**
 * Show DLBLOCK
 *
 * @param dl DLBLOCK
 * @param indention Level of indention
 */
void showDlblock(ASAM::MDF::File &, ASAM::MDF::Dlblock & dl, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "DL"
            << " flags=" << static_cast<uint16_t>(dl.dl_flags)
            << " count=" << dl.dl_count;
    if (dl.dl_equal_length.size() == 1) {
        std::cout << " equal_length=" << dl.dl_equal_length[0];
    }
    std::cout
    // @todo dl_offset
            << std::endl;

    /* follow links */
    // dl_dl_next is already covered
    // @todo dl_data
}

/**
 * Show DZBLOCK
 *
 * @param dz DZBLOCK
 * @param indention Level of indention
 */
void showDzblock(ASAM::MDF::File &, ASAM::MDF::Dzblock & dz, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "DZ"
            << " org_block_type=" << dz.dz_org_block_type[0] << dz.dz_org_block_type[1]
            << " zip_type=" << static_cast<uint16_t>(dz.dz_zip_type)
            << " zip_parameter=" << dz.dz_zip_parameter
            << " org_data_length=" << dz.dz_org_data_length
            << " data_length=" << dz.dz_data_length
            << " data=" << dz.dz_data.data()
            << std::endl;
}

/**
 * Show HLBLOCK
 *
 * @param file MDF file
 * @param hl HLBLOCK
 * @param indention Level of indention
 */
void showHlblock(ASAM::MDF::File & file, ASAM::MDF::Hlblock & hl, std::string indention)
{
    /* show data */
    std::cout
            << indention
            << "HL"
            << " flags=" << hl.hl_flags
            << " zip_type=" << static_cast<uint16_t>(hl.hl_zip_type)
            << std::endl;

    /* follow links */
    ASAM::MDF::LINK dllink = hl.hl_dl_first;
    while (dllink) {
        ASAM::MDF::Dlblock dlblock;
        dlblock.read(file, dllink);
        showDlblock(file, dlblock, indention + " ");
        dllink = dlblock.dl_dl_next;
    }
}

/**
 * main program
 *
 * @param argc argument count
 * @param argv argument values
 * @return exit code
 */
int main(int argc, char * argv[])
{
    /* check arguments */
    if (argc != 2) {
        std::cout << "Parser <filename.mf4>" << std::endl;
        return -1;
    }

    /* open file */
    ASAM::MDF::File file;
    file.open(argv[1]);
    if (!file.is_open()) {
        std::cout << "File not open" << std::endl;
        return -1;
    }

    /* show ID block */
    showIdblock(file, file.id, "");

    /* check if file is finished */
    if (!file.id.finalized()) {
        std::cout << "File is unfinalized." << std::endl;
    }

    /* show HD block */
    showHdblock(file, file.hd, "");

    /* close file */
    file.close();

    return 0;
}
