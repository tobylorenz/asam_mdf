/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Sdblock.h"

#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Sdblock::Sdblock() :
    Block(),
    sd_data()
{
    id = expectedId();
}

void Sdblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    // nothing to do here

    /* read Data section */
    size_t size = length - Block::expectedMinimumBlockSize(file.id.id_ver);
    sd_data.resize(size);
    file.fs.read(reinterpret_cast<char *>(sd_data.data()), sd_data.size());
}

void Sdblock::write(File & file, LINK & link)
{
    /* pre processing */
    link_count = 0;
    length =
        expectedMinimumBlockSize(file.id.id_ver) +
        sd_data.size() * sizeof(BYTE);

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    // nothing to do here

    /* write Data section */
    file.fs.write(reinterpret_cast<char *>(sd_data.data()), sd_data.size());
}

std::array<CHAR, 4> Sdblock::expectedId() const
{
    return {{ '#', '#', 'S', 'D' }};
}

UINT64 Sdblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size += 0;
        break;
    }
    return size;
}

UINT64 Sdblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size = length;
        break;
    }
    return size;
}

}
}
