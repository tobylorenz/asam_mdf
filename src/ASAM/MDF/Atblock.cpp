/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Atblock.h"

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

/* expected identifier */
static const std::array<CHAR, 4> expectedId = {{'#', '#', 'A', 'T'}};

Atblock::Atblock() :
    Block(),
    at_at_next(0),
    at_tx_filename(0),
    at_tx_mimetype(0),
    at_md_comment(0),
    at_flags(0),
    at_creator_index(0),
    at_reserved(),
    at_md5_checksum(),
    at_original_size(0),
    at_embedded_size(0),
    at_embedded_data()
{
    id = expectedId();
}

void Atblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, at_at_next);
    readLittleEndian(file.fs, at_tx_filename);
    readLittleEndian(file.fs, at_tx_mimetype);
    readLittleEndian(file.fs, at_md_comment);

    /* read Data section */
    readLittleEndian(file.fs, at_flags);
    readLittleEndian(file.fs, at_creator_index);
    file.fs.read(reinterpret_cast<char *>(at_reserved.data()), at_reserved.size());
    file.fs.read(reinterpret_cast<char *>(at_md5_checksum.data()), at_md5_checksum.size());
    readLittleEndian(file.fs, at_original_size);
    readLittleEndian(file.fs, at_embedded_size);
    at_embedded_data.resize(at_embedded_size);
    file.fs.read(reinterpret_cast<char *>(at_embedded_data.data()), at_embedded_size);
}

void Atblock::write(File & file, LINK & link)
{
    /* pre processing */
    at_embedded_size = at_embedded_data.size();
    link_count = 4;
    length =
        expectedMinimumBlockSize(file.id.id_ver) +
        at_embedded_size;

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, at_at_next);
    writeLittleEndian(file.fs, at_tx_filename);
    writeLittleEndian(file.fs, at_tx_mimetype);
    writeLittleEndian(file.fs, at_md_comment);

    /* write Data section */
    writeLittleEndian(file.fs, at_flags);
    writeLittleEndian(file.fs, at_creator_index);
    file.fs.write(reinterpret_cast<char *>(at_reserved.data()), at_reserved.size());
    file.fs.write(reinterpret_cast<char *>(at_md5_checksum.data()), at_md5_checksum.size());
    writeLittleEndian(file.fs, at_original_size);
    writeLittleEndian(file.fs, at_embedded_size);
    file.fs.write(reinterpret_cast<char *>(at_embedded_data.data()), at_embedded_size);
}

std::array<CHAR, 4> Atblock::expectedId() const
{
    return {{ '#', '#', 'A', 'T' }};
}

UINT64 Atblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(at_at_next) +
            sizeof(at_tx_filename) +
            sizeof(at_tx_mimetype) +
            sizeof(at_md_comment) +
            sizeof(at_flags) +
            sizeof(at_creator_index) +
            sizeof(at_reserved) +
            sizeof(at_md5_checksum) +
            sizeof(at_original_size) +
            sizeof(at_embedded_size);
        break;
    }
    return size;
}

UINT64 Atblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size = length;
        break;
    }
    return size;
}

}
}
