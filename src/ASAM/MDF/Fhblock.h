/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief File history block (FHBLOCK)
 *
 * Change history information of the MDF
 * file
 */
class ASAM_MDF_EXPORT Fhblock final : public Block
{
public:
    Fhblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief fh_fh_next
     *
     * Link to next FHBLOCK (can be NIL if list
     * finished)
     */
    LINK fh_fh_next;

    /**
     * @brief fh_md_comment
     *
     * Link to MDBLOCK containing comment about
     * the creation or modification of the MDF file.
     */
    LINK fh_md_comment;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief fh_time_ns
     *
     * Time stamp at which the file has been
     * changed/created (first entry) in nanoseconds
     * elapsed since 00:00:00 01.01.1970 (UTC time
     * or local time, depending on "local time" flag).
     */
    UINT64 fh_time_ns;

    /**
     * @brief fh_tz_offset_min
     *
     * Time zone offset in minutes.
     * The value is not necessarily a multiple of 60
     * and can be negative! For the current time zone
     * definitions, it is expected to be in the range
     * [-840,840] min.
     *
     * For example a value of 60 (min) means
     * UTC+1 time zone = Central European Time
     * (CET).
     *
     * Only valid if "time offsets valid" flag is set in
     * time flags.
     */
    INT16 fh_tz_offset_min;

    /**
     * @brief fh_dst_offset_min
     *
     * Daylight saving time (DST) offset in minutes
     * for start time stamp. During the summer
     * months, most regions observe a DST offset of
     * 60 min (1 hour).
     *
     * Only valid if "time offsets valid" flag is set in
     * time flags.
     */
    INT16 fh_dst_offset_min;

    /**
     * @brief fh_time_flags
     *
     * Time Flags
     *
     * The value contains the following bit flags (Bit 0
     * = LSB):
     *
     * Bit 0: Local time flag.
     * If set, the start time stamp in nanoseconds
     * represents the local time instead of the UTC
     * time, In this case, time zone and DST offset
     * must not be considered (time offsets flag must
     * not be set). Should only be used if UTC time is
     * unknown.
     * If not set (default), the start time stamp
     * represents the UTC time.
     *
     * Bit 1: Time offsets valid flag.
     * If set, the time zone and DST offsets are valid.
     * Must not be set together with "local time" flag
     * (mutually exclusive).
     * If the offsets are valid, the locally displayed
     * time at start of recording can be determined
     * (after conversion of offsets to ns) by
     * Local time = UTC time + time zone offset +
     * DST offset.
     */
    UINT8 fh_time_flags;

    /**
     * @brief fh_reserved
     *
     * Reserved
     */
    std::array<BYTE, 3> fh_reserved;

    /** @} */
};

}
}
