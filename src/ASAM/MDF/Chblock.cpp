/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Chblock.h"

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Chblock::Chblock() :
    Block(),
    ch_ch_next(0),
    ch_ch_first(0),
    ch_tx_name(0),
    ch_md_comment(0),
    ch_element(),
    ch_element_count(0),
    ch_type(0),
    ch_reserved()
{
    id = expectedId();
}

void Chblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, ch_ch_next);
    readLittleEndian(file.fs, ch_ch_first);
    readLittleEndian(file.fs, ch_tx_name);
    readLittleEndian(file.fs, ch_md_comment);
    ch_element.resize((link_count - 4) / 3);
    for(LinkTriple & i : ch_element) {
        readLittleEndian(file.fs, i.dg);
        readLittleEndian(file.fs, i.cg);
        readLittleEndian(file.fs, i.cn);
    }

    /* read Data section */
    readLittleEndian(file.fs, ch_element_count);
    readLittleEndian(file.fs, ch_type);
    file.fs.read(reinterpret_cast<char *>(ch_reserved.data()), ch_reserved.size());
}

void Chblock::write(File & file, LINK & link)
{
    /* pre processing */
    ch_element_count = static_cast<UINT32>(ch_element.size());
    link_count = 4 + 3 * ch_element_count;
    length =
        expectedMinimumBlockSize(file.id.id_ver) +
        (link_count - 4) * sizeof(LINK);

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, ch_ch_next);
    writeLittleEndian(file.fs, ch_ch_first);
    writeLittleEndian(file.fs, ch_tx_name);
    writeLittleEndian(file.fs, ch_md_comment);
    for(LinkTriple & i : ch_element) {
        writeLittleEndian(file.fs, i);
    }

    /* write Data section */
    writeLittleEndian(file.fs, ch_element_count);
    writeLittleEndian(file.fs, ch_type);
    file.fs.write(reinterpret_cast<char *>(ch_reserved.data()), ch_reserved.size());
}

std::array<CHAR, 4> Chblock::expectedId() const
{
    return {{ '#', '#', 'C', 'H' }};
}

UINT64 Chblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(ch_ch_next) +
            sizeof(ch_ch_first) +
            sizeof(ch_tx_name) +
            sizeof(ch_md_comment) +
            sizeof(ch_element_count) +
            sizeof(ch_type) +
            sizeof(ch_reserved);
        break;
    }
    return size;
}

UINT64 Chblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size += (link_count - 4) * sizeof(LINK);
        break;
    }
    return size;
}

}
}
