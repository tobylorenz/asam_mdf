/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <stdexcept>
#include <sstream>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"

namespace ASAM {
namespace MDF {

/**
 * Function analog to std::to_string, but returns hexadecimal string.
 *
 * @param[in] link position
 * @return Hex string
 */
static std::string to_hexstring(LINK link)
{
    std::stringstream ss;
    ss << std::hex << link;
    return ss.str();
}

/**
 * This exception is thrown when a block is read and the identifier is unexpected.
 */
class ASAM_MDF_EXPORT UnexpectedIdentifier : public std::runtime_error
{
public:
    /**
     * Constructor
     *
     * @param[in] link Block position within MDF file
     */
    explicit UnexpectedIdentifier(LINK link) :
        std::runtime_error("Identifier is unexpected at 0x" + to_hexstring(link) + ".") {
    }
};

/**
 * This exception is thrown when a block is read and the block size is unexpected.
 */
class ASAM_MDF_EXPORT UnexpectedBlockSize : public std::runtime_error
{
public:
    /**
     * Constructor
     *
     * @param[in] link Block position within MDF file
     */
    explicit UnexpectedBlockSize(LINK link) :
        std::runtime_error("Block size is unexpected at 0x" + to_hexstring(link) + ".") {
    }
};

/**
 * This exception is thrown when the file version is unsupported (either too low or too high).
 */
class ASAM_MDF_EXPORT UnsupportedVersion : public std::runtime_error
{
public:
    /**
     * Constructor
     *
     * @param[in] id_ver MDF file version
     */
    explicit UnsupportedVersion(UINT16 id_ver) :
        std::runtime_error("Version " + std::to_string(id_ver / 100)  + "." + std::to_string(id_ver % 100) + " is unsupported.") {
    }
};

/**
 * This exception is thrown when the zlib compress2/uncompress function
 * returned with a non-zero value.
 */
class ASAM_MDF_EXPORT CompressionError : public std::runtime_error
{
public:
    /**
     * Constructor
     *
     * @param[in] retVal zlib return value
     */
    explicit CompressionError(int retVal) :
        std::runtime_error("ZLIB compress2/uncompress returned with " + std::to_string(retVal) + ".") {
    }
};

}
}
