/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Dzblock.h"

#include <cassert>

#include "Compression.h"
#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Dzblock::Dzblock() :
    Block(),
    dz_org_block_type(),
    dz_zip_type(0),
    dz_reserved(0),
    dz_zip_parameter(0),
    dz_org_data_length(0),
    dz_data_length(0),
    dz_data()
{
    id = expectedId();
}

void Dzblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    // nothing to do here

    /* read Data section */
    file.fs.read(reinterpret_cast<char *>(dz_org_block_type.data()), dz_org_block_type.size());
    readLittleEndian(file.fs, dz_zip_type);
    readLittleEndian(file.fs, dz_reserved);
    readLittleEndian(file.fs, dz_zip_parameter);
    readLittleEndian(file.fs, dz_org_data_length);
    readLittleEndian(file.fs, dz_data_length);
    dz_data.resize(dz_data_length);
    file.fs.read(reinterpret_cast<char *>(dz_data.data()), dz_data.size());
}

void Dzblock::write(File & file, LINK & link)
{
    /* pre processing */
    dz_data_length = dz_data.size();
    link_count = 0;
    length =
        expectedMinimumBlockSize(file.id.id_ver) +
        dz_data.size() * sizeof(BYTE);

    /* check */
    assert(file.id.id_ver >= 410);

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    // nothing to do here

    /* write Data section */
    file.fs.write(reinterpret_cast<char *>(dz_org_block_type.data()), dz_org_block_type.size());
    writeLittleEndian(file.fs, dz_zip_type);
    writeLittleEndian(file.fs, dz_reserved);
    writeLittleEndian(file.fs, dz_zip_parameter);
    writeLittleEndian(file.fs, dz_org_data_length);
    writeLittleEndian(file.fs, dz_data_length);
    file.fs.write(reinterpret_cast<char *>(dz_data.data()), dz_data.size());
}

std::array<CHAR, 4> Dzblock::expectedId() const
{
    return {{ '#', '#', 'D', 'Z' }};
}

UINT64 Dzblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 410:
    case 411:
        size +=
            sizeof(dz_org_block_type) +
            sizeof(dz_zip_type) +
            sizeof(dz_reserved) +
            sizeof(dz_zip_parameter) +
            sizeof(dz_org_data_length) +
            sizeof(dz_data_length);
        break;
    }
    return size;
}

UINT64 Dzblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 410:
    case 411:
        size = length;
        break;
    }
    return size;
}

void Dzblock::deflate(Dtblock & dt)
{
    dz_org_block_type = {{'D', 'T'}};
    dz_org_data_length = dt.dt_data.size();

    switch (dz_zip_type) {
    case 0: {
        assert(dz_zip_parameter == 0);

        /* Deflate */
        deflateData(dz_data, dt.dt_data);
        dz_data_length = dz_data.size();
    }
    break;
    case 1: {
        assert(dz_zip_parameter > 1);

        /* Transposition */
        std::vector<BYTE> transposedData;
        transposeData(transposedData, dt.dt_data, dz_zip_parameter, dz_org_data_length % dz_zip_parameter);

        /* Deflate */
        deflateData(dz_data, transposedData);
        dz_data_length = dz_data.size();
    }
    break;
    }
}

void Dzblock::deflate(Sdblock & sd)
{
    dz_org_block_type = {{'S', 'D'}};
    dz_org_data_length = sd.sd_data.size();

    switch (dz_zip_type) {
    case 0: {
        assert(dz_zip_parameter == 0);

        /* Deflate */
        deflateData(dz_data, sd.sd_data);
        dz_data_length = dz_data.size();
    }
    break;
    case 1: {
        assert(dz_zip_parameter > 1);

        /* Transposition */
        std::vector<BYTE> transposedData;
        transposeData(transposedData, sd.sd_data, dz_zip_parameter, dz_org_data_length % dz_zip_parameter);

        /* Deflate */
        deflateData(dz_data, transposedData);
        dz_data_length = dz_data.size();
    }
    break;
    }
}

void Dzblock::deflate(Rdblock & rd)
{
    dz_org_block_type = {{'R', 'D'}};
    dz_org_data_length = rd.rd_data.size();

    switch (dz_zip_type) {
    case 0: {
        assert(dz_zip_parameter == 0);

        /* Deflate */
        deflateData(dz_data, rd.rd_data);
        dz_data_length = dz_data.size();
    }
    break;
    case 1: {
        assert(dz_zip_parameter > 1);

        /* Transposition */
        std::vector<BYTE> transposedData;
        transposeData(transposedData, rd.rd_data, dz_zip_parameter, dz_org_data_length / dz_zip_parameter);

        /* Deflate */
        deflateData(dz_data, transposedData);
        dz_data_length = dz_data.size();
    }
    break;
    }
}

void Dzblock::inflate(Dtblock & dt)
{
    /* check */
    assert((dz_org_block_type[0] == 'D') && (dz_org_block_type[1] == 'T'));

    switch (dz_zip_type) {
    case 0: {
        assert(dz_zip_parameter == 0);

        /* Inflate */
        dt.dt_data.resize(dz_org_data_length);
        inflateData(dt.dt_data, dz_data);
    }
    break;
    case 1: {
        assert(dz_zip_parameter > 1);

        /* Inflate */
        std::vector<BYTE> deflatedData;
        deflatedData.resize(dz_org_data_length);
        inflateData(deflatedData, dz_data);

        /* Transposition */
        dt.dt_data.resize(dz_org_data_length);
        transposeData(dt.dt_data, deflatedData, dz_org_data_length / dz_zip_parameter, dz_zip_parameter);
    }
    break;
    }
}

void Dzblock::inflate(Sdblock & sd)
{
    /* check */
    assert((dz_org_block_type[0] == 'S') && (dz_org_block_type[1] == 'D'));

    switch (dz_zip_type) {
    case 0: {
        assert(dz_zip_parameter == 0);

        /* Inflate */
        sd.sd_data.resize(dz_org_data_length);
        inflateData(sd.sd_data, dz_data);
    }
    break;
    case 1: {
        assert(dz_zip_parameter > 1);

        /* Inflate */
        std::vector<BYTE> deflatedData;
        deflatedData.resize(dz_org_data_length);
        inflateData(deflatedData, dz_data);

        /* Transposition */
        sd.sd_data.resize(dz_org_data_length);
        transposeData(sd.sd_data, deflatedData, dz_org_data_length % dz_zip_parameter, dz_zip_parameter);
    }
    break;
    }
}

void Dzblock::inflate(Rdblock & rd)
{
    /* check */
    assert((dz_org_block_type[0] == 'R') && (dz_org_block_type[1] == 'D'));

    switch (dz_zip_type) {
    case 0: {
        assert(dz_zip_parameter == 0);

        /* Inflate */
        rd.rd_data.resize(dz_org_data_length);
        inflateData(rd.rd_data, dz_data);
    }
    break;
    case 1: {
        assert(dz_zip_parameter > 1);

        /* Inflate */
        std::vector<BYTE> deflatedData;
        deflatedData.resize(dz_org_data_length);
        inflateData(deflatedData, dz_data);

        /* Transposition */
        rd.rd_data.resize(dz_org_data_length);
        transposeData(rd.rd_data, deflatedData, dz_org_data_length % dz_zip_parameter, dz_zip_parameter);
    }
    break;
    }
}

}
}
