/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#include "Cnblock.h"

#include <cassert>

#include "Endianess.h"
#include "Exceptions.h"
#include "File.h"

namespace ASAM {
namespace MDF {

Cnblock::Cnblock() :
    Block(),
    cn_cn_next(0),
    cn_composition(0),
    cn_tx_name(0),
    cn_si_source(0),
    cn_cc_conversion(0),
    cn_data(0),
    cn_md_unit(0),
    cn_md_comment(0),
    cn_at_reference(),
    cn_default_x(),
    cn_type(0),
    cn_sync_type(0),
    cn_data_type(0),
    cn_bit_offset(0),
    cn_byte_offset(0),
    cn_bit_count(0),
    cn_flags(0),
    cn_inval_bit_pos(0),
    cn_precision(0),
    cn_reserved(0),
    cn_attachment_count(0),
    cn_val_range_min(0.0),
    cn_val_range_max(0.0),
    cn_limit_min(0.0),
    cn_limit_max(0.0),
    cn_limit_ext_min(0.0),
    cn_limit_ext_max(0.0)
{
    id = expectedId();
}

void Cnblock::read(File & file, LINK link)
{
    /* read Header section */
    Block::read(file, link);

    /* read Link section */
    readLittleEndian(file.fs, cn_cn_next);
    readLittleEndian(file.fs, cn_composition);
    readLittleEndian(file.fs, cn_tx_name);
    readLittleEndian(file.fs, cn_si_source);
    readLittleEndian(file.fs, cn_cc_conversion);
    readLittleEndian(file.fs, cn_data);
    readLittleEndian(file.fs, cn_md_unit);
    readLittleEndian(file.fs, cn_md_comment);
    std::vector<LINK> remainingLinks;
    remainingLinks.resize(link_count - 8);
    for(LINK & i : remainingLinks) {
        readLittleEndian(file.fs, i);
    }

    /* read Data section */
    readLittleEndian(file.fs, cn_type);
    readLittleEndian(file.fs, cn_sync_type);
    readLittleEndian(file.fs, cn_data_type);
    readLittleEndian(file.fs, cn_bit_offset);
    readLittleEndian(file.fs, cn_byte_offset);
    readLittleEndian(file.fs, cn_bit_count);
    readLittleEndian(file.fs, cn_flags);
    readLittleEndian(file.fs, cn_inval_bit_pos);
    readLittleEndian(file.fs, cn_precision);
    readLittleEndian(file.fs, cn_reserved);
    readLittleEndian(file.fs, cn_attachment_count);
    readLittleEndian(file.fs, cn_val_range_min);
    readLittleEndian(file.fs, cn_val_range_max);
    readLittleEndian(file.fs, cn_limit_min);
    readLittleEndian(file.fs, cn_limit_max);
    readLittleEndian(file.fs, cn_limit_ext_min);
    readLittleEndian(file.fs, cn_limit_ext_max);

    /* assign remaining links */
    UINT16 n = 0;
    if (cn_attachment_count > 0) {
        cn_at_reference.resize(cn_attachment_count);
        cn_at_reference.assign(remainingLinks.begin() + n, remainingLinks.begin() + n + cn_at_reference.size());
        n += cn_attachment_count;
    }
    if (cn_flags & (1 << 12)) { // if "default X" flag is set
        cn_default_x.resize(1);
        cn_default_x[0].dg = remainingLinks[n++];
        cn_default_x[0].cg = remainingLinks[n++];
        cn_default_x[0].cn = remainingLinks[n++];
    }
}

void Cnblock::write(File & file, LINK & link)
{
    /* pre processing */
    cn_attachment_count = static_cast<UINT16>(cn_at_reference.size());
    link_count =
        8 +
        cn_attachment_count +
        3 * cn_default_x.size();
    length =
        expectedMinimumBlockSize(file.id.id_ver) +
        (link_count - 8) * sizeof(LINK);

    /* check */
    if (cn_at_reference.size() > 0) {
        assert(file.id.id_ver >= 410); // valid since MDF v4.1.0
    }
    if (cn_default_x.size() > 0) {
        assert(file.id.id_ver >= 410); // valid since MDF v4.1.0
        assert(cn_flags & (1 << 12));
        assert(cn_default_x.size() == 1);
    }
    if (cn_type >= 5) {
        assert(file.id.id_ver >= 410); // valid since MDF v4.1.0
    }
    if (cn_flags & (1 << 10)) {
        assert(file.id.id_ver >= 410); // valid since MDF v4.1.0
    }
    if (cn_flags & (1 << 11)) {
        assert(file.id.id_ver >= 410); // valid since MDF v4.1.0
    }
    if (cn_flags & (1 << 12)) {
        assert(file.id.id_ver >= 410); // valid since MDF v4.1.0
    }
    if (cn_attachment_count > 0) {
        assert(file.id.id_ver >= 410); // valid since MDF v4.1.0
    }

    /* write Header section */
    Block::write(file, link);

    /* write Link section */
    writeLittleEndian(file.fs, cn_cn_next);
    writeLittleEndian(file.fs, cn_composition);
    writeLittleEndian(file.fs, cn_tx_name);
    writeLittleEndian(file.fs, cn_si_source);
    writeLittleEndian(file.fs, cn_cc_conversion);
    writeLittleEndian(file.fs, cn_data);
    writeLittleEndian(file.fs, cn_md_unit);
    writeLittleEndian(file.fs, cn_md_comment);
    for(LINK & i : cn_at_reference) {
        writeLittleEndian(file.fs, i);
    }
    for(LinkTriple & i : cn_default_x) {
        writeLittleEndian(file.fs, i.dg);
        writeLittleEndian(file.fs, i.cg);
        writeLittleEndian(file.fs, i.cn);
    }

    /* write Data section */
    writeLittleEndian(file.fs, cn_type);
    writeLittleEndian(file.fs, cn_sync_type);
    writeLittleEndian(file.fs, cn_data_type);
    writeLittleEndian(file.fs, cn_bit_offset);
    writeLittleEndian(file.fs, cn_byte_offset);
    writeLittleEndian(file.fs, cn_bit_count);
    writeLittleEndian(file.fs, cn_flags);
    writeLittleEndian(file.fs, cn_inval_bit_pos);
    writeLittleEndian(file.fs, cn_precision);
    writeLittleEndian(file.fs, cn_reserved);
    writeLittleEndian(file.fs, cn_attachment_count);
    writeLittleEndian(file.fs, cn_val_range_min);
    writeLittleEndian(file.fs, cn_val_range_max);
    writeLittleEndian(file.fs, cn_limit_min);
    writeLittleEndian(file.fs, cn_limit_max);
    writeLittleEndian(file.fs, cn_limit_ext_min);
    writeLittleEndian(file.fs, cn_limit_ext_max);
}

std::array<CHAR, 4> Cnblock::expectedId() const
{
    return {{ '#', '#', 'C', 'N' }};
}

UINT64 Cnblock::expectedMinimumBlockSize(UINT16 id_ver) const
{
    UINT64 size = Block::expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size +=
            sizeof(cn_cn_next) +
            sizeof(cn_composition) +
            sizeof(cn_tx_name) +
            sizeof(cn_si_source) +
            sizeof(cn_cc_conversion) +
            sizeof(cn_data) +
            sizeof(cn_md_unit) +
            sizeof(cn_md_comment) +
            sizeof(cn_type) +
            sizeof(cn_sync_type) +
            sizeof(cn_data_type) +
            sizeof(cn_bit_offset) +
            sizeof(cn_byte_offset) +
            sizeof(cn_bit_count) +
            sizeof(cn_flags) +
            sizeof(cn_inval_bit_pos) +
            sizeof(cn_precision) +
            sizeof(cn_reserved) +
            sizeof(cn_attachment_count) +
            sizeof(cn_val_range_min) +
            sizeof(cn_val_range_max) +
            sizeof(cn_limit_min) +
            sizeof(cn_limit_max) +
            sizeof(cn_limit_ext_min) +
            sizeof(cn_limit_ext_max);
        break;
    }
    return size;
}

UINT64 Cnblock::expectedBlockSize(UINT16 id_ver) const
{
    UINT64 size = expectedMinimumBlockSize(id_ver);
    switch (id_ver) {
    case 400:
    case 410:
    case 411:
        size += (link_count - 8) * sizeof(LINK);
        break;
    }
    return size;
}

}
}
