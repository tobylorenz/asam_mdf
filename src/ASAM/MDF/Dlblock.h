/*
 * Copyright (C) 2014-2015 Tobias Lorenz.
 * Contact: tobias.lorenz@gmx.net
 *
 * This file is part of Tobias Lorenz's Toolkit.
 *
 * Commercial License Usage
 * Licensees holding valid commercial licenses may use this file in
 * accordance with the commercial license agreement provided with the
 * Software or, alternatively, in accordance with the terms contained in
 * a written agreement between you and Tobias Lorenz.
 *
 * GNU General Public License 3.0 Usage
 * Alternatively, this file may be used under the terms of the GNU
 * General Public License version 3.0 as published by the Free Software
 * Foundation and appearing in the file LICENSE.GPL included in the
 * packaging of this file.  Please review the following information to
 * ensure the GNU General Public License version 3.0 requirements will be
 * met: http://www.gnu.org/copyleft/gpl.html.
 */

#pragma once

#include <array>
#include <vector>

#include "asam_mdf_export.h"
#include "platform.h"
#include "types.h"
#include "Block.h"

namespace ASAM {
namespace MDF {

/**
 * @brief Data list block (DLBLOCK)
 *
 * Ordered list of links to (signal/reduction)
 * data blocks if the data is spread over
 * multiple blocks
 */
class ASAM_MDF_EXPORT Dlblock final : public Block
{
public:
    Dlblock();

    virtual void read(File & file, LINK link) override;
    virtual void write(File & file, LINK & link) override;
    virtual std::array<CHAR, 4> expectedId() const override;
    virtual UINT64 expectedMinimumBlockSize(UINT16 id_ver) const override;
    virtual UINT64 expectedBlockSize(UINT16 id_ver) const override;

    /**
     * @name Link section
     * @{
     */

    /**
     * @brief dl_dl_next
     *
     * Pointer to next data list block (DLBLOCK)
     * (can be NIL).
     */
    LINK dl_dl_next;

    /**
     * @brief dl_data
     *
     * Pointers to the data blocks (DTBLOCK, SDBLOCK
     * or RDBLOCK or a DZBLOCK of the respective
     * block type). None of the links in the list can be NIL.
     *
     * It is not allowed to mix the data block types: all links
     * must uniformly reference the same data block type
     * (DTBLOCK/SDBLOCK/RDBLOCK or an equivalent
     * DZBLOCK). Also all DLBLOCKs in the linked list of
     * DLBLOCKs must reference the same data block
     * type
     *
     * Note: a mixture between DZBLOCKs and
     * uncompressed data blocks is allowed. However, if
     * a DZBLOCK is in the list, the complete linked list of
     * DLBLOCKs must be preceded by a HLBLOCK.
     * See also explanation of HLBLOCK.
     */
    std::vector<LINK> dl_data;

    /** @} */

    /**
     * @name Data section
     * @{
     */

    /**
     * @brief dl_flags
     *
     * Flags
     *
     * The value contains the following bit flags (Bit 0 =
     * LSB):
     *
     * Bit 0: Equal length flag.
     * If set, each DLBLOCK in the linked list has the
     * same number of referenced blocks (dl_count) and
     * the (uncompressed) data sections of the blocks
     * referenced by dl_data have a common length given
     * by dl_equal_length. The only exception is that for
     * the last DLBLOCK in the list (dl_dl_next = NIL), its
     * number of referenced blocks dl_count can be less
     * than or equal to dl_count of the previous
     * DLBLOCK, and the data section length of its last
     * referenced block (dl_data[dl_count-1]) can be less
     * than or equal to dl_equal_length.
     * If not set, the number of referenced blocks dl_count
     * may be different for each DLBLOCK in the linked
     * list, and the data section lengths of the referenced
     * blocks may be different and a table of offsets is
     * given in dl_offset.
     * Note: The value of the "equal length" flag must be
     * equal for all DLBLOCKs in the linked list.
     */
    UINT8 dl_flags;

    /**
     * @brief dl_reserved
     *
     * Reserved
     */
    std::array<BYTE, 3> dl_reserved;

    /**
     * @brief dl_count
     *
     * Number of referenced blocks N
     *
     * If the "equal length" flag (bit 0 in dl_flags) is set,
     * then dl_count must be equal for each DLBLOCK in
     * the linked list except for the last one. For the last
     * DLBLOCK (i.e. dl_dl_next = NIL) in this case the
     * value of dl_count can be less than or equal to
     * dl_count of the previous DLBLOCK.
     */
    UINT32 dl_count;

    /**
     * @brief dl_equal_length
     *
     * Only present if "equal length" flag (bit 0 in
     * dl_flags) is set.
     *
     * Equal data section length.
     *
     * Every block in dl_data list has a data section with a
     * length equal to dl_equal_length. This must be true
     * for each DLBLOCK within the linked list, and has
     * only one exception: the very last block
     * (dl_data[dl_count-1] of last DLBLOCK in linked list)
     * may have a data section with a different length
     * which must be less than or equal to
     * dl_equal_length.
     *
     * @note The specification is partially wrong here as it
     * shows "1" in the count column. Actually "1 or empty" is correct.
     */
    std::vector<UINT64> dl_equal_length;

    /**
     * @brief dl_offset
     *
     * Only present if "equal length" flag (bit 0 in
     * dl_flags) is not set.
     *
     * Start offset (in Bytes) for the data section of each
     * referenced block.
     *
     * If the (uncompressed) data sections of all blocks
     * referenced by dl_data list (for all DLBLOCKs in the
     * linked list) are concatenated, the start offset for a
     * referenced block gives the position of the data
     * section of this block within the concatenated
     * section.
     *
     * The start offset dl_offset[i] thus is equal to the sum
     * of the data section lengths of all referenced blocks
     * in dl_data list for every previous DLBLOCK in the
     * linked list, and of all referenced blocks in dl_data
     * list up to (i-1) for the current DLBLOCK. As a
     * consequence, the start offset dl_offset[0] for the
     * very first DLBLOCK must always be zero.
     */
    std::vector<UINT64> dl_offset;

    /** @} */
};

}
}
