# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.5] - 2018-06-24
### Changed
- All dependencies are in main CMakeLists.txt file now
- Update of CMakeLists.txt to use new feature target_sources
### Fixes
- Dzblock inflate/deflate fixed

## [1.0.4] - 2016-11-07
### Fixes
- Big Endian support

## [1.0.3] - 2016-11-01
### Fixes
- Fixed build abort in CMakeLists.txt

## [1.0.2] - 2016-10-31
### Added
- Compression/Decompression support
- File::setVersion to set MDF version
### Changed
- LinkTriple introduded in Cablock, Chblock and Cnblock (instead of 3*LINK)
- dl_equal_length is optional. Type changed from UINT64 to std::vector<UINT64>

## [1.0.1] - 2016-10-14
### Changed
- Mdblock/Txblock: std::vector<CHAR> used for data instead of std::string.
- Block::blockType is now a static function.
### Fixed
- Many small fixes

## [1.0.0] - 2016-09-29
### Added
- Initial version
